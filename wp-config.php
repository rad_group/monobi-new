<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'monobi_new');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%d,Rq?Blrse=6=bt(JhXTrTbrxJpkmL/+ynmdX;3L]c >[4!1(pWj4U>|krqR)8/');
define('SECURE_AUTH_KEY',  'vS8=bq?l0f,qRe~2xcvXI$Rw?.x(PJw2%l-}{OK@z!z(tCg}rFQE%ku-T_<PD8u8');
define('LOGGED_IN_KEY',    'L sSa*ql8 N77!fO]5{9PPvq!*4_GCIn$!_XpA,$QB[`aAXj(n}0>$Pei%KzIz2*');
define('NONCE_KEY',        'PJ|VB,[n[65?pQMh()xo`NTLgP~,dPYb2BU`3J>N6S[KM4DoxFRy=q5[sY#}7.F3');
define('AUTH_SALT',        '=*wjFswbjD2/kY_8gNF.hz!C_eTj$`|5wUX-sTsDi978*@&O/3HxmcFF~_Qk4N{w');
define('SECURE_AUTH_SALT', 'j5^Tl*LgC#Y^%Z2w75yZ-j}[>z1T`;_wp)yQ+51c$E(h6?]Y@ Y zY8Q)^yT!4JG');
define('LOGGED_IN_SALT',   'Hr+UdpqiYgneh+ w@V3!abANf*;mb5|($KB1LnvhJE)tv]6Fb<+ZH#O@`2WA^(`X');
define('NONCE_SALT',       '0R.{_mQ5F]45t7Z@ZmzNohCv^3#i1;Pz>B_!;JwDJQqezuaQ }sHAn1<c:xTCE(;');

/**#@-*/
// Remove p tag from Contact form 7 labels
define( 'WPCF7_AUTOP', false );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'radms_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
