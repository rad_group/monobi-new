# Copyright (C) 2018 Improved Variable Product Attributes for WooCommerce
# This file is distributed under the same license as the Improved Variable Product Attributes for WooCommerce package.
msgid ""
msgstr ""
"Project-Id-Version: Improved Variable Product Attributes for WooCommerce "
"4.2.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/improved-variable-"
"product-attributes\n"
"POT-Creation-Date: 2018-04-21 10:08:34+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2018-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"X-Poedit-KeywordsList: esc_html__;esc_html_e;esc_html_x:1,2c\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: improved-variable-product-attributes.php:192
msgid "Improved Options"
msgstr ""

#: includes/ivpa-frontend.php:365 includes/ivpa-frontend.php:1023
msgid "Select"
msgstr ""

#: includes/ivpa-frontend.php:366
msgid "Add to cart"
msgstr ""

#: includes/ivpa-frontend.php:367
msgid "Select options"
msgstr ""

#: includes/ivpa-frontend.php:373
msgid "View Cart"
msgstr ""

#: includes/ivpa-frontend.php:1259
msgid "Click to confirm input"
msgstr ""

#: includes/ivpa-frontend.php:1259
msgid "Input confirmed"
msgstr ""

#: includes/ivpa-frontend.php:1289
msgid "Click to confirm selection"
msgstr ""

#: includes/ivpa-frontend.php:1289
msgid "Selection confirmed"
msgstr ""

#: includes/ivpa-frontend.php:1336
msgid "Clear selection"
msgstr ""

#: includes/ivpa-frontend.php:1343
msgid "Qty:"
msgstr ""

#: includes/ivpa-settings.php:35
msgid "Improved Product Options for WooCommerce"
msgstr ""

#: includes/ivpa-settings.php:36
msgid "Settings page for Improved Product Options for WooCommerce!"
msgstr ""

#: includes/ivpa-settings.php:39
msgid "More plugins and themes?"
msgstr ""

#: includes/ivpa-settings.php:43
msgid "Documentation and Plugin Guide"
msgstr ""

#: includes/ivpa-settings.php:48 includes/ivpa-settings.php:49
msgid "Product Options"
msgstr ""

#: includes/ivpa-settings.php:52
msgid "General"
msgstr ""

#: includes/ivpa-settings.php:53
msgid "General Options"
msgstr ""

#: includes/ivpa-settings.php:56
msgid "Product Page"
msgstr ""

#: includes/ivpa-settings.php:57
msgid "Product Page Options"
msgstr ""

#: includes/ivpa-settings.php:60
msgid "Shop/Archives"
msgstr ""

#: includes/ivpa-settings.php:61
msgid "Shop/Archives Options"
msgstr ""

#: includes/ivpa-settings.php:64
msgid "Installation"
msgstr ""

#: includes/ivpa-settings.php:65
msgid "Installation Options"
msgstr ""

#: includes/ivpa-settings.php:68
msgid "Plugin License"
msgstr ""

#: includes/ivpa-settings.php:69
msgid "License Options"
msgstr ""

#: includes/ivpa-settings.php:78
msgid "Options Manager"
msgstr ""

#: includes/ivpa-settings.php:80
msgid ""
"Use the manager to customize your attributes or add custom product options!"
msgstr ""

#: includes/ivpa-settings.php:90
msgid "Attribute Swatch"
msgstr ""

#: includes/ivpa-settings.php:91
msgid "Custom Option"
msgstr ""

#: includes/ivpa-settings.php:96
msgid "Select Attribute"
msgstr ""

#: includes/ivpa-settings.php:98
msgid "Select attribute to customize"
msgstr ""

#: includes/ivpa-settings.php:105 includes/ivpa-settings.php:135
msgid "Name"
msgstr ""

#: includes/ivpa-settings.php:107 includes/ivpa-settings.php:137
msgid "Use alternative name for the attribute"
msgstr ""

#: includes/ivpa-settings.php:112 includes/ivpa-settings.php:142
msgid "Description"
msgstr ""

#: includes/ivpa-settings.php:114 includes/ivpa-settings.php:144
msgid "Enter description for current attribute"
msgstr ""

#: includes/ivpa-settings.php:119 includes/ivpa-settings.php:384
msgid "Attribute Selection Support"
msgstr ""

#: includes/ivpa-settings.php:121
msgid ""
"Use attribute selection for all product types (Works with Attribute "
"Selection Support)"
msgstr ""

#: includes/ivpa-settings.php:126 includes/ivpa-settings.php:255
msgid "Shop Display Mode"
msgstr ""

#: includes/ivpa-settings.php:128 includes/ivpa-settings.php:187
msgid ""
"Show on Shop Pages (Works with Shop Display Mode set to Show Available "
"Options Only)"
msgstr ""

#: includes/ivpa-settings.php:149
msgid "Add Price"
msgstr ""

#: includes/ivpa-settings.php:151
msgid "Add-on price if option is used by customer"
msgstr ""

#: includes/ivpa-settings.php:156
msgid "Limit to Product Type"
msgstr ""

#: includes/ivpa-settings.php:158
msgid "Enter product types separated by | Sample: &rarr; "
msgstr ""

#: includes/ivpa-settings.php:163
msgid "Limit to Product Category"
msgstr ""

#: includes/ivpa-settings.php:165
msgid "Enter product category IDs separated by | Sample: &rarr; "
msgstr ""

#: includes/ivpa-settings.php:170
msgid "Limit to Products"
msgstr ""

#: includes/ivpa-settings.php:172
msgid "Enter product IDs separated by | Sample: &rarr; "
msgstr ""

#: includes/ivpa-settings.php:178
msgid "Multiselect"
msgstr ""

#: includes/ivpa-settings.php:180
msgid "Use multi select on this option"
msgstr ""

#: includes/ivpa-settings.php:185
msgid "Shop/Archive Display Mode"
msgstr ""

#: includes/ivpa-settings.php:196
msgid "Hide WooCommerce Select Boxes"
msgstr ""

#: includes/ivpa-settings.php:198
msgid ""
"Check this option to hide default WooCommerce select boxes in Product Pages."
msgstr ""

#: includes/ivpa-settings.php:205
msgid "Hide Add To Cart Before Selection"
msgstr ""

#: includes/ivpa-settings.php:207
msgid ""
"Check this option to hide the Add To Cart button in Product Pages before the "
"selection is made."
msgstr ""

#: includes/ivpa-settings.php:214
msgid "Select Descriptions Position"
msgstr ""

#: includes/ivpa-settings.php:216
msgid "Select where to show descriptions."
msgstr ""

#: includes/ivpa-settings.php:219
msgid "After Title"
msgstr ""

#: includes/ivpa-settings.php:220
msgid "After Attributes"
msgstr ""

#: includes/ivpa-settings.php:227
msgid "AJAX Add To Cart"
msgstr ""

#: includes/ivpa-settings.php:229
msgid "Check this option to enable AJAX add to cart in Product Pages."
msgstr ""

#: includes/ivpa-settings.php:236
msgid "Use Advanced Image Switcher"
msgstr ""

#: includes/ivpa-settings.php:238
msgid ""
"Check this option to enable advanced image switcher in Single Product Pages. "
"This option enables image switch when a single attribute is selected."
msgstr ""

#: includes/ivpa-settings.php:246
msgid "Show Quantities"
msgstr ""

#: includes/ivpa-settings.php:248
msgid "Check this option to enable product quantity in Shop."
msgstr ""

#: includes/ivpa-settings.php:257
msgid "Select how to show the options in Shop Pages."
msgstr ""

#: includes/ivpa-settings.php:260
msgid "Show Available Options Only"
msgstr ""

#: includes/ivpa-settings.php:261
msgid "Enable Selection and Add to Cart"
msgstr ""

#: includes/ivpa-settings.php:268
msgid "Options Alignment"
msgstr ""

#: includes/ivpa-settings.php:270
msgid "Select options alignment in Shop Pages."
msgstr ""

#: includes/ivpa-settings.php:273
msgid "Left"
msgstr ""

#: includes/ivpa-settings.php:274
msgid "Right"
msgstr ""

#: includes/ivpa-settings.php:275
msgid "Center"
msgstr ""

#: includes/ivpa-settings.php:283
msgid "Single Product Init Action"
msgstr ""

#: includes/ivpa-settings.php:285
msgid ""
"Use custom initialization action for Single Product Pages. Use actions "
"initiated in your content-single-product.php template. Please enter action "
"name in following format action_name:priority"
msgstr ""

#: includes/ivpa-settings.php:292
msgid "Shop Init Action"
msgstr ""

#: includes/ivpa-settings.php:294
msgid ""
"Use custom initialization action for Shop/Product Archive Pages. Use actions "
"initiated in your content-single-product.php template. Please enter action "
"name in following format action_name:priority"
msgstr ""

#: includes/ivpa-settings.php:302
msgid "Single Product Image jQuery Selector"
msgstr ""

#: includes/ivpa-settings.php:304
msgid "Change default image wrapper selector in Single Product pages."
msgstr ""

#: includes/ivpa-settings.php:311
msgid "Shop Product jQuery Selector"
msgstr ""

#: includes/ivpa-settings.php:313
msgid "Change default product selector in Shop."
msgstr ""

#: includes/ivpa-settings.php:320
msgid "Shop Add To Cart jQuery Selector"
msgstr ""

#: includes/ivpa-settings.php:322
msgid "Change default add to cart selector in Shop."
msgstr ""

#: includes/ivpa-settings.php:329
msgid "Shop Price jQuery Selector"
msgstr ""

#: includes/ivpa-settings.php:331
msgid "Change default price selector in Shop."
msgstr ""

#: includes/ivpa-settings.php:340
msgid "Use Plugin In Product Pages"
msgstr ""

#: includes/ivpa-settings.php:342
msgid "Check this option to use the plugin selectors in Single Product Pages."
msgstr ""

#: includes/ivpa-settings.php:349
msgid "Use Plugin In Shop"
msgstr ""

#: includes/ivpa-settings.php:351
msgid "Check this option to use the plugin styled selectors in Shop Pages."
msgstr ""

#: includes/ivpa-settings.php:359
msgid "Out Of Stock Display"
msgstr ""

#: includes/ivpa-settings.php:361
msgid "Select how the to display the Out of Stock options."
msgstr ""

#: includes/ivpa-settings.php:364
msgid "Shown but not clickable"
msgstr ""

#: includes/ivpa-settings.php:365
msgid "Shown and clickable"
msgstr ""

#: includes/ivpa-settings.php:366
msgid "Hidden from pages"
msgstr ""

#: includes/ivpa-settings.php:374
msgid "Image Switching Attributes"
msgstr ""

#: includes/ivpa-settings.php:376
msgid ""
"Select attributes that will switch the product image. Available in Shop "
"Pages and in Single Product Pages if Advanced Image Switcher option is used."
msgstr ""

#: includes/ivpa-settings.php:386
msgid ""
"Set this option to enable selection of attributes for products that are not "
"variable."
msgstr ""

#: includes/ivpa-settings.php:389
msgid "Only Variable Products"
msgstr ""

#: includes/ivpa-settings.php:390
msgid "All Products - Registered Attributes"
msgstr ""

#: includes/ivpa-settings.php:391
msgid "All Products - Registered Attributes and Custom Attributes"
msgstr ""

#: includes/ivpa-settings.php:398
msgid "Step Selection"
msgstr ""

#: includes/ivpa-settings.php:400
msgid "Check this option to use stepped selection."
msgstr ""

#: includes/ivpa-settings.php:407
msgid "Disable Option Deselection"
msgstr ""

#: includes/ivpa-settings.php:409
msgid "Check this option not to allow option deselection/unchecking."
msgstr ""

#: includes/ivpa-settings.php:416
msgid "Backorder Notifications"
msgstr ""

#: includes/ivpa-settings.php:418
msgid ""
"Check this option to enable backorders support and show notifications about "
"them."
msgstr ""

#: includes/ivpa-settings.php:425
msgid "Plugin Scripts"
msgstr ""

#: includes/ivpa-settings.php:427
msgid ""
"Check this option to load plugin scripts in all pages. This option fixes "
"issues in Quick Views."
msgstr ""

#: includes/ivpa-settings.php:434
msgid "Use Caching"
msgstr ""

#: includes/ivpa-settings.php:436
msgid "Check this option to use product caching for better performance."
msgstr ""

#: includes/ivpa-settings.php:444
msgid "Purchase Code"
msgstr ""

#: includes/ivpa-settings.php:446
msgid ""
"Enter your purchase code to get automatic updates directly in the WP "
"Dashboard!"
msgstr ""

#: includes/update/github-checker.php:120
msgid "There is no changelog available."
msgstr ""

#: includes/update/plugin-update-checker.php:637
msgid "Check for updates"
msgstr ""

#: includes/update/plugin-update-checker.php:681
msgid "This plugin is up to date."
msgstr ""

#: includes/update/plugin-update-checker.php:683
msgid "A new version of this plugin is available."
msgstr ""

#: includes/update/plugin-update-checker.php:685
msgid "Unknown update checker status \"%s\""
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Improved Variable Product Attributes for WooCommerce"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://www.mihajlovicnenad.com/improved-variable-product-attributes"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Improved Variable Product Attributes for WooCommerce! - mihajlovicnenad.com"
msgstr ""

#. Author of the plugin/theme
msgid "Mihajlovic Nenad"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://www.mihajlovicnenad.com"
msgstr ""
