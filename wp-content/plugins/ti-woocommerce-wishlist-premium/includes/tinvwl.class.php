<?php
/**
 * Run plugin class
 *
 * @since             1.0.0
 * @package           TInvWishlist
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Run plugin class
 */
class TInvWL {

	/**
	 * Plugin name
	 *
	 * @var string
	 */
	private $_n;
	/**
	 * Plugin version
	 *
	 * @var string
	 */
	private $_v;
	/**
	 * Admin class
	 *
	 * @var TInvWL_Admin_TInvWL
	 */
	public $object_admin;
	/**
	 * Public class
	 *
	 * @var TInvWL_Public_TInvWL
	 */
	public $object_public;

	/**
	 * Constructor
	 * Created admin and public class
	 */
	function __construct() {
		$this->_n			 = TINVWL_PREFIX;
		$this->_v			 = TINVWL_VERSION;

		$this->set_locale();
		TIUpdater( $this->_n, $this->_v );
		$this->maybe_update();
		$this->load_function();
		$this->define_hooks();
		$this->object_admin	 = new TInvWL_Admin_TInvWL( $this->_n, $this->_v );
		$this->object_public = TInvWL_Public_TInvWL::instance( $this->_n, $this->_v );
	}

	/**
	 * Run plugin
	 */
	function run() {
		$object = null;
		TInvWL_View::_init( $this->_n, $this->_v );
		TInvWL_Form::_init( $this->_n );
		if ( is_admin() ) {
			new TInvWL_WizardSetup( $this->_n, $this->_v );
			$object = $this->object_admin;
		} else {
			$object = $this->object_public;
		}
		$object->load_function();
	}

	/**
	 * Set localization
	 */
	private function set_locale() {
		$locale	 = apply_filters( 'plugin_locale', get_locale(), TINVWL_DOMAIN );
		$mofile	 = sprintf( '%1$s-%2$s.mo', TINVWL_DOMAIN, $locale );
		$mofiles = array();

		$mofiles[]	 = WP_LANG_DIR . DIRECTORY_SEPARATOR . basename( TINVWL_PATH ) . DIRECTORY_SEPARATOR . $mofile;
		$mofiles[]	 = WP_LANG_DIR . DIRECTORY_SEPARATOR . 'plugins' . DIRECTORY_SEPARATOR . $mofile;
		$mofiles[]	 = TINVWL_PATH . 'languages' . DIRECTORY_SEPARATOR . $mofile;
		foreach ( $mofiles as $mofile ) {
			if ( file_exists( $mofile ) && load_textdomain( TINVWL_DOMAIN, $mofile ) ) {
				return;
			}
		}

		load_plugin_textdomain( TINVWL_DOMAIN, false, basename( TINVWL_PATH ) . DIRECTORY_SEPARATOR . 'languages' );
	}

	/**
	 * Define hooks
	 */
	function define_hooks() {

	}

	/**
	 * Load function
	 */
	function load_function() {
		if ( tinv_get_option( 'template_checker', 'checked' ) ) {
			return;
		}
		TInvWL_CheckerHook::instance();
	}

	/**
	 * Testing for the ability to update the functional
	 */
	function maybe_update() {
		$current = 'p.' . $this->_v;
		$prev	 = $this->get_current_version( $current );
		$prev_p	 = get_option( $this->_n . '_verp' );
		if ( false === $prev_p ) {
			add_option( $this->_n . '_verp', $this->_v );
		}
		if ( version_compare( $current, $prev, 'gt' ) ) {
			new TInvWL_Update( $current, $prev );
			TInvWL_Activator::activate();
			update_option( $this->_n . '_verp', $this->_v );
			do_action( 'tinvwl_updated', $current, $prev );
		}
	}

	/**
	 * Get current version
	 *
	 * @param string $version Default Version.
	 * @return string
	 */
	function get_current_version( $version = false ) {
		$_version = get_option( $this->_n . '_verp' );
		if ( false !== $_version ) {
			return 'p.' . $_version;
		}
		$_version = get_option( $this->_n . '_ver' );
		if ( false !== $_version ) {
			return 'f.' . $_version;
		}
		return $version;
	}
}
