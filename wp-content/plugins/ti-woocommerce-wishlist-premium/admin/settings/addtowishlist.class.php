<?php
/**
 * Admin settings class
 *
 * @since             1.0.0
 * @package           TInvWishlist\Admin
 * @subpackage        Settings
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Admin settings class
 */
class TInvWL_Admin_Settings_Addtowishlist extends TInvWL_Admin_BaseSection {

	/**
	 * Priority for admin menu
	 *
	 * @var integer
	 */
	public $priority = 30;

	/**
	 * Menu array
	 *
	 * @return array
	 */
	function menu() {
		return array(
			'title'		 => __( 'Button Options', 'ti-woocommerce-wishlist-premium' ),
			'page_title' => __( '"Add to Wishlist" Button Options', 'ti-woocommerce-wishlist-premium' ),
			'method'	 => array( $this, '_print_' ),
			'slug'		 => 'button-settings',
		);
	}

	/**
	 * Create Scetions for this settings
	 *
	 * @return array
	 */
	function constructor_data() {
		return array(
			array(
				'id'		 => 'add_to_wishlist',
				'title'		 => __( 'Product page "Add to Wishlist" Button Settings', 'ti-woocommerce-wishlist-premium' ),
				'show_names' => true,
				'fields'	 => array(
					array(
						'type'		 => 'select',
						'name'		 => 'position',
						'text'		 => __( 'Button position', 'ti-woocommerce-wishlist-premium' ),
						'desc'		 => __( 'Add this shortcode <code>[ti_wishlists_addtowishlist]</code> anywhere on product page, if you have chosen custom position for product button. You will have to do this for each product.', 'ti-woocommerce-wishlist-premium' ),
						'std'		 => 'after',
						'options'	 => array(
							'after'		 => __( 'After "Add to Cart" button', 'ti-woocommerce-wishlist-premium' ),
							'before'	 => __( 'Before "Add to Cart" button', 'ti-woocommerce-wishlist-premium' ),
							'shortcode'	 => __( 'Custom position with code', 'ti-woocommerce-wishlist-premium' ),
						),
					),
					array(
						'type'		 => 'select',
						'name'		 => 'type',
						'text'		 => __( 'Button type', 'ti-woocommerce-wishlist-premium' ),
						'std'		 => 'button',
						'options'	 => array(
							'link'	 => __( 'Link', 'ti-woocommerce-wishlist-premium' ),
							'button' => __( 'Button', 'ti-woocommerce-wishlist-premium' ),
						),
					),
					array(
						'type'		 => 'select',
						'name'		 => 'icon',
						'text'		 => __( '"Add to Wishlist" Icon', 'ti-woocommerce-wishlist-premium' ),
						'desc'		 => __( 'You can choose from our predefined icons or upload your custom icon. Custom icon size is limited to 16x16 px.', 'ti-woocommerce-wishlist-premium' ),
						'std'		 => 'font-icon',
						'options'	 => array(
							''			 => __( 'None', 'ti-woocommerce-wishlist-premium' ),
							'heart'		 => __( 'Heart', 'ti-woocommerce-wishlist-premium' ),
							'heart-plus' => __( 'Heart+', 'ti-woocommerce-wishlist-premium' ),
							'font-icon'	 => __( 'Font Icon', 'ti-woocommerce-wishlist-premium' ),
							'custom'	 => __( 'Custom', 'ti-woocommerce-wishlist-premium' ),
						),
					),
					array(
						'type'			 => 'uploadfile',
						'name'			 => 'icon_upload',
						'std'			 => '',
						'text'			 => ' ',
						'extra'			 => array(
							'button' => array(
								'value' => __( 'Upload', 'ti-woocommerce-wishlist-premium' ),
							),
							'type'	 => array( 'image' ),
						),
						'tiwl-required'	 => array(
							'add_to_wishlist-icon' => 'custom',
						),
					),
					array(
						'type'			 => 'select',
						'name'			 => 'icon_style',
						'std'			 => 'black',
						'text'			 => __( '"Add to Wishlist" Icon Color', 'ti-woocommerce-wishlist-premium' ),
						'options'		 => array(
							'black'	 => __( 'Black', 'ti-woocommerce-wishlist-premium' ),
							'white'	 => __( 'White', 'ti-woocommerce-wishlist-premium' ),
						),
						'tiwl-required'	 => array(
							'add_to_wishlist-icon' => array( 'heart', 'heart-plus' ),
						),
					),
					array(
						'type'			 => 'color_important',
						'name'			 => 'font_icon_color',
						'std'			 => '#909090',
						'text'			 => __( '"Add to Wishlist" Icon Color', 'ti-woocommerce-wishlist-premium' ),
						'tiwl-required'	 => array(
							'add_to_wishlist-icon' => 'font-icon',
						),
					),
					array(
						'type'			 => 'color_important',
						'name'			 => 'font_iconhover_color',
						'std'			 => '#000000',
						'text'			 => __( '"Add to Wishlist" Icon Hover/Active Color', 'ti-woocommerce-wishlist-premium' ),
						'tiwl-required'	 => array(
							'add_to_wishlist-icon' => 'font-icon',
						),
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'show_text',
						'text'	 => __( 'Show button text', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
						'extra'	 => array(
							'tiwl-show' => '.tiwl-button-text',
						),
					),
					array(
						'type'	 => 'group',
						'id'	 => 'show_text_single',
						'class'	 => 'tiwl-button-text',
						'style'	 => 'border-top: 0px; padding-top: 0px;',
					),
					array(
						'type'	 => 'text',
						'name'	 => 'text',
						'text'	 => __( '"Add to Wishlist" button Text', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => 'Add to Wishlist',
					),
					array(
						'type'	 => 'text',
						'name'	 => 'text_remove',
						'text'	 => __( '"Remove from Wishlist" Button Text', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => 'Remove from Wishlist',
						'style'	 => tinv_get_option_admin( 'general', 'simple_flow' ) ? '' : 'display:none',
					),
				),
			),
			array(
				'id'		 => 'add_to_wishlist_catalog',
				'title'		 => __( 'Product listing Button Settings', 'ti-woocommerce-wishlist-premium' ),
				'desc'		 => __( 'This is separate settings for "Add to wishlist" button on product listing (Shop page, categories, etc.). You can also adjust button and text colors, size, etc. in <code>TI Wishlist > Style Options.</code>', 'ti-woocommerce-wishlist-premium' ),
				'show_names' => true,
				'fields'	 => array(
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'show_in_loop',
						'text'	 => __( 'Show in Product Listing', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
						'extra'	 => array(
							'tiwl-show' => '.tiwl-buttoncat-button',
						),
					),
					array(
						'type'	 => 'group',
						'id'	 => 'add_to_wishlist_catalog',
						'class'	 => 'tiwl-buttoncat-button',
						'style'	 => 'border-top: 0px; padding-top: 0px;',
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'default_variation',
						'text'	 => __( 'Show for variable products', 'ti-woocommerce-wishlist-premium' ),
						'desc'	 => __( 'Will show "Add to wishlist" button for variable products if product variations are set by default.', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => false,
					),
					array(
						'type'		 => 'select',
						'name'		 => 'position',
						'text'		 => __( 'Button position', 'ti-woocommerce-wishlist-premium' ),
						'std'		 => 'after',
						'options'	 => array(
							'after'			 => __( 'After "Add to Cart" button', 'ti-woocommerce-wishlist-premium' ),
							'before'		 => __( 'Before "Add to Cart" button', 'ti-woocommerce-wishlist-premium' ),
							'above_thumb'	 => __( 'Above Thumbnail', 'ti-woocommerce-wishlist-premium' ),
							'shortcode'		 => __( 'Custom position with code', 'ti-woocommerce-wishlist-premium' ),
						),
					),
					array(
						'type'		 => 'select',
						'name'		 => 'type',
						'text'		 => __( 'Button type', 'ti-woocommerce-wishlist-premium' ),
						'std'		 => 'button',
						'options'	 => array(
							'link'	 => __( 'Link', 'ti-woocommerce-wishlist-premium' ),
							'button' => __( 'Button', 'ti-woocommerce-wishlist-premium' ),
						),
					),
					array(
						'type'		 => 'select',
						'name'		 => 'icon',
						'text'		 => __( '"Add to Wishlist" Icon', 'ti-woocommerce-wishlist-premium' ),
						'std'		 => 'font-icon',
						'options'	 => array(
							''			 => __( 'None', 'ti-woocommerce-wishlist-premium' ),
							'heart'		 => __( 'Heart', 'ti-woocommerce-wishlist-premium' ),
							'heart-plus' => __( 'Heart+', 'ti-woocommerce-wishlist-premium' ),
							'font-icon'	 => __( 'Font Icon', 'ti-woocommerce-wishlist-premium' ),
							'custom'	 => __( 'Custom', 'ti-woocommerce-wishlist-premium' ),
						),
						'desc'		 => __( 'You can choose from our predefined icons or upload your custom icon. Custom icon size is limited to 16x16 px.', 'ti-woocommerce-wishlist-premium' ),
					),
					array(
						'type'			 => 'uploadfile',
						'name'			 => 'icon_upload',
						'std'			 => '',
						'text'			 => ' ',
						'extra'			 => array(
							'button' => array(
								'value' => __( 'Upload', 'ti-woocommerce-wishlist-premium' ),
							),
							'type'	 => array( 'image' ),
						),
						'tiwl-required'	 => array(
							'add_to_wishlist_catalog-icon' => 'custom',
						),
					),
					array(
						'type'			 => 'select',
						'name'			 => 'icon_style',
						'std'			 => 'black',
						'text'			 => __( '"Add to Wishlist" Icon Color', 'ti-woocommerce-wishlist-premium' ),
						'options'		 => array(
							'black'	 => __( 'Black', 'ti-woocommerce-wishlist-premium' ),
							'white'	 => __( 'White', 'ti-woocommerce-wishlist-premium' ),
						),
						'tiwl-required'	 => array(
							'add_to_wishlist_catalog-icon' => array( 'heart', 'heart-plus' ),
						),
					),
					array(
						'type'			 => 'color_important',
						'name'			 => 'font_icon_color',
						'std'			 => '#909090',
						'text'			 => __( '"Add to Wishlist" Icon Color', 'ti-woocommerce-wishlist-premium' ),
						'tiwl-required'	 => array(
							'add_to_wishlist_catalog-icon' => 'font-icon',
						),
					),
					array(
						'type'			 => 'color_important',
						'name'			 => 'font_iconhover_color',
						'std'			 => '#000000',
						'text'			 => __( '"Add to Wishlist" Icon Hover/Active Color', 'ti-woocommerce-wishlist-premium' ),
						'tiwl-required'	 => array(
							'add_to_wishlist_catalog-icon' => 'font-icon',
						),
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'show_text',
						'text'	 => __( 'Show button text', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
						'extra'	 => array(
							'tiwl-show' => '.tiwl-button-text-catalog',
						),
					),
					array(
						'type'	 => 'group',
						'id'	 => 'show_text_catalog',
						'class'	 => 'tiwl-button-text-catalog',
						'style'	 => 'border-top: 0px; padding-top: 0px;',
					),
					array(
						'type'	 => 'text',
						'name'	 => 'text',
						'text'	 => __( '"Add to Wishlist" Text', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => 'Add to Wishlist',
					),
					array(
						'type'	 => 'text',
						'name'	 => 'text_remove',
						'text'	 => __( '"Remove from Wishlist" Button Text', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => 'Remove from Wishlist',
						'style'	 => tinv_get_option_admin( 'general', 'simple_flow' ) ? '' : 'display:none',
					),
				),
			),
			array(
				'id'		 => 'add_to_wishlist_cart',
				'title'		 => __( 'Cart page Button Settings', 'ti-woocommerce-wishlist-premium' ),
				'desc'		 => __( 'This is separate settings for "Save for Later" button on a Cart page. You can also adjust button and text colors, size, etc. in <code>TI Wishlist > Style Options.</code>', 'ti-woocommerce-wishlist-premium' ),
				'show_names' => true,
				'fields'	 => array(
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'item_show_in_cart',
						'text'	 => __( 'Show button for Cart Items', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
						'desc'	 => __( 'Enables/disables "Save for Later" button for every cart item in a table.', 'ti-woocommerce-wishlist-premium' ),
						'extra'	 => array(
							'tiwl-show' => '.tiwl-buttoncart-item-button',
						),
					),
					array(
						'type'	 => 'group',
						'id'	 => 'add_to_wishlist_in_cart_item',
						'class'	 => 'tiwl-buttoncart-item-button',
						'style'	 => 'border-top: 0px; padding-top: 0px;',
					),
					array(
						'type'		 => 'select',
						'name'		 => 'item_type',
						'text'		 => __( 'Button type', 'ti-woocommerce-wishlist-premium' ),
						'std'		 => 'button',
						'options'	 => array(
							'link'	 => __( 'Link', 'ti-woocommerce-wishlist-premium' ),
							'button' => __( 'Button', 'ti-woocommerce-wishlist-premium' ),
						),
					),
					array(
						'type'		 => 'select',
						'name'		 => 'item_icon',
						'text'		 => __( '"Save for Later" Icon', 'ti-woocommerce-wishlist-premium' ),
						'std'		 => 'font-icon',
						'options'	 => array(
							''			 => __( 'None', 'ti-woocommerce-wishlist-premium' ),
							'heart'		 => __( 'Heart', 'ti-woocommerce-wishlist-premium' ),
							'heart-plus' => __( 'Heart+', 'ti-woocommerce-wishlist-premium' ),
							'font-icon'	 => __( 'Font Icon', 'ti-woocommerce-wishlist-premium' ),
							'custom'	 => __( 'Custom', 'ti-woocommerce-wishlist-premium' ),
						),
						'desc'		 => __( 'You can choose from our predefined icons or upload your custom icon. Custom icon size is limited to 16x16 px.', 'ti-woocommerce-wishlist-premium' ),
					),
					array(
						'type'			 => 'uploadfile',
						'name'			 => 'item_icon_upload',
						'std'			 => '',
						'text'			 => ' ',
						'extra'			 => array(
							'button' => array(
								'value' => __( 'Upload', 'ti-woocommerce-wishlist-premium' ),
							),
							'type'	 => array( 'image' ),
						),
						'tiwl-required'	 => array(
							'add_to_wishlist_cart-item_icon' => 'custom',
						),
					),
					array(
						'type'			 => 'select',
						'name'			 => 'item_icon_style',
						'std'			 => 'black',
						'text'			 => __( '"Save for Later" Icon Color', 'ti-woocommerce-wishlist-premium' ),
						'options'		 => array(
							'black'	 => __( 'Black', 'ti-woocommerce-wishlist-premium' ),
							'white'	 => __( 'White', 'ti-woocommerce-wishlist-premium' ),
						),
						'tiwl-required'	 => array(
							'add_to_wishlist_cart-item_icon' => array( 'heart', 'heart-plus' ),
						),
					),
					array(
						'type'			 => 'color_important',
						'name'			 => 'font_item_icon_color',
						'std'			 => '#909090',
						'text'			 => __( '"Save for Later" Icon Color', 'ti-woocommerce-wishlist-premium' ),
						'tiwl-required'	 => array(
							'add_to_wishlist_cart-item_icon' => 'font-icon',
						),
					),
					array(
						'type'			 => 'color_important',
						'name'			 => 'font_item_iconhover_color',
						'std'			 => '#000000',
						'text'			 => __( '"Save for Later" Icon Hover/Active Color', 'ti-woocommerce-wishlist-premium' ),
						'tiwl-required'	 => array(
							'add_to_wishlist_cart-item_icon' => 'font-icon',
						),
					),
					array(
						'type'	 => 'text',
						'name'	 => 'item_text',
						'text'	 => __( '"Save for Later" Text', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => 'Save for later',
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'item_remove_from_cart',
						'text'	 => __( 'Remove from cart', 'ti-woocommerce-wishlist-premium' ),
						'desc'	 => __( 'Remove from cart when products are added to wishlist.', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => false,
					),
					array(
						'type'	 => 'group',
						'id'	 => 'show_in_cart',
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'show_in_cart',
						'text'	 => __( 'Show button for Cart', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
						'desc'	 => __( 'Enables/disables "Save Cart" button that allows to add all cart items to wishlist in one click.', 'ti-woocommerce-wishlist-premium' ),
						'extra'	 => array(
							'tiwl-show' => '.tiwl-buttoncart-button',
						),
					),
					array(
						'type'	 => 'group',
						'id'	 => 'add_to_wishlist_in_cart',
						'class'	 => 'tiwl-buttoncart-button',
						'style'	 => 'border-top: 0px; padding-top: 0px;',
					),
					array(
						'type'		 => 'select',
						'name'		 => 'type',
						'text'		 => __( 'Button type', 'ti-woocommerce-wishlist-premium' ),
						'std'		 => 'button',
						'options'	 => array(
							'link'	 => __( 'Link', 'ti-woocommerce-wishlist-premium' ),
							'button' => __( 'Button', 'ti-woocommerce-wishlist-premium' ),
						),
					),
					array(
						'type'		 => 'select',
						'name'		 => 'icon',
						'text'		 => __( '"Save Cart" Icon', 'ti-woocommerce-wishlist-premium' ),
						'std'		 => 'font-icon',
						'options'	 => array(
							''			 => __( 'None', 'ti-woocommerce-wishlist-premium' ),
							'heart'		 => __( 'Heart', 'ti-woocommerce-wishlist-premium' ),
							'heart-plus' => __( 'Heart+', 'ti-woocommerce-wishlist-premium' ),
							'font-icon'	 => __( 'Font Icon', 'ti-woocommerce-wishlist-premium' ),
							'custom'	 => __( 'Custom', 'ti-woocommerce-wishlist-premium' ),
						),
						'desc'		 => __( 'You can choose from our predefined icons or upload your custom icon. Custom icon size is limited to 16x16 px.', 'ti-woocommerce-wishlist-premium' ),
					),
					array(
						'type'			 => 'uploadfile',
						'name'			 => 'icon_upload',
						'std'			 => '',
						'text'			 => ' ',
						'extra'			 => array(
							'button' => array(
								'value' => __( 'Upload', 'ti-woocommerce-wishlist-premium' ),
							),
							'type'	 => array( 'image' ),
						),
						'tiwl-required'	 => array(
							'add_to_wishlist_cart-icon' => 'custom',
						),
					),
					array(
						'type'			 => 'select',
						'name'			 => 'icon_style',
						'std'			 => 'black',
						'text'			 => __( '"Save Cart" Icon Color', 'ti-woocommerce-wishlist-premium' ),
						'options'		 => array(
							'black'	 => __( 'Black', 'ti-woocommerce-wishlist-premium' ),
							'white'	 => __( 'White', 'ti-woocommerce-wishlist-premium' ),
						),
						'tiwl-required'	 => array(
							'add_to_wishlist_cart-icon' => array( 'heart', 'heart-plus' ),
						),
					),
					array(
						'type'			 => 'color_important',
						'name'			 => 'font_icon_color',
						'std'			 => '#909090',
						'text'			 => __( '"Save Cart" Icon Color', 'ti-woocommerce-wishlist-premium' ),
						'tiwl-required'	 => array(
							'add_to_wishlist_cart-icon' => 'font-icon',
						),
					),
					array(
						'type'			 => 'color_important',
						'name'			 => 'font_iconhover_color',
						'std'			 => '#000000',
						'text'			 => __( '"Save Cart" Icon Hover/Active Color', 'ti-woocommerce-wishlist-premium' ),
						'tiwl-required'	 => array(
							'add_to_wishlist_cart-icon' => 'font-icon',
						),
					),
					array(
						'type'	 => 'text',
						'name'	 => 'text',
						'text'	 => __( '"Save Cart" Text', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => 'Save Cart',
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'remove_from_cart',
						'text'	 => __( 'Remove from cart', 'ti-woocommerce-wishlist-premium' ),
						'desc'	 => __( 'Remove from cart when products are added to wishlist.', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => false,
					),
				),
			),
			array(
				'id'	 => 'save_buttons',
				'class'	 => 'only-button',
				'noform' => true,
				'fields' => array(
					array(
						'type'	 => 'button_submit',
						'name'	 => 'setting_save',
						'std'	 => '<span><i class="fa fa-check"></i></span>' . __( 'Save Settings', 'ti-woocommerce-wishlist-premium' ),
						'extra'	 => array( 'class' => 'tinvwl-btn split status-btn-ok' ),
					),
					array(
						'type'	 => 'button_submit',
						'name'	 => 'setting_reset',
						'std'	 => '<span><i class="fa fa-times"></i></span>' . __( 'Reset', 'ti-woocommerce-wishlist-premium' ),
						'extra'	 => array( 'class' => 'tinvwl-btn split status-btn-ok tinvwl-confirm-reset' ),
					),
					array(
						'type'	 => 'button_submit_quick',
						'name'	 => 'setting_save_quick',
						'std'	 => '<span><i class="fa fa-floppy-o"></i></span>' . __( 'Save', 'ti-woocommerce-wishlist-premium' ),
					),
				),
			),
		);
	}

	/**
	 * Save value to database
	 *
	 * @param array $data Post section data.
	 */
	function constructor_save( $data ) {
		if ( filter_input( INPUT_POST, 'save_buttons-setting_reset' ) ) {
			foreach ( $data as $key => $value ) {
				$data[ $key ] = array();
			}
		}
		parent::constructor_save( $data );
		$this->save_icon_color();
		if ( 'custom' !== tinv_get_option( 'add_to_wishlist', 'icon' ) ) {
			tinv_update_option( 'add_to_wishlist', 'icon_upload', '' );
		}
	}

	/**
	 * Save global optins if change style
	 */
	function save_icon_color() {
		delete_transient( TINVWL_PREFIX . '_dynamicfont' );
		if ( tinv_get_option( 'style', 'customstyle' ) ) {
			return;
		}
		$template		 = tinv_template();
		$style_options	 = 'style_options' . $template;

		$color = tinv_get_option( 'add_to_wishlist_catalog', 'font_icon_color' );
		tinv_update_option( $style_options, TInvWL_Admin_BaseStyle::create_selectorkey( '.tinv-wishlist .tinvwl_add_to_wishlist_button.tinvwl-icon-font-icon:before,.woocommerce ul.products li a.button.tinvwl-icon-font-icon.tinvwl_add_to_wishlist_button:before,.woocommerce ul.products li.product a.button.tinvwl-icon-font-icon.tinvwl_add_to_wishlist_button:before', 'color' ), $color );
		$color = tinv_get_option( 'add_to_wishlist_catalog', 'font_iconhover_color' );
		tinv_update_option( $style_options, TInvWL_Admin_BaseStyle::create_selectorkey( '.tinv-wishlist .tinvwl_add_to_wishlist_button.tinvwl-icon-font-icon:hover:before,.woocommerce ul.products li a.button.tinvwl-icon-font-icon.tinvwl_add_to_wishlist_button:hover:before,.woocommerce ul.products li.product a.button.tinvwl-icon-font-icon.tinvwl_add_to_wishlist_button:hover:before,.tinv-wishlist .tinvwl_add_to_wishlist_button.tinvwl-icon-font-icon.tinvwl-product-in-list:before,.woocommerce ul.products li a.button.tinvwl-icon-font-icon.tinvwl_add_to_wishlist_button.tinvwl-product-in-list:before,.woocommerce ul.products li.product a.button.tinvwl-icon-font-icon.tinvwl_add_to_wishlist_button.tinvwl-product-in-list:before', 'color' ), $color );

		$color = tinv_get_option( 'add_to_wishlist', 'font_icon_color' );
		tinv_update_option( $style_options, TInvWL_Admin_BaseStyle::create_selectorkey( '.woocommerce div.product form.cart a.tinvwl_add_to_wishlist_button:before', 'color' ), $color );
		$color = tinv_get_option( 'add_to_wishlist', 'font_iconhover_color' );
		tinv_update_option( $style_options, TInvWL_Admin_BaseStyle::create_selectorkey( '.woocommerce div.product form.cart a.tinvwl_add_to_wishlist_button:hover:before,.woocommerce div.product form.cart a.tinvwl_add_to_wishlist_button.tinvwl-product-in-list:before', 'color' ), $color );

		$color = tinv_get_option( 'add_to_wishlist_cart', 'font_item_icon_color' );
		tinv_update_option( $style_options, TInvWL_Admin_BaseStyle::create_selectorkey( '.woocommerce-cart .tinv-wishlist .tinvwl_cart_to_wishlist_button.tinvwl-icon-font-icon:before', 'color' ), $color );
		$color = tinv_get_option( 'add_to_wishlist_cart', 'font_item_iconhover_color' );
		tinv_update_option( $style_options, TInvWL_Admin_BaseStyle::create_selectorkey( '.woocommerce-cart .tinv-wishlist .tinvwl_cart_to_wishlist_button.tinvwl-icon-font-icon:hover:before,.woocommerce-cart .tinv-wishlist .tinvwl_cart_to_wishlist_button.tinvwl-icon-font-icon.tinvwl-product-in-list:before', 'color' ), $color );

		$color = tinv_get_option( 'add_to_wishlist_cart', 'font_icon_color' );
		tinv_update_option( $style_options, TInvWL_Admin_BaseStyle::create_selectorkey( '.tinv-wishlist .tinvwl_all_cart_to_wishlist_button.tinvwl-icon-font-icon:before', 'color' ), $color );
		$color = tinv_get_option( 'add_to_wishlist_cart', 'font_iconhover_color' );
		tinv_update_option( $style_options, TInvWL_Admin_BaseStyle::create_selectorkey( '.tinv-wishlist .tinvwl_all_cart_to_wishlist_button.tinvwl-icon-font-icon:hover:before,.tinv-wishlist .tinvwl_all_cart_to_wishlist_button.tinvwl-icon-font-icon.tinvwl-product-in-list:before', 'color' ), $color );
		TInvWL_Admin_Settings_Style::convert_styles( $template );
		delete_transient( $this->_n . '_dynamic_' . $template );
	}
}
