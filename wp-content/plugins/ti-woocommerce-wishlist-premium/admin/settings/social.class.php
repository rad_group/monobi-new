<?php
/**
 * Admin settings class
 *
 * @since             1.0.0
 * @package           TInvWishlist\Admin
 * @subpackage        Settings
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Admin settings class
 */
class TInvWL_Admin_Settings_Social extends TInvWL_Admin_BaseSection {

	/**
	 * Priority for admin menu
	 *
	 * @var integer
	 */
	public $priority = 50;

	/**
	 * Menu array
	 *
	 * @return array
	 */
	function menu() {
		return array(
			'title'		 => __( 'Sharing Options', 'ti-woocommerce-wishlist-premium' ),
			'page_title' => __( 'Social Networks Sharing Options', 'ti-woocommerce-wishlist-premium' ),
			'method'	 => array( $this, '_print_' ),
			'slug'		 => 'social-settings',
		);
	}

	/**
	 * Create Scetions for this settings
	 *
	 * @return array
	 */
	function constructor_data() {
		return array(
			array(
				'id'		 => 'social',
				'show_names' => false,
				'fields'	 => array(
					array(
						'type'		 => 'group',
						'id'		 => 'social',
						'desc'		 => __( 'Following options enable/disable Social share icons below wishlist table on wishlist page. Wishlist owner can easily share their wishlists using this button on social networks. Wishlist privacy should be set to public or shared status, private wishlists can\'t be shared.', 'ti-woocommerce-wishlist-premium' ),
						'class'		 => 'tinvwl-info-top',
					),
					array(
						'type'	 => 'html',
						'name'	 => 'social',
						'text'	 => __( 'Social Networks Sharing Options', 'ti-woocommerce-wishlist-premium' ),
						'class'	 => 'tinvwl-header-row tinvwl-line-border',
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'facebook',
						'text'	 => __( 'Show "Facebook" Button', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'twitter',
						'text'	 => __( 'Show "Twitter" Button', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'pinterest',
						'text'	 => __( 'Show "Pinterest" Button', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'google',
						'text'	 => __( 'Show "Google+" Button', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'email',
						'text'	 => __( 'Show "Share by Email" Button', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'	 => 'text',
						'name'	 => 'share_on',
						'text'	 => __( '"Share on" Text', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => 'Share on',
					),
					array(
						'type'	 => 'color_important',
						'name'	 => 'font_icon_color',
						'std'	 => '#909090',
						'text'	 => __( 'Social Icons Color', 'ti-woocommerce-wishlist-premium' ),
					),
					array(
						'type'	 => 'color_important',
						'name'	 => 'font_iconhover_color',
						'std'	 => '#000000',
						'text'	 => __( 'Social Icons Hover Color', 'ti-woocommerce-wishlist-premium' ),
					),
				),
			),
			array(
				'id'	 => 'save_buttons',
				'class'	 => 'only-button',
				'noform' => true,
				'fields' => array(
					array(
						'type'	 => 'button_submit',
						'name'	 => 'setting_save',
						'std'	 => '<span><i class="fa fa-check"></i></span>' . __( 'Save Settings', 'ti-woocommerce-wishlist-premium' ),
						'extra'	 => array( 'class' => 'tinvwl-btn split status-btn-ok' ),
					),
					array(
						'type'	 => 'button_submit',
						'name'	 => 'setting_reset',
						'std'	 => '<span><i class="fa fa-times"></i></span>' . __( 'Reset', 'ti-woocommerce-wishlist-premium' ),
						'extra'	 => array( 'class' => 'tinvwl-btn split status-btn-ok tinvwl-confirm-reset' ),
					),
					array(
						'type'	 => 'button_submit_quick',
						'name'	 => 'setting_save_quick',
						'std'	 => '<span><i class="fa fa-floppy-o"></i></span>' . __( 'Save', 'ti-woocommerce-wishlist-premium' ),
					),
				),
			),
		);
	}

	/**
	 * Save value to database
	 *
	 * @param array $data Post section data.
	 */
	function constructor_save( $data ) {
		if ( filter_input( INPUT_POST, 'save_buttons-setting_reset' ) ) {
			foreach ( $data as $key => $value ) {
				$data[ $key ] = array();
			}
		}
		parent::constructor_save( $data );
		$this->save_icon_color();
	}

	/**
	 * Save global optins if change style
	 */
	function save_icon_color() {
		$template		 = tinv_template();
		$style_options	 = 'style_options' . $template;

		$color = tinv_get_option( 'social', 'font_icon_color' );
		tinv_update_option( $style_options, TInvWL_Admin_BaseStyle::create_selectorkey( '.tinv-wishlist .social-buttons li a:before', 'color' ), $color );
		$color = tinv_get_option( 'social', 'font_iconhover_color' );
		tinv_update_option( $style_options, TInvWL_Admin_BaseStyle::create_selectorkey( '.tinv-wishlist .social-buttons li a:hover:before', 'color' ), $color );
		delete_transient( TINVWL_PREFIX . '_dynamicfont' );
	}
}
