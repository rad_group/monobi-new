<?php
/**
 * Admin settings class
 *
 * @since             1.0.0
 * @package           TInvWishlist\Admin
 * @subpackage        Settings
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Admin settings class
 */
class TInvWL_Admin_Settings_Table extends TInvWL_Admin_BaseSection {

	/**
	 * Priority for admin menu
	 *
	 * @var integer
	 */
	public $priority = 40;

	/**
	 * Menu array
	 *
	 * @return array
	 */
	function menu() {
		return array(
			'title'		 => __( 'Table Options', 'ti-woocommerce-wishlist-premium' ),
			'page_title' => __( 'Wishlist Table Options', 'ti-woocommerce-wishlist-premium' ),
			'method'	 => array( $this, '_print_' ),
			'slug'		 => 'table-settings',
		);
	}

	/**
	 * Create Scetions for this settings
	 *
	 * @return array
	 */
	function constructor_data() {
		return array(
			array(
				'id'		 => 'product_table',
				'title'		 => __( 'Product settings', 'ti-woocommerce-wishlist-premium' ),
				'desc'		 => __( 'Following options allows you to choose what information/functionality to show/enable in wishlist table on wishlist page.', 'ti-woocommerce-wishlist-premium' ),
				'show_names' => true,
				'fields'	 => array(
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'add_to_cart',
						'text'	 => __( 'Show "Add to Cart" button', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
						'extra'	 => array( 'tiwl-show' => '.tiwl-table-action-addcart' ),
					),
					array(
						'type'	 => 'text',
						'name'	 => 'text_add_to_cart',
						'text'	 => __( '"Add to Cart" Text', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => 'Add to Cart',
						'class'	 => 'tiwl-table-action-addcart',
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'colm_price',
						'text'	 => __( 'Show Unit price', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'colm_stock',
						'text'	 => __( 'Show Stock status', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'colm_date',
						'text'	 => __( 'Show Date of addition', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'colm_quantity',
						'text'	 => __( 'Show Quantity', 'ti-woocommerce-wishlist-premium' ),
						'desc'	 => __( 'This option enables quantity functionality on wishlist page. If customer adds the same product to the same wishlist more than one time, he will receive notification that this product is already on the wishlist. But if quantity enabled, it will not show any notifications but increase quality of this product in wishlist instead', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'move',
						'text'	 => __( 'Show "Move" button', 'ti-woocommerce-wishlist-premium' ),
						'desc'	 => __( 'This option allows customers to move products between their wishlists', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
				),
			),
			array(
				'id'		 => 'table',
				'title'		 => __( 'Table settings', 'ti-woocommerce-wishlist-premium' ),
				'desc'		 => __( 'Following options will help user to manage and add products to cart from wishlist table in bulk.', 'ti-woocommerce-wishlist-premium' ),
				'show_names' => true,
				'fields'	 => array(
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'colm_checkbox',
						'text'	 => __( 'Show Checkboxes', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
						'extra'	 => array( 'tiwl-show' => '.tiwl-table-cb-button' ),
					),
					array(
						'type'	 => 'group',
						'id'	 => 'cb_button',
						'class'	 => 'tiwl-table-cb-button',
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'colm_actions',
						'text'	 => __( 'Show Actions button', 'ti-woocommerce-wishlist-premium' ),
						'desc'	 => __( 'Bulk actions drop down at the bottom of wishlist table', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'add_select_to_cart',
						'text'	 => __( 'Show "Add Selected to Cart" button', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
						'extra'	 => array( 'tiwl-show' => '.tiwl-table-addcart-sel', 'ti-woocommerce-wishlist-premium' ),
					),
					array(
						'type'	 => 'text',
						'name'	 => 'text_add_select_to_cart',
						'text'	 => __( '"Add Selected to Cart" Button Text', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => 'Add Selected to Cart',
						'class'	 => 'tiwl-table-addcart-sel',
					),
					array(
						'type'	 => 'group',
						'id'	 => '_button',
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'add_all_to_cart',
						'text'	 => __( 'Show "Add All to Cart" button', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
						'extra'	 => array( 'tiwl-show' => '.tiwl-table-addcart-all' ),
					),
					array(
						'type'	 => 'text',
						'name'	 => 'text_add_all_to_cart',
						'text'	 => __( '"Add All to Cart" Button Text', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => 'Add All to Cart',
						'class'	 => 'tiwl-table-addcart-all',
					),
				),
			),
			array(
				'id'		 => 'navigation',
				'title'		 => __( 'Wishlist Table Navigation Buttons', 'ti-woocommerce-wishlist-premium' ),
				'desc'		 => __( 'This is options for quick navigation menu that can be placed above or/and below wishlist table.', 'ti-woocommerce-wishlist-premium' ),
				'show_names' => true,
				'fields'	 => array(
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'in_title',
						'text'	 => __( 'Show in title area', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'after_table',
						'text'	 => __( 'Show after table', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'		 => 'select',
						'name'		 => 'type',
						'text'		 => __( 'Button type', 'ti-woocommerce-wishlist-premium' ),
						'std'		 => 'link',
						'options'	 => array(
							'link'	 => __( 'Link', 'ti-woocommerce-wishlist-premium' ),
							'button' => __( 'Button', 'ti-woocommerce-wishlist-premium' ),
						),
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'icon',
						'text'	 => __( 'Show icons', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'public',
						'text'	 => __( 'Show All Wishlists button', 'ti-woocommerce-wishlist-premium' ),
						'desc'	 => __( 'This button opens a page with Recent Wishlist shortcode. You can assign page to this button in <code>TI Wishlist > General Settings > Wishlist Page Options: All Public Wishlists</code>', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
					array(
						'type'	 => 'checkboxonoff',
						'name'	 => 'searchp',
						'text'	 => __( 'Show Search button', 'ti-woocommerce-wishlist-premium' ),
						'desc'	 => __( 'This button opens a page with Search shortcode. You can assign page to this button in <code>TI Wishlist > General Settings > Wishlist Page Options: Wishlist Search Page</code>', 'ti-woocommerce-wishlist-premium' ),
						'std'	 => true,
					),
				),
			),
			array(
				'id'	 => 'save_buttons',
				'class'	 => 'only-button',
				'noform' => true,
				'fields' => array(
					array(
						'type'	 => 'button_submit',
						'name'	 => 'setting_save',
						'std'	 => '<span><i class="fa fa-check"></i></span>' . __( 'Save Settings', 'ti-woocommerce-wishlist-premium' ),
						'extra'	 => array( 'class' => 'tinvwl-btn split status-btn-ok' ),
					),
					array(
						'type'	 => 'button_submit',
						'name'	 => 'setting_reset',
						'std'	 => '<span><i class="fa fa-times"></i></span>' . __( 'Reset', 'ti-woocommerce-wishlist-premium' ),
						'extra'	 => array( 'class' => 'tinvwl-btn split status-btn-ok tinvwl-confirm-reset' ),
					),
					array(
						'type'	 => 'button_submit_quick',
						'name'	 => 'setting_save_quick',
						'std'	 => '<span><i class="fa fa-floppy-o"></i></span>' . __( 'Save', 'ti-woocommerce-wishlist-premium' ),
					),
				),
			),
		);
	}

	/**
	 * Save value to database
	 *
	 * @param array $data Post section data.
	 */
	function constructor_save( $data ) {
		if ( filter_input( INPUT_POST, 'save_buttons-setting_reset' ) ) {
			foreach ( $data as $key => $value ) {
				$data[ $key ] = array();
			}
		}
		parent::constructor_save( $data );
	}
}
