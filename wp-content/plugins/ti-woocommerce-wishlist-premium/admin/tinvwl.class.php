<?php
/**
 * Admin pages class
 *
 * @since             1.0.0
 * @package           TInvWishlist\Admin
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Admin pages class
 */
class TInvWL_Admin_TInvWL extends TInvWL_Admin_Base {

	/**
	 * Constructor
	 *
	 * @param string $plugin_name Plugin name.
	 * @param string $version Plugin version.
	 */
	function __construct( $plugin_name, $version ) {
		$this->_n	 = $plugin_name;
		$this->_v	 = $version;
	}

	/**
	 * Load functions.
	 * Create Wishlist and Product class.
	 * Load settings classes.
	 */
	function load_function() {
		$this->wishlist	 = new TInvWL_Admin_Wishlist( $this->_n, $this->_v );
		$this->product	 = new TInvWL_Admin_Product( $this->_n, $this->_v );
		$this->load_settings();

		$this->define_hooks();
	}

	/**
	 * Load settings classes.
	 *
	 * @return boolean
	 */
	function load_settings() {
		$dir = TINVWL_PATH . 'admin/settings/';
		if ( ! file_exists( $dir ) || ! is_dir( $dir ) ) {
			return false;
		}
		$files = scandir( $dir );
		foreach ( $files as $value ) {
			if ( preg_match( '/\.class\.php$/i', $value ) ) {
				$file		 = preg_replace( '/\.class\.php$/i', '', $value );
				$class		 = 'TInvWL_Admin_Settings_' . ucfirst( $file );
				$settings	 = new $class( $this->_n, $this->_v );
			}
		}
		return true;
	}

	/**
	 * Define hooks
	 */
	function define_hooks() {
		add_action( 'init', array( $this, 'preview_email' ), 1000 );
		add_action( 'admin_menu', array( $this, 'action_menu' ) );
		if ( tinv_get_option( 'notification_stock_email', 'enabled' ) ) {
			add_action( 'woocommerce_product_set_stock_status', array( $this, 'product_set_stock_status' ), 100, 2 );
			add_action( 'woocommerce_variation_set_stock_status', array( $this, 'variation_set_stock_status' ), 100, 2 );
		}
		if ( tinv_get_option( 'notification_price_email', 'enabled' ) ) {
			add_action( 'updated_postmeta', array( $this, 'updated_postmeta_price' ), 100, 4 );
		}
		add_action( $this->_n . '_send_promotional_error', array( $this, 'email_promotional_error' ), 10, 4 );
		add_action( $this->_n . '_send_promotional_successfully', array( $this, 'email_promotional_successfully' ), 10, 3 );

		add_action( 'wp_ajax_prepare_promotion', array( $this->product, 'preview_email_presave' ) );
		add_action( 'init', array( $this->product, 'preview_email' ), 1000 );
		if ( 'skip' === filter_input( INPUT_GET, $this->_n . '-wizard' ) ) {
			update_option( $this->_n . '_wizard', true );
		}
		if ( ! get_option( $this->_n . '_wizard' ) ) {
			add_action( 'admin_notices', array( $this, 'wizard_run_admin_notice' ) );
		} elseif ( ! tinv_get_option( 'page', 'wishlist' ) ) {
			add_action( 'admin_notices', array( $this, 'empty_page_admin_notice' ) );
		}
		add_action( 'woocommerce_system_status_report', array( $this, 'system_report_templates' ) );
		add_action( 'switch_theme', array( $this, 'admin_notice_outdated_templates' ) );
		add_action( 'tinvwl_updated', array( $this, 'admin_notice_outdated_templates' ) );
		add_action( 'wp_ajax_' . $this->_n . '_checker_hook', array( $this, 'validation_template' ) );
		add_action( 'switch_theme', array( $this, 'clear_notice_validation_template' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts_validate_template' ) );
		add_action( 'tinvwl_admin_promo_footer', array( $this, 'promo_footer' ) );
		add_action( 'tinvwl_remove_without_author_wishlist', array( $this, 'remove_old_wishlists' ) );
		$this->scheduled_remove_wishlist();
	}

	/**
	 * Error notice if wizard didn't run.
	 */
	function wizard_run_admin_notice() {
		printf( '<div class="notice notice-error"><p>%1$s</p><p><a href="%2$s" class="button-primary">%3$s</a> <a href="%4$s" class="button-secondary">%5$s</a></p></div>',
			__( '<strong>Welcome to TI WooCommerce Wishlist<strong> – You‘re almost ready to start :)', 'ti-woocommerce-wishlist-premium' ), // @codingStandardsIgnoreLine WordPress.XSS.EscapeOutput.OutputNotEscaped
			esc_url( admin_url( 'index.php?page=tinvwl-wizard' ) ),
			esc_html__( 'Run the Setup Wizard', 'ti-woocommerce-wishlist-premium' ),
			esc_url( admin_url( 'index.php?page=' . $this->_n . '&' . $this->_n . '-wizard=skip' ) ),
			esc_html__( 'Skip Setup', 'ti-woocommerce-wishlist-premium' )
		);
	}

	/**
	 * Error notice if wishlist page not set.
	 */
	function empty_page_admin_notice() {
		printf( '<div class="notice notice-error is-dismissible" style="position: relative;"><p>%1$s <a href="%2$s">%3$s</a>%4$s<a href="%5$s">%6$s</a></p><button type="button" class="notice-dismiss"><span class="screen-reader-text">' . __( 'Dismiss' ) . '</span></button></div>', // @codingStandardsIgnoreLine WordPress.XSS.EscapeOutput.OutputNotEscaped
			esc_html__( 'Link to Wishlists does not work!', 'ti-woocommerce-wishlist-premium' ),
			esc_url( $this->admin_url( '' ) . '#general' ),
			esc_html__( 'Please apply the Wishlist page', 'ti-woocommerce-wishlist-premium' ),
			esc_html__( ' or ', 'ti-woocommerce-wishlist-premium' ),
			esc_url( admin_url( 'index.php?page=tinvwl-wizard' ) ),
			esc_html__( 'Run the Setup Wizard', 'ti-woocommerce-wishlist-premium' )
		);
	}

	/**
	 * Check change product price
	 *
	 * @param integer $meta_id Not Used.
	 * @param integer $object_id Product or Variation ID.
	 * @param strung  $meta_key Name meta key.
	 * @param mixed   $meta_value Price value.
	 * @return boolean
	 */
	function updated_postmeta_price( $meta_id, $object_id, $meta_key, $meta_value ) {
		if ( '_price' === $meta_key ) {
			$object_id	 = absint( $object_id );
			$meta_value	 = floatval( $meta_value );
			$wlp		 = new TInvWL_Product( array(), $this->_n );
			$products	 = $wlp->get( array(
				'external'	 => false,
				'sql'		 => "SELECT * FROM `{table}` WHERE ( ( `product_id` = $object_id  AND `variation_id` = 0 ) OR `variation_id` = $object_id ) AND `price` <> $meta_value",
			) );
			if ( empty( $products ) ) {
				return false;
			}
			$wishlists	 = array();
			$product	 = null;
			foreach ( $products as $_product ) {
				$wlp->update( $_product );
				$_product['price'] = floatval( $_product['price'] );
				if ( $meta_value < $_product['price'] ) {
					$product	 = $_product;
					$wishlists[] = $_product['wishlist_id'];
				}
			}
			$wishlists = array_unique( $wishlists, SORT_NUMERIC );
			if ( ! empty( $product ) && ! empty( $wishlists ) ) {
				$_product = $wlp->product_data( $product['product_id'], $product['variation_id'] );
				if ( ! empty( $_product ) ) {
					$wl			 = new TInvWL_Wishlist( $this->_n );
					$wishlists	 = $wl->get( array(
						'ID' => $wishlists,
					) );
					$users		 = array();
					foreach ( $wishlists as $wishlist ) {
						$users[ $wishlist['author'] ][] = $wishlist['ID'];
					}
					if ( function_exists( 'WC' ) ) {
						WC()->mailer();
					}
					foreach ( $users as $user_id => $wishlists ) {
						do_action( $this->_n . '_send_notification_price', $_product, $user_id, $wishlists );
					}
				}
			}
		} // End if().
	}

	/**
	 * Check product stock status when is update
	 *
	 * @param integer $product_id Product.
	 * @param string  $status Stock Staus.
	 * @return boolean
	 */
	function product_set_stock_status( $product_id, $status = 'instock' ) {
		if ( empty( $product_id ) ) {
			return false;
		}
		$status		 = 'instock' === $status;
		$wlp		 = new TInvWL_Product( array(), $this->_n );
		$products	 = $wlp->get( array(
			'product_id'	 => $product_id,
			'variation_id'	 => 0,
			'external'		 => false,
			'in_stock'		 => $status ? 0 : 1,
		) );
		return $this->set_stock_status( $products, $status );
	}

	/**
	 * Check variation product stock status when is update
	 *
	 * @param integer $variation_id Variation product.
	 * @param string  $status Stock Staus.
	 * @return boolean
	 */
	function variation_set_stock_status( $variation_id, $status = 'instock' ) {
		if ( empty( $variation_id ) ) {
			return false;
		}
		$status		 = 'instock' === $status;
		$wlp		 = new TInvWL_Product( array(), $this->_n );
		$products	 = $wlp->get( array(
			'variation_id'	 => $variation_id,
			'external'		 => false,
			'in_stock'		 => $status ? 0 : 1,
		) );
		return $this->set_stock_status( $products, $status );
	}

	/**
	 * Update stock status in wishlist table
	 *
	 * @param array  $products Array of the products.
	 * @param string $status Stock Staus.
	 * @return boolean
	 */
	function set_stock_status( $products, $status = true ) {
		if ( empty( $products ) ) {
			return false;
		}
		$wlp		 = new TInvWL_Product( array(), $this->_n );
		$wishlists	 = array();
		$product	 = null;
		foreach ( $products as $_product ) {
			if ( $status !== $_product['in_stock'] ) {
				$wlp->update( $_product );
				$product	 = $_product;
				$wishlists[] = $_product['wishlist_id'];
			}
		}
		$wishlists = array_unique( $wishlists, SORT_NUMERIC );
		if ( ! empty( $product ) && ! empty( $wishlists ) && $status ) {
			$_product = $wlp->product_data( $product['product_id'], $product['variation_id'] );
			if ( ! empty( $_product ) && ! $_product->backorders_allowed() ) {
				$wl			 = new TInvWL_Wishlist( $this->_n );
				$wishlists	 = $wl->get( array(
					'ID' => $wishlists,
				) );
				$users		 = array();
				foreach ( $wishlists as $wishlist ) {
					do_action( 'tinvwl_changed_wishlist', ( $status ? 32 : 16 ), $wishlist, $product['product_id'], $product['variation_id'] );
					$users[ $wishlist['author'] ][] = $wishlist['ID'];
				}
				if ( $status ) {
					if ( function_exists( 'WC' ) ) {
						WC()->mailer();
					}
					foreach ( $users as $user_id => $wishlists ) {
						do_action( $this->_n . '_send_notification_stock', $_product, $user_id, $wishlists );
					}
				}
			}
		}
	}

	/**
	 * Creation mune and sub-menu
	 */
	function action_menu() {
		$page	 = add_menu_page( 'TI Wishlist', 'TI Wishlist', 'manage_options', $this->_n, array( $this->wishlist, '_print_' ), TINVWL_URL . 'asset/img/icon_menu.png', 56 );
		add_action( "load-$page", array( $this, 'onload' ) );
		$menu	 = apply_filters( $this->_n . '_admin_menu', array() );
		foreach ( $menu as $item ) {
			if ( ! array_key_exists( 'page_title', $item ) ) {
				$item['page_title'] = $item['title'];
			}
			if ( ! array_key_exists( 'parent', $item ) ) {
				$item['parent'] = $this->_n;
			}
			if ( ! array_key_exists( 'capability', $item ) ) {
				$item['capability'] = 'manage_options';
			}
			$item['slug'] = implode( '-', array_filter( array( $this->_n, $item['slug'] ) ) );

			$page = add_submenu_page( $item['parent'], $item['page_title'], $item['title'], $item['capability'], $item['slug'], $item['method'] );
			add_action( "load-$page", array( $this, 'onload' ) );
		}
	}

	/**
	 * Load style and javascript
	 */
	function onload() {
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_filter( 'admin_footer_text', array( $this, 'footer_admin' ) );
		add_filter( 'screen_options_show_screen', array( $this, 'screen_options_hide_screen' ), 10, 2 );
	}

	/**
	 * Load style
	 */
	function enqueue_styles() {
		wp_enqueue_style( 'gfonts', ( is_ssl() ? 'https' : 'http' ) . '://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800', '', null, 'all' );
		wp_enqueue_style( $this->_n, TINVWL_URL . 'asset/css/admin.css', array(), $this->_v, 'all' );
		wp_enqueue_style( $this->_n . '-font-awesome', TINVWL_URL . 'asset/css/font-awesome.min.css', array(), $this->_v, 'all' );
		wp_enqueue_style( $this->_n . '-form', TINVWL_URL . 'asset/css/admin-form.css', array( 'wp-color-picker' ), $this->_v, 'all' );
	}

	/**
	 * Load javascript
	 */
	function enqueue_scripts() {
		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		wp_enqueue_script( $this->_n . '-bootstrap', TINVWL_URL . 'asset/js/bootstrap' . $suffix . '.js', array( 'jquery' ), $this->_v, 'all' );
		wp_enqueue_script( 'wp-color-picker-alpha', TINVWL_URL . 'asset/js/wp-color-picker-alpha.min.js', array( 'wp-color-picker' ), $this->_v, 'all' );
		wp_register_script( $this->_n, TINVWL_URL . 'asset/js/admin' . $suffix . '.js', array( 'jquery', 'wp-color-picker-alpha' ), $this->_v, 'all' );
		wp_localize_script( $this->_n, 'tinvwl_comfirm', array(
			'text_comfirm_reset' => __( 'Are you sure you want to reset the settings?', 'ti-woocommerce-wishlist-premium' ),
		) );
		wp_enqueue_script( $this->_n );
	}

	/**
	 * Add plugin footer copywriting
	 */
	function footer_admin() {
		do_action( 'tinvwl_admin_promo_footer' );
	}

	/**
	 * Promo in footer for wishlist
	 */
	function promo_footer() {
		echo 'Made with <i class="fa fa-heart"></i> by <a href="https://templateinvaders.com/?utm_source=wishlist_plugin_premium&utm_campaign=made_by&utm_medium=footer">TemplateInvaders</a>';
	}

	/**
	 * Show preview email
	 */
	function preview_email() {
		$data = filter_input_array( INPUT_GET, array(
			'page'		 => FILTER_SANITIZE_STRING,
			'class'		 => FILTER_SANITIZE_STRING,
			'email'		 => FILTER_SANITIZE_STRING,
			'default'	 => FILTER_VALIDATE_BOOLEAN,
			'_wpnonce'	 => FILTER_DEFAULT,
		) );
		if ( $this->_n . '-previewemail' !== $data['page'] || ! wp_verify_nonce( $data['_wpnonce'], sprintf( '%s-%s', $this->_n, 'previewemail' ) ) ) {
			return false;
		}
		WC_Emails::instance();
		$email_class = 'TInvWL_Public_Email_' . $data['class'];
		if ( class_exists( $email_class ) ) {
			$email = new $email_class( $this->_n, $this->_v );
			$email->set_template( $data['email'] );
			tinv_wishlist_template( str_replace( DIRECTORY_SEPARATOR . 'ti-', DIRECTORY_SEPARATOR . 'ti-preview', $email->template_html ) );
		}
		die();
	}

	/**
	 * Error message for sent promotional email
	 *
	 * @param string $recipient Recipient.
	 * @param object $user	User.
	 * @param object $product Product.
	 * @param string $error Error message.
	 */
	function email_promotional_error( $recipient, $user, $product, $error ) {
		TInvWL_View::set_error( sprintf( __( 'Promotional for Product "%1$s" is not sent! %2$s', 'ti-woocommerce-wishlist-premium' ), esc_html( $this->product->get_title() ), $error ), 138 ); // WPCS: xss ok.
	}

	/**
	 * Successfully message for sent promotional email
	 *
	 * @param string $recipient Recipient.
	 * @param object $user	User.
	 * @param object $product Product.
	 */
	function email_promotional_successfully( $recipient, $user, $product ) {
		TInvWL_View::set_tips( sprintf( __( 'Promotional for Product "%s" is successfully sent!', 'ti-woocommerce-wishlist-premium' ), esc_html( $product->get_title() ) ) );
	}

	/**
	 * Templates overriding status check.
	 *
	 * @param boolean $outdated Out date status.
	 * @return string
	 */
	function templates_status_check( $outdated = false ) {

		$found_files = array();

		$scanned_files = WC_Admin_Status::scan_template_files( TINVWL_PATH . '/templates/' );

		foreach ( $scanned_files as $file ) {
			if ( file_exists( get_stylesheet_directory() . '/' . $file ) ) {
				$theme_file = get_stylesheet_directory() . '/' . $file;
			} elseif ( file_exists( get_stylesheet_directory() . '/woocommerce/' . $file ) ) {
				$theme_file = get_stylesheet_directory() . '/woocommerce/' . $file;
			} elseif ( file_exists( get_template_directory() . '/' . $file ) ) {
				$theme_file = get_template_directory() . '/' . $file;
			} elseif ( file_exists( get_template_directory() . '/woocommerce/' . $file ) ) {
				$theme_file = get_template_directory() . '/woocommerce/' . $file;
			} else {
				$theme_file = false;
			}

			if ( ! empty( $theme_file ) ) {
				$core_version	 = WC_Admin_Status::get_file_version( TINVWL_PATH . '/templates/' . $file );
				$theme_version	 = WC_Admin_Status::get_file_version( $theme_file );

				if ( $core_version && ( empty( $theme_version ) || version_compare( $theme_version, $core_version, '<' ) ) ) {
					if ( $outdated ) {
						return 'outdated';
					}
					$found_files[] = sprintf( __( '<code>%1$s</code> version <strong style="color:red">%2$s</strong> is out of date. The core version is <strong style="color:red">%3$s</strong>', 'ti-woocommerce-wishlist-premium' ), str_replace( WP_CONTENT_DIR . '/themes/', '', $theme_file ), $theme_version ? $theme_version : '-', $core_version );
				} else {
					$found_files[] = str_replace( WP_CONTENT_DIR . '/themes/', '', $theme_file );
				}
			}
		}

		return $found_files;
	}

	/**
	 * Templates overriding status for WooCommerce Status report page.
	 */
	function system_report_templates() {

	    TInvWL_View::view( 'templates-status', array( 'found_files' => $this->templates_status_check() ) );
	}

	/**
	 * Outdated templates notice.
	 */
	function admin_notice_outdated_templates() {
		if ( 'outdated' === $this->templates_status_check( true ) ) {

			$theme = wp_get_theme();

			$html = sprintf( __( '<strong>Your theme (%1$s) contains outdated copies of some TI WooCommerce Wishlist template files.</strong><br> These files may need updating to ensure they are compatible with the current version of TI WooCommerce Wishlist.<br> You can see which files are affected from the <a href="%2$s">system status page</a>.<br> If in doubt, check with the author of the theme.', 'ti-woocommerce-wishlist-premium' ), esc_html( $theme['Name'] ), esc_url( admin_url( 'admin.php?page=wc-status' ) ) );

			WC_Admin_Notices::add_custom_notice( 'outdated_templates', $html );
		} else {
			WC_Admin_Notices::remove_notice( 'outdated_templates' );
		}
	}

	/**
	 * Load javascript for validation templates
	 */
	function enqueue_scripts_validate_template() {
		$theme	 = wp_get_theme();
		$theme	 = $theme->get_template();
		if ( tinv_get_option( 'template_checker', 'theme' ) !== $theme ) {
			tinv_update_option( 'template_checker', '', array() );
			tinv_update_option( 'template_checker', 'theme', $theme );
			tinv_update_option( 'template_checker', 'checked', false );
			tinv_update_option( 'template_checker', 'time', 0 );
		}
		if ( tinv_get_option( 'template_checker', 'checked' ) && absint( tinv_get_option( 'template_checker', 'time' ) ) + HOUR_IN_SECONDS > time() ) {
			return;
		}
		$types	 = array_keys( wc_get_product_types() );
		foreach ( $types as $type => $type_name ) {
			if ( ! tinv_get_option( 'template_checker', 'missing_hook_' . $type ) ) {
				$data = filter_input_array( INPUT_GET, array(
					'wc-hide-notice'	 => FILTER_DEFAULT,
					'_wc_notice_nonce'	 => FILTER_DEFAULT,
				) );
				if ( 'missing_hook_' . $type === $data['wc-hide-notice'] && wp_verify_nonce( $data['_wc_notice_nonce'], 'woocommerce_hide_notices_nonce' ) ) {
					tinv_update_option( 'template_checker', 'missing_hook_' . $type, true );
				}
			}
		}
		if ( ! tinv_get_option( 'template_checker', 'hide_product_listing' ) ) {
			$data = filter_input_array( INPUT_GET, array(
				'wc-hide-notice'	 => FILTER_DEFAULT,
				'_wc_notice_nonce'	 => FILTER_DEFAULT,
			) );
			if ( 'missing_hook_listing' === $data['wc-hide-notice'] && wp_verify_nonce( $data['_wc_notice_nonce'], 'woocommerce_hide_notices_nonce' ) ) {
				tinv_update_option( 'template_checker', 'hide_product_listing', true );
			}
		}

		wp_enqueue_script( $this->_n . '-checher', TINVWL_URL . 'asset/js/admin.checher.min.js', array( 'jquery' ), $this->_v, 'all' );
	}

	/**
	 * Validation templates hook from request remote page
	 */
	function validation_template() {
		global $post, $product;

		if ( tinv_get_option( 'template_checker', 'checked' ) ) {
			return;
		}
		if ( absint( tinv_get_option( 'template_checker', 'time' ) ) + HOUR_IN_SECONDS > time() ) {
			return;
		}
		tinv_update_option( 'template_checker', 'time', time() );
		$tags	 = array(
			'woocommerce_single_product_summary'	 => 'tinvwl_single_product_summary',
			'woocommerce_before_add_to_cart_button'	 => 'tinvwl_before_add_to_cart_button',
			'woocommerce_after_add_to_cart_button'	 => 'tinvwl_after_add_to_cart_button',
		);
		$tch		 = TInvWL_CheckerHook::instance();
		$tch->add_action( $tags );
		$tch->add_action( array_keys( $tags ) );

		$types	 = wc_get_product_types();
		$result	 = array();
		$check = true;
		foreach ( $types as $type => $type_name ) {
			if ( tinv_get_option( 'template_checker', 'missing_hook_' . $type ) ) {
				continue;
			}
			$products = array();
			if ( function_exists( 'wc_get_products' ) ) {
				$products	 = wc_get_products( array(
					'status' => 'publish',
					'type'	 => $type,
					'limit'	 => 1,
				) );
			} else {
				$products = array_map( 'wc_get_product', get_posts( array(
					'post_type'		 => 'product',
					'post_status'	 => 'publish',
					'numberposts'	 => 1,
					'tax_query'		 => array(
						array(
							'taxonomy'	 => 'product_type',
							'field'		 => 'slug',
							'terms'		 => $type,
						),
					),
				) ) );
			}
			if ( ! empty( $products ) ) {
				$product = array_shift( $products );
				$post	 = get_post( $product->get_id() ); // @codingStandardsIgnoreLine  WordPress.Variables.GlobalVariables.OverrideProhibited
				$result	 = $tch->run( array(
					'template'		 => array( 'content-single-product.php', 'single-product/add-to-cart/' . $type . '.php' ),
					'template_args'	 => array(
						'available_variations'	 => array( 1, 2, 3, 4, 5 ),
						'attributes'			 => array(),
					),
					'url'			 => $product->get_permalink(),
				) );
				if ( ! empty( $result ) ) {
					$_result = $result	 = array_keys( $result );
					foreach ( $result as $key => $tag ) {
						if ( array_key_exists( $tag, $tags ) ) {
							$_tag = $tags[ $tag ];
							if ( ! array_key_exists( $tag, $tags ) ) {
								unset( $result[ $key ] );
							}
						} else {
							unset( $result[ $key ] );
						}
					}
					if ( ! empty( $result ) ) {
						WC_Admin_Notices::add_custom_notice( 'missing_hook_' . $type, sprintf( _n( 'The "Add to Wishlist" button may work improperly in a product type "%1$s" because the hook "%2$s" is missing.<br />Please, ask your theme developers to check the theme templates or <a href="https://templateinvaders.com/help/" target="_blank">contact us</a> for assistance.', 'The "Add to Wishlist" button may work improperly in a product type "%1$s" because the hooks "%2$s" are missing.<br />Please, ask your theme developers to check the theme templates or <a href="https://templateinvaders.com/help/" target="_blank">contact us</a> for assistance.', count( $result ), 'ti-woocommerce-wishlist-premium' ), $type_name, '<strong>' . join( '</strong>, <strong>', $result ) . '</strong>' ) );
						$check = false;
					} else {
						WC_Admin_Notices::remove_notice( 'missing_hook_' . $type );
					}
				} else {
					WC_Admin_Notices::remove_notice( 'missing_hook_' . $type );
				}
			}
		} // End foreach().

		if ( ! tinv_get_option( 'template_checker', 'hide_product_listing' ) ) {
			$products = array();
			if ( function_exists( 'wc_get_products' ) ) {
				$products = wc_get_products( array(
					'status' => 'publish',
					'limit'	 => 1,
				) );
			} else {
				$products = get_posts( array(
					'post_type'		 => 'product',
					'numberposts'	 => 1,
				) );
			}
			if ( ! empty( $products ) ) {
				$tags		 = array(
					'woocommerce_after_shop_loop_item' => 'tinvwl_after_shop_loop_item',
				);
				$tch		 = TInvWL_CheckerHook::instance();
				$tch->add_action( $tags );
				$tch->add_action( array_keys( $tags ) );
				$product	 = array_shift( $products );
				$_result	 = $tch->run( array(
					'template'	 => 'content-product.php',
					'url'		 => get_permalink( wc_get_page_id( 'shop' ) ),
				) );
				$__result	 = $tch->run( array( 'url' => add_query_arg( 'post_type', 'product', home_url( '/' ) ) ) );
				$_result = $result = array_keys( tinv_array_merge( $_result, $__result ) );
				if ( ! empty( $result ) ) {
					foreach ( $result as $key => $tag ) {
						if ( array_key_exists( $tag, $tags ) ) {
							$_tag = $tags[ $tag ];
							if ( ! array_key_exists( $tag, $tags ) ) {
								unset( $result[ $key ] );
							}
						} else {
							unset( $result[ $key ] );
						}
					}
					if ( ! empty( $result ) ) {
						WC_Admin_Notices::add_custom_notice( 'missing_hook_listing', sprintf( __( 'The "Add to Wishlist" button may work improperly in a catalog because the hook "%1$s" is missing.<br />Please, ask your theme developers to check the theme templates or <a href="https://templateinvaders.com/help/" target="_blank">contact us</a> for assistance.', 'ti-woocommerce-wishlist-premium' ), '<strong>woocommerce_after_shop_loop_item</strong>' ) );
						$check = false;
					} else {
						WC_Admin_Notices::remove_notice( 'missing_hook_listing' );
					}
				} else {
					WC_Admin_Notices::remove_notice( 'missing_hook_listing' );
				}
			}
		} // End if().
		tinv_update_option( 'template_checker', 'checked', $check );
		wp_die();
	}

	/**
	 * Clear notice validation template when theme switched
	 */
	function clear_notice_validation_template() {
		WC_Admin_Notices::remove_notice( 'missing_hook_listing' );
		$types	 = wc_get_product_types();
		foreach ( $types as $type => $type_name ) {
			WC_Admin_Notices::remove_notice( 'missing_hook_' . $type );
		}
		tinv_update_option( 'template_checker', '', array() );
	}

	/**
	 * Disable screen option on plugin pages
	 *
	 * @param boolean    $show_screen Show screen.
	 * @param \WP_Screen $_this Screen option page.
	 * @return boolean
	 */
	function screen_options_hide_screen( $show_screen, $_this ) {
		if ( $this->_n === $_this->parent_base || $this->_n === $_this->parent_file ) {
			return false;
		}
		return $show_screen;
	}

	/**
	 * Check if there is a hook in the cron
	 */
	function scheduled_remove_wishlist() {
		$timestamp = wp_next_scheduled( 'tinvwl_remove_without_author_wishlist' );
		if ( ! $timestamp ) {
			$time = strtotime( '00:00 today +1 HOURS' );
			wp_schedule_event( $time, 'daily', 'tinvwl_remove_without_author_wishlist' );
		}
	}

	/**
	 * Removing old wishlist without a user older than 34 days
	 */
	public function remove_old_wishlists() {
		$wl			 = new TInvWL_Wishlist();
		$wishlists	 = $wl->get( array(
			'author' => 0,
			'type'	 => 'default',
			'sql'	 => 'SELECT * FROM {table} {where} AND `date` < DATE_SUB( CURDATE(), INTERVAL 34 DAY)',
		) );
		foreach ( $wishlists as $wishlist ) {
			$wl->remove( $wishlist['ID'] );
		}
	}
}
