TEMPLATEINVADERS LLC END-USER LICENSE AGREEMENT
This License is entered by TemplateInvaders to govern the usage 
or redistribution of TemplateInvaders software. This is a legal 
agreement between you (either an individual or a single entity) 
and TemplateInvaders for TemplateInvaders software product(s) 
which may include extensions, templates and services.

By purchasing, installing, or otherwise using TemplateInvaders 
products, you acknowledge that you have read this License and 
agree to be bound by the terms of this Agreement. If you do not 
agree to the terms of this License, do not install or use 
TemplateInvaders products.

The Agreement becomes effective at the moment when you acquire 
software from our site or receive it through email or on data 
medium or by any other means. TemplateInvaders reserves the 
right to make reasonable changes to the terms of this license 
agreement and impose its clauses at any given time.

We are the copyright holder of the Software. The Software or 
a portion of it is a copyrightable matter and is liable to 
protection by the law. Any activity that infringes terms of 
this Agreement violates copyright law and will be prosecuted 
according to the current law. We reserve the right to revoke 
the license of any user who is holding an invalid license.

You are bound to preserve the copyright information intact in 
the source files.

We reserve the right to publish a selected list of users of 
our Software.

TemplateInvaders reserves the right to change this license 
agreement at any time and impose its clauses at any given time.


1. GRANT OF LICENSE
The Regular License grants you, the purchaser, an ongoing, 
non-exclusive, worldwide license to make use of the digital 
work (Item) you have selected.

Customer will obtain a License Certificate which will remain 
valid until the Customer stops using the Product or until 
TemplateInvaders terminates this License because of 
Customer's failure to comply with any of its Terms and 
Conditions.

Your use of the Work is limited to the number of API Keys
(subscrition) activations.

You are licensed to use the Item for yourself or for your 
clients and on a single or on the corresponding to your 
subscription(API key activations) number of domains.(if it is 
used on the website).

You are allowed to create one End Product for a client, and you 
can transfer that single End Product to your client for any fee. 
This license is then transferred to your client. 

You are allowed to modify or manipulate the Item. You can 
combine the Item with other works and make a derivative work 
from it. The resulting works are subject to the terms of this 
license.


2. DESCRIPTION OF LIMITATIONS
You can't Sell the End Product, except to one client.

You can't re-distribute the Item (Any portion of the Software or 
Documentation) as stock, in a tool or template, or with source 
files. You can't do this with an Item either on its own or bundled 
with other items or any part of the code, and even if you modify 
the Item. You can't re-distribute or make available the Item as-is 
or with superficial modifications. These things are not allowed 
even if the re-distribution is for Free. 

You can't use the Item in any application allowing an end user to 
customise a digital or physical product to their specific needs, 
such as an "on demand", "made to order" or "build it yourself" 
application. You can use the Item in this way only if you purchase 
a separate license for each final product incorporating the Item 
that is created using the application.

Although you can modify the Item and therefore delete unwanted 
components before creating your single End Product, you can't 
extract and use a single component of an Item on a stand-alone 
basis.

You must not permit an end user of the End Product to extract the 
Item and use it separately from the End Product.

You may not place the Software on a server so that it is accessible 
via a public network such as the Internet for distribution purposes.


3. TERMINATION
Without prejudice to any other rights, TemplateInvaders may terminate 
this License at any time if you fail to comply with the terms and 
conditions of this License. In such event, it constitutes a breach 
of the agreement, and your license to use the program is revoked and 
you must destroy all copies of TemplateInvaders products in your 
possession.

After being notified of termination of your license, if you continue 
to use TemplateInvaders software, you hereby agree to accept an 
injunction to prevent you from its further use and to pay all costs 
(including but not limited to reasonable attorney fees) to enforce 
our revocation of your license and any damages suffered by us because 
of your misuse of the Software.

We are not bound to return you the amount spent for purchase of the 
Software for the termination of this License.


4. LIMITATION OF LIABILITY
In no event shall TemplateInvaders be liable for any damages 
(including, without limitation, lost profits, business interruption, 
or lost information) rising out of "Authorized Users" use of or 
inability to use the TemplateInvaders products, even if 
TemplateInvaders has been advised of the possibility of such damages.

In no event will TemplateInvaders be liable for prosecution arising 
from use of the Software against law or for any illegal use.


5. SUPPORT & UPDATES
This software is designed to work with a specific Software version or 
edition and its use on a version or edition other than specified is 
prohibited. TemplateInvaders does not provide support in case of 
incorrect version or edition use.

Technical support and product updates are time-limited according to 
the purchased support periods for the product.

Detailed support policy can be found on 
https://templateinvaders.com/support-policy/

6. DEFINITIONS
End Product - is a customized implementation of the Item.

Sell or Sold - Sell, license, sub-license or distribute for any type 
of fee or charge.