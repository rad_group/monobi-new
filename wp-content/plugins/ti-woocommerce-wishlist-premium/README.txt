=== TI WooCommerce Wishlist Premium ===
Contributors: templateinvaders
Donate link: https://templateinvaders.com/
Tags: wishlist, woocommerce, products, e-commerce, shop, ecommerce wishlist, woocommerce wishlist, woocommerce , shop wishlist, wishlist  for Woocommerce
Requires at least: 4.5
Tested up to: 4.9
Stable tag: 1.7.1
License: Template Invaders License
License URI: https://templateinvaders.com/license-agreement/
Documentation: https://templateinvaders.com/documentation/ti-woocommerce-wishlist/

More than just a Wishlist for WooCommerce, a powerful marketing & analytics tool.

== Description ==

You have full control over the look and functionality of your wishlist, from buttons and columns to processing and login options.
But what is really important is that you can grow a community using our exclusive follow feature and social share options. 
People can create multiple wishlists for different events like birthdays, Christmas and share it with their friends, so they can not worry about gifts anymore. 
On the other hand, customers can use it as collections with sets of different trendy clothes, gain followers and become a superstar! 
When you at this time can learn what your customers "wish". 
With the help of integrated analytics, you can see sales comparing to product popularity and other important information, that can help you build your successful sales strategy.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the `TI WooCommerce Wishlist Premium` through the 'Plugins' screen in WordPress
3. For the plugin to work as it should, WooCommerce plugin has to be installed and enabled.

== Changelog ==

= 1.7.1 - Released: 2017/03/13 =
* Fixed PHP fatal error on a public wishlist view
* Fixed wishlists limit in a dropdown of add to wishlist dialog when multi-wishlists support enabled 


= 1.7.0 - Released: 2017/03/01 =
* Fixed Fatal error: if $product is not WooCommerce product
* Fixed Fatal error: if get_user_by is an object
* Fixed jQuery notice related to 3.0+ 
* Fixed an issue with deprecated function create_function(); on PHP 7.2+
* Fixed an issue with duplicated navigation buttons on a Wishlist page
* Fixed an issue with duplicated products in a Wishlist
* Fixed an issue with saving products into Wishlist on a cart page
* Fixed an issue with empty Wishlist name
* Fixed an issue with custom meta disappearing after moving products between wishlists
* Fixed an issue when "Add to Cart" action was available after disabling all buttons on a Wishlist page
* Fixed an issue when "Add to Wishlist" button didn't appear on a product details page for products without price 
* Fixed an issue when empty wishlist is created once a guest visits the shop page
* Fixed an issue with displaying SKU attribute after adding products to Wishlist from a catalog
* Fixed text domains for some strings
* Added new option "Require Login" that disallows guests to add products to a Wishlist until they sign-in
* Added new option "Show button text" that allows displaying the only add to wishlist icon
* Added "nofollow" attribute for button links
* Added wishlists sorting by date of creation in admin panel
* Added filter <i>tinvwl_addtowishlist_dialog_box</i> that will be helpful to override data in pop-up windows
* Added filters <i>tinvwl_addtowishlist_login_page</i> and <i>tinvwl_addtowishlist_return_ajax</i> that will be helpful to override "Require Login" popup.
* Added support for WooCommerce TM Extra Product Options plugin
* Added support for WP Multilang plugin
* Added French (Canada) translation 
* Added Dutch (Netherlands) translation 
* Improved compatibility with Personalized Product Option Manager plugin
* Improved Wishlist Products Counter functionality
* Improved variable products processing when adding to Wishlist

= 1.6.1 - Released: 2017/12/05 =

* Fixed an issue when guests could not add products into a Wishlist
* Improved Wishlist Products Counter functionality

= 1.6.0 - Released: 2017/12/01 =

* Fixed an issue with the wrong metadata after sharing Wishlist on Facebook
* Fixed Wishlist Products Counter issue when the wrong number of products was displaying if cache is enabled
* Fixed an issue with W3 Total Cache plugin
* Fixed an issue with extra scheduled cleanup events
* Fixed JavaScript frontend compatibility issue
* Fixed SQL query to avoid an issue when Wishlist title has an apostrophe
* Fixed an issue with duplicated call to WC AJAX fragments
* Fixed an issue with displaying product thumbnails in emails

* Added “Reset to Defaults” option in the admin panel
* Added an option to show the “Add to Wishlist” button above product thumbnail on a catalog page
* Added an option to show/hide mini wishlist for products counter
* Added support for Comet Cache plugin
* Added support for WP Fastest Cache plugin
* Added new "Soft Skin" for wishlists
* Added filter ‘tinvwl_allow_addtowishlist_single_product’ that helps to show/hide the “Add to Wishlist” button for specific products on a single products page
* Added hook 'tinvwl_send_ask_for_estimate_args' that helps to override the estimate email form
* Lots of CSS fixes and improvements

* Improved Wishlists storage functionality: 
 * empty wishlists that do not have a user will be automatically removed after 7 days
 * wishlists that contain at least 1 product but do not have a user will be automatically removed after 31 days
* Improved performance for custom styles
* Improved Setup Wizard
* Improved W3 Validation
* Improved Translation
 * Corrected some texts
 * Corrected some typos

= 1.5.7 - Released: 2017/10/21 =

* Fixed an issue with fonts not applying in Wishlist if "Use Theme Style" option is enabled
* Fixed an issue with transferring products between guest and user wishlists used on the same device/machine in the same browser.
* Fixed an issue with empty pop up window after clicking Share Wishlist by Email button
* Internal improvements:
 * Remove product from Wishlist on second click functionality does not depend whether products quantity is enabled or not any more. 
 * Variable product (without predefined variations applied by default) added from products listing page will be always substituted with the product added from details page (with selected variations).
 * Reorganized "General Settings" section in admin panel to make it more user friendly.
 * Added options descriptions in the admin panel.
* Improved "Wishlist Products Counter" widget
 * If product added to multiple wishlists at the same time, all wishlists will be displayed below the product thumbnail in a dropdown.
 * "View Wishlist" and "Add All to cart" buttons are no longer visible if wishlist is empty.
* Improved WooCommerce Composite Products plugin support:
 * Fixed individual price calculation with components
* Improved Polylang plugin support

= 1.5.3 - Released: 2017/09/20 =

* Fixed an issue with transferring products between guest and customer wishlists after signing in or logout.
* Fixed an issue when it's not possible to remove products from wishlist as a guest
* Fixed an issue with adding a product variations to wishlist
* Improved WooCommerce Product Bundles plugin support:
 * Fixed an issue with displaying product custom meta attributes
* Improved WPML plugin compatibility:
 * Fixed an issue with "Remove/Add" button text when switching languages

= 1.5.2 - Released: 2017/09/11 =

* Improved WooCommerce product bundles support:
 * Fixed and issue when product variations was not applied in bundled products
* Fixed an issue with products visibility on a Wishlist page
* Fixed an issue with inactive "Add to Cart" button for product variations when Wishlist functionality for unauthenticated users is disabled
* Added arguments for filter that make possible overriding popup notices

= 1.5.1 - Released: 2017/09/03 =

* Fixed an issue when product does not remove automatically from Wishlist after adding to cart
* Fixed an issue with duplicated products in Wishlist when WooCommerce Multilingual plugin activated
* Fixed a JavaScript error that prevents adding products to wishlist

= 1.5.0 - Released: 2017/08/30 =

* Added "Wishlist Products counter" shortcode & widget
* Added "Save for Later" functionality for a Cart page
* Added the ability to remove product from a Wishlist on the second click
* Added an option to show/hide a popup with successful or error notices after adding or removing products from a Wishlist
* Added support for plugins/WooCommerce add-ons that use custom meta:
 * WooCommerce Gift Cards
 * WooCommerce Bookings
 * WooCommerce Subscriptions
 * WooCommerce Composite Products
 * WooCommerce Product Bundles
 * WooCommerce Mix and Match
 * WooCommerce Quantity Increment
 * WooCommerce Personalized Product Option
 * WooCommerce Gravity Forms Product Add*Ons
 * YITH WooCommerce Product Bundles
* Added missing descriptions to some of the settings in the admin panel
* Added the ability to load custom translation files
* Added minified version of FontAwesome library
* Improved WPML compatibility
* Fixed an issue with redirect to 404 error page after new wishlist is created
* Fixed fatal error in Dashboard menu
* Fixed a few PHP notices
* Fixed an issue when variation has additional attribute(s) with any value
* Lots of CSS fixes
* Overall frontend performance optimization
* Code cleanup

= 1.3.3 - Released: 2017/05/07 =

* Improved WPML compatibility (fixed an issue with URL formats)
* Fixed issues with deprecated hooks related to WooCommerce 3.0.5
* Added Polylang plugin support
* Added new option that allows product automatic removal when it's added to cart by anyone

= 1.3.2 - Released: 2017/04/27 =

* Minor CSS fixes
* Improved compatibility for WooCommerce 2 & 3
* Improved theme compatibility tests performance

= 1.3.1 - Released: 2017/04/26 =

* Fixed issues with promotional and estimate emails
* Improved  theme compatibility tests for shared hosts
* Improved compatibility for WooCommerce 2 & 3

= 1.3.0 - Released: 2017/04/24 =

* Fixed WPML string translations issue
* Added theme compatibility notices
* Wishlist custom item meta hidden from order
* Added compatibility with WooCommerce – Gift Cards

= 1.2.0 - Released: 2017/04/12 =

* WooCommerce 3.0+ support
* Added template overrides check for WooCommerce system report

= 1.1.3 - Released: 2017/04/04 =

* Fixed multiple issues with WPML support

= 1.1.2.11 - Released: 2017/03/16 =

* Fixed an issue when the Wishlist was not refreshed after the product is removed or added to cart by the unauthenticated user.

= 1.1.2.10 - Released: 2017/03/03 =

* Fixed an issue with external products link

= 1.1.2.9 - Released: 2017/03/02 =

* The Setup Wizard enhancements
* Added new hooks for wishlist create|update|delete  and wishlist product add|update|remove events

= 1.1.2.8 - Released: 2017/02/26 =

* Fixed an issue with W3 Total Cache compatibility
* Added public functions

= 1.1.2.7 - Released: 2017/02/03 =

* Fixed an issue with "Add to Wishlist" function in a quick view popup (Compatibility with plugins that provide QuickView functionality)
* Added JavaScript alert for the "Add to Wishlist" button on a single product page when no variations are selected

= 1.1.2.6 - Released: 2017/01/27 =

* Fixed an issue when maximum 10 products can be added to cart from a Wishlist page using the "Add all to cart" button

= 1.1.2.5 - Released: 2017/01/27 =

* Fixed class loading critical error after plugin activation
* Added an option to show "Add to Wishlist" button on a catalog page for variable products (if all default variations are set)

= 1.1.2.4 - Released: 2017/01/10 =

* Fixed issue with empty wishlist page
* Fixed issue with wrong product quantity on add to cart event from wishlist


= 1.1.2.3 - Released: 2016/12/09 =

* Fixed issues with pagination 
* Added support for WordPress 4.7
* Removed Genericicons fonts

= 1.1.2.2 - Released: 2016/11/08 =

* Fixed issue with upgrade from free to premium version 
* Fixed issue with "Remove Wishlist" button


= 1.1.2.1 - Released: 2016/10/13 =

* Added reload page when create new wishlist
* Fixed database error



= 1.1.2 - Released: 2016/10/12 =

* Added support for W3 Total Cache plugin
* Added support for WooCommerce - Gravity Forms Product Add-Ons
* Added minimized versions of JS
	

= 1.1.1 - Released: 2016/10/11 =
* Added support of WpP Super Cache plugin
* Fixed Text in Follow emails
* Fixed possible issue with updater
* Fixed Period for sending notifications followers option
* Fixed field validator in email settings
* Fixed promotional emails preview
* Fixed disable of Follow functionality when disabling email in WooCommerce > Settings > Emails
* Fixed	default colors for predefined promotional emails 
* Follow wishlist button now visible for guests and suggests to login


= 1.1.0 - Released: 2016/09/22 =


* Added WP Rocket support
* Added promo email default skin values.
* Added filters for plugin options.
* Added social share and add to cart buttons to someone else wishlist page.
* Added theme font option.
* Added interception and output of ajax errors.
* Email heading and subject are now correctly applied to email.
* Moved modal windows to the footer.
* Fixed issue when quantity in wishlist exceeds the number of products in stock.
* Fixed link in login modal after error message.
* Fixed second predefined wishlist icon in black color not appear.
* Fixed subscribe issue.
* Fixed send estimate issue.
* Fixed plugin updater.
* Wishlist table footer is now hidden if all elements are disabled.
* A lot of minor fixes

= 1.0.0 - Released: 2016/09/09 =

* Initial release
