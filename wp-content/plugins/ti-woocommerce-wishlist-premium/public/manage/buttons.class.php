<?php
/**
 * Action buttons for Manage Wishlist
 *
 * @since             1.0.0
 * @package           TInvWishlist\Public
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Action buttons for Manage Wishlist
 */
class TInvWL_Public_Manage_Buttons {

	/**
	 * Plugin name
	 *
	 * @var string
	 */
	static $_n;
	/**
	 * Basic event
	 *
	 * @var string
	 */
	static $event;

	/**
	 * First run function
	 *
	 * @param string $plugin_name Plugin name.
	 */
	public static function init( $plugin_name = TINVWL_PREFIX ) {
		self::$_n    = $plugin_name;
		self::$event = 'tinvwl_after_wishlistmanage_table';
		self::htmloutput();
	}

	/**
	 * Defined buttons
	 *
	 * @return array
	 */
	private static function prepare() {
		$buttons   = array();
		$buttons[] = array(
			'name'     => 'manage_apply',
			'title'    => sprintf( __( 'Apply %s', 'ti-woocommerce-wishlist-premium' ), "<span class='tinvwl-mobile'>" . __( 'Action', 'ti-woocommerce-wishlist-premium' ) . '</span>' ),
			'method'   => array( __CLASS__, 'apply_action' ),
			'before'   => array( __CLASS__, 'apply_action_before' ),
			'priority' => 10,
		);
		$buttons[] = array(
			'name'     => 'manage_save',
			'title'    => __( 'Save Wishlists', 'ti-woocommerce-wishlist-premium' ),
			'method'   => array( __CLASS__, 'save' ),
			'priority' => 30,
		);
		add_filter( self::$_n . '_prepare_attr__button_manage_save', array( __CLASS__, 'remove_class' ) );
		add_filter( self::$_n . '_prepare_attr__button_manage_apply', array( __CLASS__, 'add_break_class' ) );

		$buttons = apply_filters( 'tinvwl_buttons_create', $buttons );

		return $buttons;
	}

	/**
	 * Remove class
	 *
	 * @param array $attr Atribites for button.
	 *
	 * @return array
	 */
	public static function remove_class( $attr ) {
		if ( array_key_exists( 'class', (array) $attr ) ) {
			unset( $attr['class'] );
		}

		return $attr;
	}

	/**
	 * Output buttons
	 */
	public static function htmloutput() {
		$buttons = self::prepare();
		foreach ( $buttons as $button ) {
			self::addbutton( $button );
		}
	}

	/**
	 * Add break class
	 *
	 * @param array $attr Attributes.
	 *
	 * @return array
	 */
	public static function add_break_class( $attr ) {
		if ( array_key_exists( 'class', $attr ) ) {
			$attr['class'] .= ' tinvwl-break-input tinvwl-break-checkbox';
		} else {
			$attr['class'] = 'tinvwl-break-input tinvwl-break-checkbox';
		}

		return $attr;
	}

	/**
	 * Create button and action
	 *
	 * @param array $button Structure for button.
	 *
	 * @return boolean
	 */
	public static function addbutton( $button ) {
		if ( ! array_key_exists( 'name', $button ) ) {
			return false;
		}
		if ( ! array_key_exists( 'priority', $button ) ) {
			$button['priority'] = 10;
		}
		if ( ! array_key_exists( 'method', $button ) ) {
			$button['method'] = array( __CLASS__, 'null_action' );
		}
		if ( ! array_key_exists( 'event', $button ) ) {
			$button['event'] = self::$event;
		}
		if ( ! array_key_exists( 'condition', $button ) ) {
			$button['condition'] = 'true';
		}
		if ( array_key_exists( 'submit', $button ) ) {
			$button['submit'] = $button['submit'] ? 'submit' : 'button';
		} else {
			$button['submit'] = 'submit';
		}

		if ( array_key_exists( 'before', $button ) ) {
			add_filter( self::$_n . '_before__button_' . $button['name'], $button['before'] );
		}
		if ( array_key_exists( 'after', $button ) ) {
			add_filter( self::$_n . '_after__button_' . $button['name'], $button['after'] );
		}

		add_action( $button['event'], function () use ( $button ) {
			if ( $button['condition'] ) {
				self::button( $button['name'], $button['title'], $button['submit'] );
			}
		}, $button['priority'] );

		add_action( 'tinvwl_action_' . $button['name'], $button['method'], 10, 4 );
	}

	/**
	 * Create html button
	 *
	 * @param string $value Vaule for tinvwl-action.
	 * @param string $title HTML title for button.
	 * @param string $submit Type button.
	 * @param boolean $echo Retun or echo.
	 *
	 * @return string
	 */
	public static function button( $value, $title, $submit, $echo = true ) {
		$html = apply_filters( self::$_n . '_before__button_' . $value, '' );
		$attr = array(
			'type'  => $submit,
			'class' => 'button',
			'name'  => 'tinvwl-action',
			'value' => $value,
		);
		$attr = apply_filters( self::$_n . '_prepare_attr__button_' . $value, $attr );
		foreach ( $attr as $key => &$value ) {
			$value = sprintf( '%s="%s"', $key, esc_attr( $value ) );
		}
		$attr = implode( ' ', $attr );

		$html .= apply_filters( self::$_n . '__button_' . $value, sprintf( '<button %s>%s</button>', $attr, $title ) );
		$html .= apply_filters( self::$_n . '_after__button_' . $value, '' );

		if ( $echo ) {
			echo $html; // WPCS: xss ok.
		} else {
			return $html;
		}
	}

	/**
	 * Default action for button
	 *
	 * @return boolean
	 */
	public static function null_action() {
		return false;
	}

	/**
	 * Add class 'alt' to button
	 *
	 * @param array $attr Attributes for button.
	 *
	 * @return array
	 */
	public static function style_action( $attr ) {
		if ( array_key_exists( 'style', $attr ) ) {
			$attr['style'] .= ' alt';
		} else {
			$attr['style'] = 'alt';
		}

		return $attr;
	}

	/**
	 * Create select for custom action
	 *
	 * @return string
	 */
	public static function apply_action_before() {
		return TInvWL_Form::_select( 'manage_actions', '', array( 'class' => 'tinvwl-break-input-field' ), array(
			''        => __( 'Actions', 'ti-woocommerce-wishlist-premium' ),
			'remove'  => __( 'Delete', 'ti-woocommerce-wishlist-premium' ),
			'public'  => __( 'Set "Public"', 'ti-woocommerce-wishlist-premium' ),
			'share'   => __( 'Set "Share"', 'ti-woocommerce-wishlist-premium' ),
			'private' => __( 'Set "Private"', 'ti-woocommerce-wishlist-premium' ),
		) );
	}

	/**
	 * Apply action for manage_apply
	 *
	 * @param array $wishlists Array user wishlists.
	 * @param array $wishlist_pr Array selected wishlists.
	 */
	public static function apply_action( $wishlists, $wishlist_pr ) {
		$action = filter_input( INPUT_POST, 'manage_actions', FILTER_SANITIZE_STRING );
		switch ( $action ) {
			case 'public':
			case 'share':
			case 'private':
				$action = array( 'status', $action );
				break;
			default:
				$action = array( $action );
				break;
		}
		$wl     = new TInvWL_Wishlist();
		$result = array();
		foreach ( $wishlists as $wishlist ) {
			$wishlist_id = $wishlist['ID'];
			if ( false === array_search( $wishlist_id, (array) $wishlist_pr, true ) ) {
				continue;
			}
			switch ( $action[0] ) {
				case 'remove':
					if ( 'default' === $wishlist['type'] ) {
						continue;
					}
					if ( $wl->remove( $wishlist_id ) ) {
						$result[ $wishlist_id ] = $wishlist['title'];
					}
					break;
				case 'status':
					if ( ! in_array( $action[1], array(
						'public',
						'share',
						'private'
					) ) ) { // @codingStandardsIgnoreLine WordPress.PHP.StrictInArray.MissingTrueStrict
						$action[1] = 'public';
					}
					if ( $wishlist['status'] !== $action[1] ) {
						$wishlist['status'] = $action[1];
						if ( $wl->update( $wishlist_id, $wishlist ) ) {
							$result[ $wishlist_id ] = $wishlist['title'];
						}
					}
					break;
			}
		}
		if ( ! empty( $result ) ) {
			switch ( $action[0] ) {
				case 'remove':
					wc_add_notice( sprintf( __( 'Successfully deleted wishlists: %s.', 'ti-woocommerce-wishlist-premium' ), implode( ', ', $result ) ) );
					break;
				case 'status':
					$privacy = array(
						'public'  => __( 'Public', 'ti-woocommerce-wishlist-premium' ),
						'share'   => __( 'Share', 'ti-woocommerce-wishlist-premium' ),
						'private' => __( 'Private', 'ti-woocommerce-wishlist-premium' ),
					);
					wc_add_notice( sprintf( __( 'Successfully changed the status to "%1$s" wishlists: %2$s.', 'ti-woocommerce-wishlist-premium' ), ( array_key_exists( $action[1], $privacy ) ? $privacy[ $action[1] ] : $action[1] ), implode( ', ', $result ) ) );
					break;
			}

			return true;
		}

		return false;
	}

	/**
	 * Apply action for manage_save
	 *
	 * @param array $wishlists Array user wishlists.
	 * @param array $wishlist_pr Not used.
	 * @param array $wishlist_privacy Array privacy wishlists.
	 * @param array $wishlist_name Array name wishlists.
	 */
	public static function save( $wishlists, $wishlist_pr, $wishlist_privacy, $wishlist_name ) {
		$wl     = new TInvWL_Wishlist();
		$result = array();
		foreach ( $wishlists as $wishlist ) {
			$need_update       = false;
			$wishlist_id       = $wishlist['ID'];
			$privacy           = 'public';
			$original_wishlist = $wishlist;
			if ( array_key_exists( $wishlist_id, (array) $wishlist_privacy ) ) {
				$_privacy = $wishlist_privacy[ $wishlist_id ];
				if ( in_array( $_privacy, array(
					'public',
					'share',
					'private'
				) ) ) { // @codingStandardsIgnoreLine WordPress.PHP.StrictInArray.MissingTrueStrict
					$privacy = $_privacy;
				}
				if ( $wishlist['status'] !== $privacy ) {
					$need_update = true;
				}
				$wishlist['status'] = $privacy;
			}
			if ( array_key_exists( $wishlist_id, (array) $wishlist_name ) ) {
				$name = trim( $wishlist_name[ $wishlist_id ] );
				if ( 'default' === $wishlist['type'] ) {
					if ( $wishlist['title'] !== $name ) {
						$need_update = true;
					}
					if ( apply_filters( 'tinvwl-general-default_title', tinv_get_option( 'general', 'default_title' ) ) == $name ) { // WPCS: loose comparison ok.
						$name = '';
					}
					$wishlist['title'] = $name;
				} elseif ( ! empty( $name ) ) {
					if ( $wishlist['title'] !== $name ) {
						$need_update = true;
					}
					$wishlist['title'] = $name;
				}
			}
			$wishlist = apply_filters( 'tinvwl_manage_wishlist_update', $wishlist, $wishlist_id );
			if ( apply_filters( 'tinvwl_manage_wishlist_need_update', $need_update, $wishlist_id, $wishlist, $original_wishlist ) ) {
				if ( $wl->update( $wishlist_id, $wishlist ) ) {
					$result[ $wishlist_id ] = ( 'default' === $wishlist['type'] && empty( $wishlist['title'] )? apply_filters( 'tinvwl-general-default_title', tinv_get_option( 'general', 'default_title' ) ) : $wishlist['title'] );
				}
			}
		} // End foreach().
		if ( ! empty( $result ) ) {
			wc_add_notice( sprintf( __( 'Successfully updated wishlists: %s.', 'ti-woocommerce-wishlist-premium' ), implode( ', ', $result ) ) );

			return true;
		}

		return false;
	}
}
