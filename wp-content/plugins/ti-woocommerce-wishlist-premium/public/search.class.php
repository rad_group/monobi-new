<?php
/**
 * Search wishlist shortcode
 *
 * @since             1.0.0
 * @package           TInvWishlist\Public
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Search wishlist shortcode
 */
class TInvWL_Public_Search {

	/**
	 * Plugin name
	 *
	 * @var string
	 */
	private $_n;
	/**
	 * Default setting for shortcode
	 *
	 * @var array
	 */
	private $default;
	/**
	 * This class
	 *
	 * @var \TInvWL_Public_Search
	 */
	protected static $_instance = null;

	/**
	 * Get this class object
	 *
	 * @param string $plugin_name Plugin name.
	 *
	 * @return \TInvWL_Public_Search
	 */
	public static function instance( $plugin_name = TINVWL_PREFIX ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $plugin_name );
		}

		return self::$_instance;
	}

	/**
	 * Constructor
	 *
	 * @param string $plugin_name Plugin name.
	 */
	function __construct( $plugin_name ) {
		$this->_n = $plugin_name;
		$this->define_hooks();
	}

	/**
	 * Defined shortcode
	 */
	function define_hooks() {
	}

	/**
	 * Output page
	 *
	 * @param array $atts Array parameter for shortcode.
	 *
	 * @return boolean
	 */
	function htmloutput( $atts ) {
		$search = get_query_var( 'tiws' );
		tinv_wishlist_template( 'ti-wishlist-searchform.php', array(
			'search'                 => $search,
			'button_text'            => $atts['button_text'],
			'placeholder_input_text' => $atts['input_text'],
		) );

		if ( empty( $search ) && ! is_page( apply_filters( 'wpml_object_id', tinv_get_option( 'page', 'search' ), 'page', true ) ) ) {
			return '';
		}

		$default_wl = '';
		if ( empty( $search ) || false !== mb_strpos( mb_strtolower( apply_filters( 'tinvwl-general-default_title', tinv_get_option( 'general', 'default_title' ) ) ), mb_strtolower( $search ) ) ) {
			$default_wl = " OR `title` = '' ";
		}
		$search = '%' . $search . '%';

		$wl    = new TInvWL_Wishlist( $this->_n );
		$paged = get_query_var( 'paged', 1 );
		$paged = 1 < $paged ? $paged : 1;

		$data = array(
			'status'   => 'public',
			'order_by' => 'date',
			'order'    => 'DESC',
			'count'    => absint( $atts['lists_per_page'] ),
			'offset'   => absint( $atts['lists_per_page'] ) * ( $paged - 1 ),
		);
		global $wpdb;
		$users_id = $wpdb->get_col( $wpdb->prepare( "SELECT `A`.`ID` FROM `{$wpdb->users}` AS `A` INNER JOIN `{$wpdb->usermeta}` AS `B` ON `A`.`ID` = `B`.`user_id` WHERE `B`.`meta_key` IN ('first_name', 'last_name') AND (`A`.`display_name` LIKE %s OR `A`.`user_email` LIKE %s OR `B`.`meta_value` LIKE %s ) GROUP BY `A`.`ID`", $search, $search, $search ) ); // @codingStandardsIgnoreLine WordPress.VIP.RestrictedVariables.user_meta
		if ( ! empty( $users_id ) ) {
			$default_wl .= ' OR `author` IN(' . implode( ',', $users_id ) . ') ';
		}
		if ( is_user_logged_in() ) {
			$data['sql'] = $wpdb->prepare( "SELECT * FROM `{table}` WHERE (`status`='public' OR `author`=%d) AND (`title` LIKE %s {$default_wl})", get_current_user_id(), $search ); // @codingStandardsIgnoreLine WordPress.WP.PreparedSQL.NotPrepared
		} else {
			$data['sql'] = $wpdb->prepare( "SELECT * FROM `{table}` WHERE `status`='public' AND (`title` LIKE %s {$default_wl})", $search ); // @codingStandardsIgnoreLine WordPress.WP.PreparedSQL.NotPrepared
		}
		if ( 'yes' === $atts['show_navigation'] ) {
			$pages = ceil( count( $wl->get( $data ) ) / absint( $atts['lists_per_page'] ) );
			if ( 1 < $paged ) {
				add_action( 'tinvwl_pagenation_wishlistpublic_table', array( $this, 'page_prev' ) );
			}
			if ( $pages > $paged ) {
				add_action( 'tinvwl_pagenation_wishlistpublic_table', array( $this, 'page_next' ) );
			}
		}

		$data['sql'] .= ' ORDER BY `{order_by}` {order} LIMIT {offset},{count};';
		$wishlists   = apply_filters( 'tinvwl_search_prepare_results', $wl->get( $data ) );

		if ( empty( $wishlists ) ) {
			return tinv_wishlist_template( 'ti-wishlist-searchform-empty.php' );
		}

		$data = array(
			'wishlists' => $wishlists,
		);
		tinv_wishlist_template( 'ti-wishlist-public.php', $data );
	}

	/**
	 * Prev page button
	 */
	function page_prev() {
		$paged = get_query_var( 'paged', 1 );
		$paged = 1 < $paged ? $paged - 1 : 0;
		$this->page( $paged, sprintf( '<i class="fa fa-chevron-left"></i>%s', __( 'Previous Page', 'ti-woocommerce-wishlist-premium' ) ), array( 'class' => 'button tinv-prev' ) );
	}

	/**
	 * Next page button
	 */
	function page_next() {
		$paged = get_query_var( 'paged', 1 );
		$paged = 1 < $paged ? $paged + 1 : 2;
		$this->page( $paged, sprintf( '%s<i class="fa fa-chevron-right"></i>', __( 'Next Page', 'ti-woocommerce-wishlist-premium' ) ), array( 'class' => 'button tinv-next' ) );
	}

	/**
	 * Page button
	 *
	 * @param integer $paged Index page.
	 * @param string $text Text button.
	 * @param style $style Style attribute.
	 *
	 * @return boolean
	 */
	function page( $paged, $text, $style = array() ) {
		$paged = absint( $paged );
		$page  = tinv_get_option( 'page', 'search' );
		if ( empty( $page ) ) {
			return false;
		}
		if ( is_array( $style ) ) {
			$style = TInvWL_Form::__atrtostr( $style );
		}
		$link = get_permalink( $page );
		if ( get_option( 'permalink_structure' ) ) {
			$link .= 'page/' . $paged . '/?tiws=' . get_query_var( 'tiws', '' );
		} else {
			$link .= '&paged=' . $paged . '&tiws=' . get_query_var( 'tiws', '' );
		}
		printf( '<a href="%s" %s>%s</a>', esc_url( $link ), $style, $text ); // WPCS: xss ok.
	}

	/**
	 * Shortcode basic function
	 *
	 * @param array $atts Array parameter from shortcode.
	 *
	 * @return string
	 */
	function shortcode( $atts = array() ) {
		$default = array(
			'lists_per_page'  => 10,
			'button_text'     => __( 'Search for a Wishlist', 'ti-woocommerce-wishlist-premium' ),
			'input_text'      => __( 'Type a name or email', 'ti-woocommerce-wishlist-premium' ),
			'show_navigation' => 'yes',
		);
		$atts    = shortcode_atts( $default, $atts );

		if ( tinv_get_option( 'page', 'search' ) ) {
			ob_start();
			$this->htmloutput( $atts );

			return ob_get_clean();
		} else {
			return '';
		}
	}
}
