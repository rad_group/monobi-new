<?php
/**
 * Basic email class
 *
 * @since             1.0.0
 * @package           TInvWishlist\Public
 * @subpackage		  Email
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Basic email class
 */
abstract class TInvWL_Public_Email extends WC_Email {

	/**
	 * Plugin name
	 *
	 * @var string
	 */
	public $_n;

	/**
	 * Plugin version
	 *
	 * @var string
	 */
	public $_v;

	/**
	 * Constructor
	 *
	 * @param string $plugin_name Plugin name.
	 * @param string $version Plugin version.
	 */
	function __construct( $plugin_name, $version ) {
		$this->_n	 = $plugin_name;
		$this->_v	 = $version;
		$this->load_data();
		$this->set_templates();
		parent::__construct();
		add_filter( 'woocommerce_email_get_option', array( $this, 'get_option_tinvwl' ), 10, 4 );
		add_filter( 'woocommerce_email_enabled_' . $this->id, array( $this, 'enabled_tinvwl' ), 10, 1 );
	}

	/**
	 * Set email defaults
	 */
	function load_data() {

	}

	/**
	 * Get current template path
	 *
	 * @param string  $template Name template.
	 * @param boolean $plain Plain or HTML template?.
	 * @param string  $emailtemplate Name skin template.
	 * @return string
	 */
	function loadtemplates( $template, $plain = false, $emailtemplate ) {
		$curtemplate	 = tinv_template();
		$template_name	 = 'emails' . DIRECTORY_SEPARATOR . ( $plain ? 'plain' . DIRECTORY_SEPARATOR : '' ) . $template . $emailtemplate . '.php';
		if ( ! empty( $curtemplate ) ) {
			if ( file_exists( TINVWL_PATH . implode( DIRECTORY_SEPARATOR, array( 'templates', $curtemplate, $template_name ) ) ) ) {
				return $template_name;
			}
		}
		if ( file_exists( TINVWL_PATH . implode( DIRECTORY_SEPARATOR, array( 'templates', $template_name ) ) ) ) {
			return $template_name;
		}
		return 'emails' . DIRECTORY_SEPARATOR . ( $plain ? 'plain' . DIRECTORY_SEPARATOR : '' ) . $template . '.php';
	}

	/**
	 * Set template email
	 */
	function set_templates() {
		$emailtemplate = tinv_template_email( get_class( $this ) );
		$this->set_template( $emailtemplate );
	}

	/**
	 * Set template email
	 *
	 * @param string $emailtemplate Email template name.
	 */
	function set_template( $emailtemplate = '' ) {
		$this->template_html	 = $this->loadtemplates( $this->template_name, false, $emailtemplate );
		$this->template_plain	 = $this->loadtemplates( $this->template_name, true, $emailtemplate );
	}

	/**
	 * Get list templates
	 *
	 * @param string $template Name template.
	 * @return array
	 */
	public static function templates( $template ) {
		$paths = array(
			TINVWL_PATH . implode( DIRECTORY_SEPARATOR, array( 'templates', 'emails', $template ) ),
		);

		$curtemplate = tinv_template();
		if ( ! empty( $curtemplate ) ) {
			array_unshift( $paths, TINVWL_PATH . implode( DIRECTORY_SEPARATOR, array( 'templates', $curtemplate, 'emails', $template ) ) );
		}
		$templates = array();
		foreach ( $paths as $path ) {
			$template_paths = glob( $path . '*.php' );
			foreach ( $template_paths as $value ) {
				$value = preg_replace( '/(^' . $template . '|\.php$)/i', '', basename( $value ) );

				$templates[ $value ] = empty( $value ) ? __( 'Default', 'ti-woocommerce-wishlist-premium' ) : ucfirst( $value );
			}
		}
		$templates = array_unique( $templates );
		asort( $templates );

		return apply_filters( 'tinvwl_get_list_email_templates', $templates, $template );
	}

	/**
	 * Update status email
	 *
	 * @param boolean $value Woocommerce status.
	 * @return boolean
	 */
	function enabled_tinvwl( $value ) {
		$option_name = str_replace( $this->_n . '_', '', $this->id );
		$_value		 = tinv_get_option( $option_name, 'enabled' );
		if ( is_null( $_value ) ) {
			return $value;
		}
		return $_value;
	}

	/**
	 * It replaces the value to the value of the plugin
	 *
	 * @param mixed  $value Set value.
	 * @param object $_this Object for validation id.
	 * @param mixed  $_value New Value.
	 * @param string $key key field.
	 * @return mixed
	 */
	function get_option_tinvwl( $value, $_this, $_value, $key ) {
		if ( $this->id === $_this->id ) {
			$option_name = str_replace( $this->_n . '_', '', $this->id );
			$_value		 = tinv_get_option( $option_name, $key );
			if ( is_null( $_value ) ) {
				return $value;
			}
			if ( is_bool( $_value ) ) {
				$_value = $_value ? 'yes' : 'no';
			}
			return $_value;
		}
		return $value;
	}

	/**
	 * Save value to plugin
	 */
	function process_admin_options() {
		parent::process_admin_options();

		$option_name = str_replace( $this->_n . '_', '', $this->id );
		$post_data	 = $this->get_post_data();

		foreach ( $this->get_form_fields() as $key => $field ) {
			try {
				$value = $this->get_field_value( $key, $field, $post_data );
				if ( 'checkbox' === $this->get_field_type( $field ) ) {
					$value = 'yes' === $value;
				}
				tinv_update_option( $option_name, $key, $value );
			} catch ( Exception $e ) {
				$this->add_error( $e->getMessage() );
			}
		}
	}
}
