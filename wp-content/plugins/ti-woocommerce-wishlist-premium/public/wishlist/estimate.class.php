<?php
/**
 * Ask for Estimate actions button functional
 *
 * @since             1.0.0
 * @package           TInvWishlist\Public
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Ask for Estimate actions button functional
 */
class TInvWL_Public_Wishlist_Estimate {

	/**
	 * First run method
	 *
	 * @param array $wishlist Set from action.
	 *
	 * @return boolean
	 */
	public static function init( $wishlist ) {
		if ( apply_filters( 'tinvwl_estimate_guest_disable', true ) ) {
			if ( ! is_user_logged_in() || ! tinv_get_option( 'estimate_button', 'allow' ) ) {
				return false;
			}
			if ( get_current_user_id() !== $wishlist['author'] ) {
				return false;
			}
		} else {
			if ( ! $wishlist['is_owner'] || ! tinv_get_option( 'estimate_button', 'allow' ) ) {
				return false;
			}
		}
		$nonce = filter_input( INPUT_POST, 'tinvwl_estimate_nonce' );
		if ( $nonce && wp_verify_nonce( $nonce, "tinvwl_check_estimate_{$wishlist['ID']}" ) ) {
			self::send( $wishlist );
		}

		self::htmloutput( $wishlist );
	}

	/**
	 * Output function
	 *
	 * @param array $wishlist Set from action.
	 */
	public static function htmloutput( $wishlist ) {

		$data = array(
			'wishlist'           => $wishlist,
			'wishlist_id'        => $wishlist['ID'],
			'estimate_note'      => tinv_get_option( 'estimate_button', 'notes' ),
			'estimate_note_text' => apply_filters( 'tinvwl-estimate_button-text_notes', tinv_get_option( 'estimate_button', 'text_notes' ) ),
		);
		tinv_wishlist_template( 'ti-wishlist-estimate.php', $data );
	}

	/**
	 * Action send email
	 *
	 * @param array $wishlist Set from action.
	 *
	 * @return boolean
	 */
	public static function send( $wishlist ) {
		$wishlist_id = filter_input( INPUT_POST, 'tinvwl_estimate', FILTER_VALIDATE_INT );
		if ( $wishlist_id !== $wishlist['ID'] ) {
			return false;
		}

		$note = '';
		if ( tinv_get_option( 'estimate_button', 'notes' ) ) {
			$note = tinv_get_option( 'estimate_button', 'notes' ) ? filter_input( INPUT_POST, 'estimate_note', FILTER_SANITIZE_STRING ) : '';
		}
		if ( function_exists( 'WC' ) ) {
			WC()->mailer();
		}
		add_action( TINVWL_PREFIX . '_estimate_email_error', array(
			'TInvWL_Public_Wishlist_Estimate',
			'message_error'
		), 10, 4 );
		add_action( TINVWL_PREFIX . '_estimate_email_successfully', array(
			'TInvWL_Public_Wishlist_Estimate',
			'message_successfully'
		), 10, 3 );
		do_action( 'tinvwl_send_ask_for_estimate', $wishlist, $note, apply_filters( 'tinvwl_send_ask_for_estimate_args', array() ) );
	}

	/**
	 * Error message send estimate
	 *
	 * @param string $recipient Recipients.
	 * @param object $user User.
	 * @param array $wishlist Wishlist.
	 * @param string $error Error message.
	 */
	public static function message_error( $recipient, $user, $wishlist, $error ) {
		wc_add_notice( sprintf( __( 'Ask for Estimate for Wishlist "%1$s" is not sent! %2$s', 'ti-woocommerce-wishlist-premium' ), $wishlist['title'], $error ), 'error' );
		echo '<script type="text/javascript">window.location = window.location.href;</script>';
	}

	/**
	 * Successfully message send estimate
	 *
	 * @param string $recipient Recipients.
	 * @param object $user User.
	 * @param array $wishlist Wishlist.
	 */
	public static function message_successfully( $recipient, $user, $wishlist ) {
		wc_add_notice( sprintf( __( 'Ask for Estimate for Wishlist "%s" is successfully sent!', 'ti-woocommerce-wishlist-premium' ), $wishlist['title'] ) );
		echo '<script type="text/javascript">window.location = window.location.href;</script>';
	}
}
