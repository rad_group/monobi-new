<?php
/**
 * Drop down widget
 *
 * @since             1.4.0
 * @package           TInvWishlist\Public
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Drop down widget
 */
class TInvWL_Public_TopWishlist {

	/**
	 * Plugin name
	 *
	 * @var string
	 */
	static $_n;
	/**
	 * Name for GET attribute action
	 *
	 * @var string
	 */
	static $_get_atribute = 'tinv-miniwishlist-action';
	/**
	 * This class
	 *
	 * @var \TInvWL_Public_TopWishlist
	 */
	protected static $_instance = null;

	/**
	 * Get this class object
	 *
	 * @param string $plugin_name Plugin name.
	 *
	 * @return \TInvWL_Public_TopWishlist
	 */
	public static function instance( $plugin_name = TINVWL_PREFIX ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $plugin_name );
		}

		return self::$_instance;
	}

	/**
	 * Constructor
	 *
	 * @param string $plugin_name Plugin name.
	 */
	function __construct( $plugin_name ) {
		self::$_n = $plugin_name;
		$this->define_hooks();
	}

	/**
	 * Define hooks
	 */
	function define_hooks() {
		add_filter( 'tinvwl_addtowishlist_return_ajax', array( __CLASS__, 'update_widget' ) );
		add_filter( 'woocommerce_add_to_cart_fragments', array( __CLASS__, 'update_fragments' ) );
		add_filter( 'tinvwl_after_mini_wishlist', array( __CLASS__, 'button_view_wishlist' ) );
		if ( ! tinv_get_option( 'general', 'multi' ) ) {
			add_filter( 'tinvwl_after_mini_wishlist', array( __CLASS__, 'button_all_to_cart' ) );
		}
		add_action( 'wp_loaded', array( __CLASS__, 'apply_action' ), 0 );
	}

	/**
	 * Apply actions for mini wishlist
	 */
	public static function apply_action() {
		$action = filter_input( INPUT_GET, self::$_get_atribute, FILTER_DEFAULT );
		switch ( $action ) {
			case 'add-all-to-cart':
				$wishlist = tinv_wishlist_get();
				TInvWL_Public_Wishlist_Buttons::add_all( $wishlist );
				wp_redirect( remove_query_arg( self::$_get_atribute ) );
				die();
				break;
			default:
				if ( ! empty( $action ) ) {
					do_action( 'tinvwl_mini_wishlist_action_' . $action );
					wp_redirect( remove_query_arg( self::$_get_atribute ) );
					die();
				}
				break;
		}
	}

	/**
	 * Show button for view wishlist after mini wishlist
	 */
	public static function button_view_wishlist( $products ) {
		if ( empty( $products ) ) {
			return;
		}
		if ( tinv_get_option( 'general', 'multi' ) && is_user_logged_in() ) {
			echo sprintf( '<a href="%1$s" class="button tinv-view-wishlist">%2$s</a>', esc_url( get_permalink( apply_filters( 'wpml_object_id', tinv_get_option( 'page', 'manage' ), 'page', true ) ) ), esc_html__( 'View My Wishlists', 'ti-woocommerce-wishlist-premium' ) );
		} else {
			echo sprintf( '<a href="%1$s" class="button tinv-view-wishlist">%2$s</a>', esc_url( tinv_url_wishlist_default() ), esc_html__( 'View Wishlist', 'ti-woocommerce-wishlist-premium' ) );
		}
	}

	/**
	 * Show button for add all product to cart after mini wishlist
	 */
	public static function button_all_to_cart( $products ) {
		if ( empty( $products ) ) {
			return;
		}
		echo sprintf( '<a href="%1$s" class="button tinv-add-all-to-cart">%2$s</a>', esc_attr( add_query_arg( self::$_get_atribute, 'add-all-to-cart' ) ), tinv_get_option( 'table', 'text_add_all_to_cart' ) ); // WPCS: xss ok.
	}

	/**
	 * Output shortcode
	 *
	 * @param array $atts Shortcode attributes.
	 */
	function htmloutput( $atts ) {
		$data = array(
			'icon'         => tinv_get_option( 'topline', 'icon' ),
			'icon_class'   => ( $atts['show_icon'] && tinv_get_option( 'topline', 'icon' ) ) ? 'top_wishlist-' . tinv_get_option( 'topline', 'icon' ) : '',
			'icon_style'   => ( $atts['show_icon'] && tinv_get_option( 'topline', 'icon' ) ) ? esc_attr( 'top_wishlist-' . tinv_get_option( 'topline', 'icon_style' ) ) : '',
			'icon_upload'  => tinv_get_option( 'topline', 'icon_upload' ),
			'text'         => $atts['show_text'] ? $atts['text'] : '',
			'counter'      => $atts['show_counter'],
			'drop_down'    => $atts['drop_down'],
			'show_counter' => $atts['show_counter'],
			'use_link'     => $atts['link'],
			'link'         => ( tinv_get_option( 'general', 'multi' ) && is_user_logged_in() ) ? get_permalink( apply_filters( 'wpml_object_id', tinv_get_option( 'page', 'manage' ), 'page', true ) ) : ( ( tinv_get_option( 'general', 'require_login' ) && ! is_user_logged_in() ) ? wc_get_page_permalink( 'myaccount' ) : tinv_url_wishlist_default() ),
		);
		tinv_wishlist_template( 'ti-wishlist-product-counter.php', $data );
	}

	/**
	 * AJAX update elements.
	 *
	 * @param array $data AJAX data.
	 *
	 * @return array
	 */
	public static function update_widget( $data ) {
		$data['fragments'] = self::update_fragments( array() );

		return $data;
	}

	/**
	 * Load fragments for wishlist product counter
	 *
	 * @param array $data Woocommerce Fragments for updateing data.
	 */
	public static function update_fragments( $data = array() ) {
		ob_start();
		self::mini_wishlist();
		$data['div.tinv_mini_wishlist_list']           = ob_get_clean();
		$data['span.wishlist_products_counter_number'] = sprintf( '<span class="wishlist_products_counter_number">%d</span>', self::counter() );

		return $data;
	}

	/**
	 * Get count product in all wishlist
	 *
	 * @return integer
	 */
	public static function counter() {
		$count = 0;
		$wl    = new TInvWL_Wishlist();
		if ( is_user_logged_in() ) {
			$wishlists = array();
			if ( tinv_get_option( 'general', 'multi' ) ) {
				$wishlists = $wl->get_by_user( 0, array(
					'count' => 999999,
				) );
			} else {
				$wishlists[] = $wl->add_user_default();
			}
			$_wishlists = array();
			foreach ( $wishlists as $key => $wishlist ) {
				$_wishlists[] = $wishlist['ID'];
			}
			$wlp    = new TInvWL_Product();
			$counts = $wlp->get( array(
				'external'    => false,
				'wishlist_id' => $_wishlists,
				'sql'         => sprintf( 'SELECT %s(`quantity`) AS `quantity` FROM {table} WHERE {where}', ( tinv_get_option( 'general', 'quantity_func' ) ? 'SUM' : 'COUNT' ) ),
			) );
			$counts = array_shift( $counts );
			$count  = absint( $counts['quantity'] );
		} else {
			$wishlist = $wl->get_by_sharekey_default();
			if ( ! empty( $wishlist ) ) {
				$wishlist = array_shift( $wishlist );
				$wlp      = new TInvWL_Product( $wishlist );
				$counts   = $wlp->get_wishlist( array(
					'external' => false,
					'sql'      => sprintf( 'SELECT %s(`quantity`) AS `quantity` FROM {table} WHERE {where}', ( tinv_get_option( 'general', 'quantity_func' ) ? 'SUM' : 'COUNT' ) ),
				) );
				$counts   = array_shift( $counts );
				$count    = absint( $counts['quantity'] );
			}
		}

		return $count;
	}

	/**
	 * Show mini wishlist.
	 *
	 * @param array $args arguments for show mini wishlist.
	 */
	public static function mini_wishlist( $args = array() ) {
		$defaults              = array(
			'list_class'        => '',
			'show_wishlist'     => 'on',
			'positios_wishlist' => tinv_get_option( 'topline', 'drop_down_position_wishlist' ),
			'count_product'     => tinv_get_option( 'topline', 'drop_down_count_product' ),
		);
		$args                  = wp_parse_args( $args, $defaults );
		$args['count_product'] = absint( $args['count_product'] );
		if ( ! $args['count_product'] ) {
			$args['count_product'] = 10;
		}
		if ( ! tinv_get_option( 'general', 'multi' ) ) {
			$args['show_wishlist'] = 'off';
		}
		$products       = array();
		$products_count = self::counter();
		if ( is_user_logged_in() ) {
			$wl        = new TInvWL_Wishlist();
			$wishlists = array();
			if ( tinv_get_option( 'general', 'multi' ) ) {
				$wishlists = $wl->get_by_user( 0, array(
					'count' => 999999,
				) );
			} else {
				$wishlists[] = $wl->add_user_default();
			}
			$_wishlists = array();
			foreach ( $wishlists as $key => $wishlist ) {
				$id                = $wishlist['ID'];
				$wishlist['url']   = tinv_url_wishlist_by_key( $wishlist['share_key'] );
				$_wishlists[ $id ] = $wishlist;
			}
			$wishlists = $_wishlists;
			unset( $_wishlists );
			$wlp      = new TInvWL_Product();
			$data     = array(
				'count'       => $args['count_product'],
				'order'       => 'DESC',
				'order_by'    => 'date',
				'wishlist_id' => array_keys( $wishlists ),
				'sql'         => 'SELECT {field}, GROUP_CONCAT(`wishlist_id`) AS `wishlist_id`, COUNT(`ID`) AS `counts`, SUM(`quantity`) AS `quantity_new` FROM `{table}` WHERE {where} GROUP BY `product_id`,`variation_id`,`formdata` ORDER BY `{order_by}` {order} LIMIT {offset},{count}',
			);
			$products = $wlp->get( $data );
			foreach ( $products as $key => $product ) {
				$product['wishlist_id'] = explode( ',', $product['wishlist_id'] );
				$product['wishlists']   = array();
				foreach ( $product['wishlist_id'] as $wishlist_id ) {
					if ( array_key_exists( $wishlist_id, $wishlists ) ) {
						$product['wishlists'][ $wishlist_id ] = $wishlists[ $wishlist_id ];
					}
				}
				$products[ $key ] = $product;
			}
		} else {
			$args['show_wishlist'] = 'off';

			$wl       = new TInvWL_Wishlist();
			$wishlist = $wl->get_by_sharekey_default();
			$products = array();
			if ( ! empty( $wishlist ) ) {
				$wishlist = array_shift( $wishlist );
				$wlp      = new TInvWL_Product( $wishlist );
				$products = $wlp->get_wishlist( array(
					'count'    => $args['count_product'],
					'order'    => 'DESC',
					'order_by' => 'date',
				) );
			}
			foreach ( $products as $key => $product ) {
				$product['quantity_new'] = $product['quantity'];
				$product['counts']       = 1;
				$products[ $key ]        = $product;
			}
		} // End if().

		$default_wishlist = array(
			'title' => apply_filters( 'tinvwl-general-default_title', tinv_get_option( 'general', 'default_title' ) ),
			'url'   => tinv_url_wishlist_default(),
		);
		foreach ( $products as $key => $product ) {
			if ( ! array_key_exists( 'wishlists', $product ) && empty( $product['wishlists'] ) ) {
				$product['wishlists'][0] = $default_wishlist;
			}
			if ( ! tinv_get_option( 'general', 'quantity_func' ) ) {
				$product['quantity'] = $product['counts'];
			} else {
				$product['quantity'] = $product['quantity_new'];
			}
			$products[ $key ] = $product;
		}

		$args['show_wishlist'] = filter_var( $args['show_wishlist'], FILTER_VALIDATE_BOOLEAN );
		tinv_wishlist_template( 'ti-miniwishlist.php', array(
			'args'           => $args,
			'products'       => $products,
			'subtotal_count' => $products_count,
		) );
	}

	/**
	 * Shortcode basic function
	 *
	 * @param array $atts Array parameter from shortcode.
	 *
	 * @return string
	 */
	function shortcode( $atts = array() ) {
		$default = array(
			'show_icon'    => (bool) tinv_get_option( 'topline', 'icon' ),
			'show_text'    => tinv_get_option( 'topline', 'show_text' ),
			'show_counter' => tinv_get_option( 'topline', 'counter' ),
			'text'         => apply_filters( 'tinvwl-topline-text', tinv_get_option( 'topline', 'text' ) ),
			'drop_down'    => tinv_get_option( 'topline', 'drop_down' ),
			'link'         => tinv_get_option( 'topline', 'link' ),
		);
		$atts    = filter_var_array( shortcode_atts( $default, $atts ), array(
			'show_icon'    => FILTER_VALIDATE_BOOLEAN,
			'show_text'    => FILTER_VALIDATE_BOOLEAN,
			'show_counter' => FILTER_VALIDATE_BOOLEAN,
			'drop_down'    => FILTER_VALIDATE_BOOLEAN,
			'link'         => FILTER_VALIDATE_BOOLEAN,
			'text'         => FILTER_DEFAULT,
		) );
		ob_start();
		$this->htmloutput( $atts );

		return ob_get_clean();
	}
}
