<?php
/**
 * Ask for Estimate create email
 *
 * @since             1.0.0
 * @package           TInvWishlist\Public
 * @subpackage		  Email
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Ask for Estimate create email
 */
class TInvWL_Public_Email_Estimate extends TInvWL_Public_Email {

	/**
	 * Run method send mail
	 *
	 * @param array  $wishlist Wishlist object.
	 * @param string $note Description for email.
	 * @return boolean
	 */
	function trigger( $wishlist, $note, $args ) {
		if ( ! $this->is_enabled() ) {
			return false;
		}
		if ( empty( $wishlist ) || ! array_key_exists( 'ID', (array) $wishlist ) ) {
			return false;
		} else {
			$this->wishlist = $wishlist;
		}

		$this->heading	 = $this->get_option( 'heading' );
		$this->subject	 = $this->get_option( 'subject' );

		$this->notes = $note;

		// Get products fin this wishlist.
		$wlp = new TInvWL_Product( $this->wishlist, $this->_n );

		$this->wishlist['products'] = $wlp->get_wishlist( array( 'count' => 9999999 ) );

		// Get wishlist url.
		$this->wishlist['url'] = tinv_url_wishlist( $wishlist['ID'] );

		// Get user info.
		$user = get_user_by( 'id', $this->wishlist['author'] );

		if ( $user && $user->exists() ) {
			$this->wishlist['author_display_name']	 = $user->display_name;
			$this->wishlist['author_user_email']	 = $user->user_email;
		} else {
			$this->wishlist['author_display_name']	 = '';
			$this->wishlist['author_user_email']	 = '';
		}

		// This sets the recipient to the settings defined below in init_form_fields().
		$this->recipient = $this->get_option( 'recipient' );

		// If none was entered, just use the WP admin email as a fallback.
		if ( ! $this->recipient ) {
			$this->recipient = get_option( 'admin_email' );
		}

		// Dublicate copy for user.
		$this->copy	 = $this->get_option( 'copy' );
		$this->copy	 = 'yes' === $this->copy;
		$this->args	 = $args;

		$result = $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
		if ( is_wp_error( $result ) ) {
			do_action( $this->_n . '_estimate_email_error', $this->get_recipient(), $user, $this->wishlist, $result->get_error_message() );
		} else {
			do_action( $this->_n . '_estimate_email_successfully', $this->get_recipient(), $user, $this->wishlist );
		}
	}

	/**
	 * Update header for Send CC copy
	 *
	 * @return string
	 */
	function get_headers() {
		$headers = array( 'Reply-to: ' . $this->wishlist['author_user_email'] );
		if ( $this->copy ) {
			$headers[] = 'Cc: ' . $this->wishlist['author_user_email'];
		}
		$headers[]	 = 'Content-Type: ' . $this->get_content_type();
		$headers	 = implode( "\r\n", $headers );

		return apply_filters( 'woocommerce_email_headers', $headers, $this->id, $this );
	}

	/**
	 * Get content html function
	 *
	 * @return string
	 */
	public function get_content_html() {
		ob_start();
		tinv_wishlist_template( $this->template_html, apply_filters( 'tinvwl_estimate_email_data_template_html', array(
			'wishlist'				 => $this->wishlist,
			'additional_note'		 => $this->notes,
			'additional_arguments'	 => $this->args,
			'wishlist_table_row'	 => tinv_get_option( 'product_table' ),
			'email_heading'			 => $this->get_heading(),
			'blogname'				 => $this->get_blogname(),
			'sent_to_admin'			 => true,
			'plain_text'			 => false,
			'email'					 => $this,
		) ) );
		return ob_get_clean();
	}

	/**
	 * Get content plain function
	 *
	 * @return string
	 */
	public function get_content_plain() {
		ob_start();
		tinv_wishlist_template( $this->template_plain, apply_filters( 'tinvwl_estimate_email_data_template_plain', array(
			'wishlist'				 => $this->wishlist,
			'additional_note'		 => $this->notes,
			'additional_arguments'	 => $this->args,
			'wishlist_table_row'	 => tinv_get_option( 'product_table' ),
			'email_heading'			 => $this->get_heading(),
			'blogname'				 => $this->get_blogname(),
			'sent_to_admin'			 => true,
			'plain_text'			 => true,
			'email'					 => $this,
		) ) );
		return ob_get_clean();
	}

	/**
	 * Set email defaults
	 */
	function load_data() {
		$this->id = $this->_n . '_estimate_email';
		$this->title = __( 'Wishlist Ask for Estimate', 'ti-woocommerce-wishlist-premium' );
		$this->description = __( 'This email is sent when a user click the button "Ask for Estimate".', 'ti-woocommerce-wishlist-premium' );

		// These are the default heading and subject lines that can be overridden using the settings.
		$this->heading	 = __( 'Estimate Request', 'ti-woocommerce-wishlist-premium' );
		$this->subject	 = __( 'Estimate Request', 'ti-woocommerce-wishlist-premium' );

		// These define the locations of the templates that this email should use, we'll just use the new order template since this email is similar.
		$this->template_name = 'ti-estimate';

		// Trigger on new paid orders.
		add_action( $this->_n . '_send_ask_for_estimate', array( $this, 'trigger' ), 10, 3 );
	}

	/**
	 * Get list templates
	 *
	 * @return array
	 */
	public static function gettemplates() {
		return self::templates( 'ti-estimate' );
	}

	/**
	 * Save value to plugin
	 */
	function process_admin_options() {
		parent::process_admin_options();
		$option_name = str_replace( $this->_n . '_', '', $this->id );
		$enabled	 = tinv_get_option( $option_name, 'enabled' );
		tinv_update_option( 'estimate_button', 'allow', $enabled );
	}

	/**
	 * Initialise Settings Form Fields
	 */
	public function init_form_fields() {
		$this->form_fields = array(
			'enabled'	 => array(
				'title'		 => __( 'Enable/Disable', 'ti-woocommerce-wishlist-premium' ),
				'type'		 => 'checkbox',
				'label'		 => __( 'Enable this email notification', 'ti-woocommerce-wishlist-premium' ),
				'default'	 => 'yes',
			),
			'subject'	 => array(
				'title'			 => __( 'Email Subject', 'ti-woocommerce-wishlist-premium' ),
				'type'			 => 'text',
				'description'	 => sprintf( __( 'Defaults to <code>%s</code>', 'ti-woocommerce-wishlist-premium' ), $this->subject ),
				'default'		 => $this->subject,
				'desc_tip'		 => true,
			),
			'heading'	 => array(
				'title'			 => __( 'Email Heading', 'ti-woocommerce-wishlist-premium' ),
				'type'			 => 'text',
				'description'	 => sprintf( __( 'Defaults to <code>%s</code>', 'ti-woocommerce-wishlist-premium' ), $this->heading ),
				'default'		 => $this->heading,
				'desc_tip'		 => true,
			),
			'recipient'	 => array(
				'title'			 => __( 'Recipient(s)', 'ti-woocommerce-wishlist-premium' ),
				'type'			 => 'textarea',
				'description'	 => sprintf( __( 'Enter recipients (comma separated) for this email. Defaults to <code>%s</code>.', 'ti-woocommerce-wishlist-premium' ), esc_attr( get_option( 'admin_email' ) ) ),
				'default'		 => get_option( 'admin_email' ),
				'css'			 => 'width:350px;resize:none;',
			),
			'copy'		 => array(
				'title'			 => __( 'CC copy', 'ti-woocommerce-wishlist-premium' ),
				'type'			 => 'checkbox',
				'label'			 => __( 'Send CC copy to wishlist owner', 'ti-woocommerce-wishlist-premium' ),
				'default'		 => '',
			),
			'email_type' => array(
				'title'			 => __( 'Email type', 'ti-woocommerce-wishlist-premium' ),
				'type'			 => 'select',
				'description'	 => __( 'Choose which format of email to send.', 'ti-woocommerce-wishlist-premium' ),
				'default'		 => 'html',
				'class'			 => 'email_type wc-enhanced-select',
				'options'		 => $this->get_email_type_options(),
				'desc_tip'		 => true,
			),
		);
	}
}
