<?php
/**
 * Wishlist Notification a change price product create email
 *
 * @since             1.0.0
 * @package           TInvWishlist\Public
 * @subpackage		  Email
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Wishlist Notification a change price product create email
 */
class TInvWL_Public_Email_NotificationPrice extends TInvWL_Public_Email {

	/**
	 * Set email defaults
	 */
	function load_data() {
		$this->id = $this->_n . '_notification_price_email';
		$this->title = __( 'Wishlist Notification a change price', 'ti-woocommerce-wishlist-premium' );
		$this->description = __( 'This notification will be sent to customers if the price of a product has been decreased and they have this product in a Wishlist.', 'ti-woocommerce-wishlist-premium' );

		// These are the default heading and subject lines that can be overridden using the settings.
		$this->heading	 = __( 'Product Price Decreased!', 'ti-woocommerce-wishlist-premium' );
		$this->subject	 = __( 'A product of your wishlist is on sale', 'ti-woocommerce-wishlist-premium' );

		// These define the locations of the templates that this email should use, we'll just use the new order template since this email is similar.
		$this->template_name = 'ti-notification-price';

		// Trigger on new paid orders.
		add_action( $this->_n . '_send_notification_price', array( $this, 'trigger' ), 10, 4 );

		// This sets the recipient to the settings defined below in init_form_fields().
		$this->customer_email	 = true;
	}

	/**
	 * Get list templates
	 *
	 * @return array
	 */
	public static function gettemplates() {
		return self::templates( 'ti-notification-price' );
	}

	/**
	 * Run method send mail
	 *
	 * @param object  $product Promotion product.
	 * @param integer $user_id User ID Promotional.
	 * @param array   $wishlists An array of wishlists in which the product is be found.
	 * @return boolean
	 */
	function trigger( $product, $user_id, $wishlists = array() ) {
		if ( empty( $product ) || empty( $user_id ) ) {
			return false;
		}

		$this->heading	 = $this->get_option( 'heading' );
		$this->subject	 = $this->get_option( 'subject' );

		// Prepare Product.
		$this->product = $product;

		// Prepare User.
		$user = get_user_by( 'id', $user_id );
		if ( ! $user  || ! $user->exists() ) {
			return false;
		}
		$this->user		 = $user;
		$this->recipient = $user->user_email;

		// Prepare Wishlists.
		$this->wishlists_first	 = '';
		if ( ! empty( $wishlists ) ) {
			$wl			 = new TInvWL_Wishlist( $this->_n );
			$wishlists	 = $wl->get( array(
				'ID' => $wishlists,
			) );
			if ( ! empty( $wishlists ) ) {
				$wishlists_plain = array();
				foreach ( $wishlists as &$wishlist ) {
					$link = tinv_url_wishlist( $wishlist['share_key'] );
					if ( empty( $this->wishlists_first ) ) {
						$this->wishlists_first = $link;
					}
					$wishlists_plain[]	 = sprintf( '"%s" Link: %s', $wishlist['title'], esc_url( $link ) );
					$wishlist			 = sprintf( '<a href="%s">%s</a>', esc_url( $link ), esc_html( $wishlist['title'] ) );
				}
				$this->wishlists_plain = implode( "\n\n", $wishlists_plain );
				$this->wishlists = '<ul><li>' . implode( '</li> <li>', $wishlists ) . '</li></ul>';
			}
		}

		$result = $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
		if ( is_wp_error( $result ) ) {
			do_action( $this->_n . '_send_notification_price_error', $this->get_recipient(), $this->user, $this->product, $result->get_error_message() );
		} else {
			do_action( $this->_n . '_send_notification_price_successfully', $this->get_recipient(), $this->user, $this->product );
		}
	}

	/**
	 * Get content html function
	 *
	 * @return string
	 */
	public function get_content_html() {
		ob_start();
		tinv_wishlist_template( $this->template_html, apply_filters( 'tinvwl_notification_price_data_template_html', array(
			'email_heading'			 => $this->get_heading(),
			'blogname'				 => $this->get_blogname(),
			'product_in_wishlists'	 => $this->wishlists,
			'wishlist_with_product'	 => $this->wishlists_first,
			'product'				 => $this->product,
			'user_name'				 => $this->user->user_login,
			'user_email'			 => $this->user->user_email,
			'user_first_name'		 => $this->user->billing_first_name,
			'user_last_name'		 => $this->user->billing_first_name,
			'sent_to_admin'			 => true,
			'plain_text'			 => false,
			'email'					 => $this,
		) ) );
		return ob_get_clean();
	}

	/**
	 * Get content plain function
	 *
	 * @return string
	 */
	public function get_content_plain() {
		ob_start();
		tinv_wishlist_template( $this->template_plain, apply_filters( 'tinvwl_notification_price_data_template_plain', array(
			'email_heading'			 => $this->get_heading(),
			'blogname'				 => $this->get_blogname(),
			'product_in_wishlists'	 => $this->wishlists_plain,
			'wishlist_with_product'	 => $this->wishlists_first,
			'product'				 => $this->product,
			'user_name'				 => $this->user->user_login,
			'user_email'			 => $this->user->user_email,
			'user_first_name'		 => $this->user->billing_first_name,
			'user_last_name'		 => $this->user->billing_first_name,
			'sent_to_admin'			 => true,
			'plain_text'			 => true,
			'email'					 => $this,
		) ) );
		return ob_get_clean();
	}

	/**
	 * Initialise Settings Form Fields
	 */
	public function init_form_fields() {
		$this->form_fields = array(
			'enabled'	 => array(
				'title'		 => __( 'Enable/Disable', 'ti-woocommerce-wishlist-premium' ),
				'type'		 => 'checkbox',
				'label'		 => __( 'Enable this email notification', 'ti-woocommerce-wishlist-premium' ),
				'default'	 => 'yes',
			),
			'subject'	 => array(
				'title'			 => __( 'Email Subject', 'ti-woocommerce-wishlist-premium' ),
				'type'			 => 'text',
				'description'	 => sprintf( __( 'Defaults to <code>%s</code>', 'ti-woocommerce-wishlist-premium' ), $this->subject ),
				'default'		 => $this->subject,
				'desc_tip'		 => true,
			),
			'heading'	 => array(
				'title'			 => __( 'Email Heading', 'ti-woocommerce-wishlist-premium' ),
				'type'			 => 'text',
				'description'	 => sprintf( __( 'Defaults to <code>%s</code>', 'ti-woocommerce-wishlist-premium' ), $this->heading ),
				'default'		 => $this->heading,
				'desc_tip'		 => true,
			),
			'email_type' => array(
				'title'			 => __( 'Email type', 'ti-woocommerce-wishlist-premium' ),
				'type'			 => 'select',
				'description'	 => __( 'Choose which format of email to send.', 'ti-woocommerce-wishlist-premium' ),
				'default'		 => 'html',
				'class'			 => 'email_type wc-enhanced-select',
				'options'		 => $this->get_email_type_options(),
				'desc_tip'		 => true,
			),
		);
	}
}
