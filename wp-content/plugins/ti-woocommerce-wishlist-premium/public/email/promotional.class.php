<?php
/**
 * Wishlist Promotional create email
 *
 * @since             1.0.0
 * @package           TInvWishlist\Public
 * @subpackage		  Email
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Wishlist Promotional create email
 */
class TInvWL_Public_Email_Promotional extends TInvWL_Public_Email {

	/**
	 * Content with HTML tags
	 *
	 * @var string
	 */
	public $content;

	/**
	 * Content without HTML tags
	 *
	 * @var string
	 */
	public $content_plain;

	/**
	 * Promotional Product
	 *
	 * @var \WC_Product_Simple
	 */
	public $product;

	/**
	 * Recipient user
	 *
	 * @var \WP_User
	 */
	public $user;

	/**
	 * Wishlists in which the product is be found, with HTML tags
	 *
	 * @var string
	 */
	public $wishlists;

	/**
	 * Wishlists in which the product is be found, without HTML tags
	 *
	 * @var string
	 */
	public $wishlists_plain;

	/**
	 * Link a first wishlist
	 *
	 * @var string
	 */
	public $wishlists_first;

	/**
	 * Coupon
	 *
	 * @var \WC_Coupon
	 */
	public $coupon;

	/**
	 * Set email defaults
	 */
	function load_data() {
		$this->id = $this->_n . '_promotional_email';
		$this->title = __( 'Wishlist Promotional Email', 'ti-woocommerce-wishlist-premium' );
		$this->description = __( 'This email is sent to users to inform them about a promotion on a product that was added to their wishlist.', 'ti-woocommerce-wishlist-premium' );

		// These are the default heading and subject lines that can be overridden using the settings.
		$this->heading	 = __( 'There is a deal for you!', 'ti-woocommerce-wishlist-premium' );
		$this->subject	 = __( 'A product of your wishlist is on sale', 'ti-woocommerce-wishlist-premium' );

		// These define the locations of the templates that this email should use, we'll just use the new order template since this email is similar.
		$this->template_name = 'ti-promotional';

		// Trigger on new paid orders.
		add_action( $this->_n . '_send_promotional', array( $this, 'trigger' ), 10, 4 );

		// This sets the recipient to the settings defined below in init_form_fields().
		$this->customer_email	 = true;
		$this->manual			 = true;

		$this->wishlists		 = '';
		$this->wishlists_plain	 = '';
		$this->coupon			 = null;
	}

	/**
	 * Get list templates
	 *
	 * @return array
	 */
	public static function gettemplates() {
		return self::templates( 'ti-promotional' );
	}

	/**
	 * Run method send mail
	 *
	 * @param object  $product Promotion product.
	 * @param integer $user_id User ID Promotional.
	 * @param array   $wishlists An array of wishlists in which the product is be found.
	 * @param string  $coupon Personal coupon code.
	 * @return boolean
	 */
	function trigger( $product, $user_id, $wishlists = array(), $coupon = null ) {
		if ( empty( $product ) || empty( $user_id ) ) {
			return false;
		}

		wc()->frontend_includes();

		$this->heading	 = $this->get_option( 'heading' );
		$this->subject	 = $this->get_option( 'subject' );

		$this->content		 = apply_filters( $this->_n . '_prepare_promotional_content', $this->get_option( 'content' ) );
		$this->content_plain = apply_filters( $this->_n . '_prepare_promotional_content_plain', $this->get_option( 'content_plain' ) );

		// Prepare Product.
		$this->product = $product;

		// Prepare User.
		$user = get_user_by( 'id', $user_id );
		if ( ! $user || ! $user->exists() ) {
			return false;
		}
		$this->user		 = $user;
		$this->recipient = $user->user_email;

		// Prepare Wishlists.
		$this->wishlists_first	 = '';
		if ( ! empty( $wishlists ) ) {
			$wl			 = new TInvWL_Wishlist( $this->_n );
			$wishlists	 = $wl->get( array(
				'ID' => $wishlists,
			) );
			if ( ! empty( $wishlists ) ) {
				$wishlists_plain = array();
				foreach ( $wishlists as &$wishlist ) {
					$link = tinv_url_wishlist( $wishlist['share_key'] );
					if ( empty( $this->wishlists_first ) ) {
						$this->wishlists_first = $link;
					}
					$wishlists_plain[]	 = sprintf( '"%s" Link: %s', $wishlist['title'], esc_url( $link ) );
					$wishlist			 = sprintf( '<a href="%s">%s</a>', esc_url( $link ), esc_html( $wishlist['title'] ) );
				}
				$this->wishlists_plain = implode( "\n\n", $wishlists_plain );
				$this->wishlists = '<ul><li>' . implode( '</li> <li>', $wishlists ) . '</li></ul>';
			}
		}

		// Prepare Coupon.
		if ( ! empty( $coupon ) ) {
			$this->coupon = new WC_Coupon( $coupon );
		}
		$result = $this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
		if ( is_wp_error( $result ) ) {
			do_action( $this->_n . '_send_promotional_error', $this->get_recipient(), $this->user, $this->product, $result->get_error_message() );
		} else {
			do_action( $this->_n . '_send_promotional_successfully', $this->get_recipient(), $this->user, $this->product );
		}
	}

	/**
	 * Get_type function.
	 *
	 * @return string
	 */
	public function get_email_type() {
		$email_type = $this->email_type && class_exists( 'DOMDocument' ) ? $this->email_type : 'plain';
		return apply_filters( $this->_n . '_prepare_promotional_email_type', $email_type );
	}

	/**
	 * Get content html function
	 *
	 * @return string
	 */
	public function get_content_html() {
		ob_start();
		tinv_wishlist_template( $this->template_html, apply_filters( 'tinvwl_promotional_email_data_template_html', array(
			'email_heading'		 => $this->get_heading(),
			'blogname'			 => $this->get_blogname(),
			'content'			 => $this->get_updated_content( $this->content ),
			'sent_to_admin'		 => true,
			'plain_text'		 => false,
			'email'				 => $this,
		) ) );
		return ob_get_clean();
	}

	/**
	 * Get content plain function
	 *
	 * @return string
	 */
	public function get_content_plain() {
		ob_start();
		tinv_wishlist_template( $this->template_plain, apply_filters( 'tinvwl_promotional_email_data_template_plain', array(
			'email_heading'		 => $this->get_heading(),
			'blogname'			 => $this->get_blogname(),
			'content'			 => $this->get_updated_content( $this->content_plain, false ),
			'sent_to_admin'		 => true,
			'plain_text'		 => true,
			'email'				 => $this,
		) ) );
		return ob_get_clean();
	}

	/**
	 * Update content
	 *
	 * @param string $content Content email.
	 * @param type   $is_html HTML or Plain?.
	 */
	public function get_updated_content( $content, $is_html = true ) {
		$this->find		 = tinv_array_merge( $this->find, array(
			'user_name'			 => '{user_name}',
			'user_email'		 => '{user_email}',
			'user_first_name'	 => '{user_first_name}',
			'user_last_name'	 => '{user_last_name}',
		) );
		$this->replace	 = tinv_array_merge( $this->replace, array(
			'user_name'			 => $this->user->user_login,
			'user_email'		 => $this->user->user_email,
			'user_first_name'	 => $this->user->billing_first_name,
			'user_last_name'	 => $this->user->billing_first_name,
		) );

		$price_func		 = 'get_price' . ($is_html ? '_html' : '');
		$this->find		 = tinv_array_merge( $this->find, array(
			'product_name'			 => '{product_name}',
			'product_name_with_url'	 => '{product_name_with_url}',
			'product_price'			 => '{product_price}',
			'product_price_regular'	 => '{product_price_regular}',
			'product_price_sale'	 => '{product_price_sale}',
			'product_image'			 => '{product_image}',
			'product_image_with_url' => '{product_image_with_url}',
			'product_url'			 => '{product_url}',
		) );

		$display_regular_price		 = $display_regular_price_plain	 = ( version_compare( WC_VERSION, '3.0.0', '<' ) ? $this->product->get_display_price( $this->product->get_regular_price() ) : wc_get_price_to_display( $this->product, array( 'price' => $this->product->get_regular_price() ) ) );
		$display_sale_price			 = $display_sale_price_plain	 = ( version_compare( WC_VERSION, '3.0.0', '<' ) ? $this->product->get_display_price( $this->product->get_sale_price() ) : wc_get_price_to_display( $this->product, array( 'price' => $this->product->get_sale_price() ) ) );
		$display_regular_price		 = wc_price( $display_regular_price );
		$display_sale_price			 = wc_price( $display_sale_price );
		if ( $this->product->get_price() > 0 ) {
			if ( $this->product->is_on_sale() && $this->product->get_regular_price() ) {
				$display_regular_price	 = '<strike>' . $display_regular_price . '</strike>';
			}
		} elseif ( $this->product->get_price() == 0 ) { // WPCS: loose comparison ok.
			if ( $this->product->is_on_sale() && $this->get_regular_price() ) {
				$display_regular_price	 = '<strike>' . $display_regular_price . '</strike>';
				$display_sale_price		 = __( 'Free!', 'ti-woocommerce-wishlist-premium' );
			} else {
				$display_regular_price = '<span class="amount">' . __( 'Free!', 'ti-woocommerce-wishlist-premium' ) . '</span>';
			}
		}
		if ( $display_regular_price_plain === $display_sale_price_plain ) {
			$display_sale_price_plain = '';
		}
		if ( $display_regular_price === $display_sale_price ) {
			$display_sale_price = '';
		}
		$this->replace	 = tinv_array_merge( $this->replace, array(
			'product_name'			 => $this->product->get_title(),
			'product_name_with_url'	 => $is_html ? sprintf( '<a href="%s" style="text-decoration:none;">%s</a>', $this->product->get_permalink(), $this->product->get_title() ) : '',
			'product_price'			 => $this->product->$price_func(),
			'product_price_regular'	 => $is_html ? $display_regular_price : $display_regular_price_plain,
			'product_price_sale'	 => $is_html ? $display_sale_price : $display_sale_price_plain,
			'product_image'			 => $is_html ? apply_filters( 'tinvwl_email_wishlist_item_thumbnail', '<img src="' . ( $this->product->get_image_id() ? current( wp_get_attachment_image_src( $this->product->get_image_id(), 'thumbnail' ) ) : wc_placeholder_img_src() ) . '" alt="' . esc_attr__( 'Product image', 'woocommerce' ) . '" width="180" />' ) : '',
			'product_image_with_url' => $is_html ? sprintf( '<a href="%s" style="text-decoration:none;">%s</a>', $this->product->get_permalink(), $this->product->get_image() ) : '',
			'product_url'			 => $this->product->get_permalink(),
		) );

		$this->find		 = tinv_array_merge( $this->find, array(
			'product_in_wishlists'	 => '{product_in_wishlists}',
			'wishlist_with_product'	 => '{url_wishlist_with_product}',
		) );
		$this->replace	 = tinv_array_merge( $this->replace, array(
			'product_in_wishlists'	 => $is_html ? $this->wishlists : $this->wishlists_plain,
			'wishlist_with_product'	 => $this->wishlists_first,
		) );

		$this->find = tinv_array_merge( $this->find, array(
			'coupon_code'	 => '{coupon_code}',
			'coupon_amount'	 => '{coupon_amount}',
			'coupon_value'	 => '{coupon_value}',
		) );

		$this->replace = tinv_array_merge( $this->replace, array(
			'coupon_code'	 => empty( $this->coupon ) ? '' : $this->coupon->code,
			'coupon_amount'	 => empty( $this->coupon ) ? '' : $this->coupon->coupon_amount,
			'coupon_value'	 => empty( $this->coupon ) ? '' : $this->coupon->get_discount_amount( $this->product->get_price() ),
		) );

		return $this->format_string( stripcslashes( $content ) );
	}

	/**
	 * Save value to plugin
	 */
	function process_admin_options() {
		parent::process_admin_options();
		$option_name = str_replace( $this->_n . '_', '', $this->id );
		tinv_update_option( $option_name . '_tmp', '', array() );
	}

	/**
	 * Initialise Settings Form Fields
	 */
	public function init_form_fields() {
		$this->form_fields = array(
			'subject'	 => array(
				'title'			 => __( 'Email Subject', 'ti-woocommerce-wishlist-premium' ),
				'type'			 => 'text',
				'description'	 => sprintf( __( 'Defaults to <code>%s</code>', 'ti-woocommerce-wishlist-premium' ), $this->subject ),
				'default'		 => $this->subject,
				'desc_tip'		 => true,
			),
			'heading'	 => array(
				'title'			 => __( 'Email Heading', 'ti-woocommerce-wishlist-premium' ),
				'type'			 => 'text',
				'description'	 => sprintf( __( 'Defaults to <code>%s</code>', 'ti-woocommerce-wishlist-premium' ), $this->heading ),
				'default'		 => $this->heading,
				'desc_tip'		 => true,
			),
			'content'	 => array(
				'title'			 => __( 'Email Content', 'ti-woocommerce-wishlist-premium' ),
				'type'			 => 'textarea',
				'description'	 => __( 'This field lets you modify the main content of the HTML email. You can use the following placeholder: <code>{user_name}</code>, <code>{user_email}</code>, <code>{user_first_name}</code>, <code>{user_last_name}</code>, <code>{product_image}</code>, <code>{product_image_with_url}</code>, <code>{product_in_wishlists}</code>, <code>{product_name}</code>, <code>{product_name_with_url}</code>, <code>{product_price}</code>, <code>{product_price_regular}</code>, <code>{product_price_sale}</code>, <code>{product_url}</code>, <code>{coupon_code}</code>, <code>{coupon_amount}</code>, <code>{url_wishlist_with_product}</code>.', 'ti-woocommerce-wishlist-premium' ),
				'default'		 => '<p>Hi {user_name}</p>
<p>A product of your wishlist is on sale!</p>
<p>{product_in_wishlists}</p>
<p>
    <table>
        <tr>
            <td>{product_image}</td>
            <td>{product_name}</td>
            <td>{product_price}</td>
        </tr>
    </table>
</p>
<p>Use this coupon code <b><a href="{wishlist_with_product}" >{coupon_code}</a></b> to obtain a wonderful discount!</p>',
				'css'			 => 'width:80%;height:250px;resize:vertical;',
			),
			'content_plain'	 => array(
				'title'			 => __( 'Email Content Plain', 'ti-woocommerce-wishlist-premium' ),
				'type'			 => 'textarea',
				'description'	 => __( 'This field lets you modify the main content of the HTML email. You can use the following placeholder: <code>{user_name}</code>, <code>{user_email}</code>, <code>{user_first_name}</code>, <code>{user_last_name}</code>, <code>{product_image}</code>, <code>{product_in_wishlists}</code>, <code>{product_name}</code>, <code>{product_price}</code>, <code>{product_price_regular}</code>, <code>{product_price_sale}</code>, <code>{product_url}</code>, <code>{coupon_code}</code>, <code>{coupon_amount}</code>, <code>{url_wishlist_with_product}</code>.', 'ti-woocommerce-wishlist-premium' ),
				'default'		 => 'Hi {user_name}
A product of your wishlist is on sale!

{product_name} {product_price}

Visit {wishlist_with_product} and use this coupon code
{coupon_code}
to obtain a wonderful discount!',
				'css'			 => 'width:80%;height:250px;resize:vertical;',
			),
			'email_type' => array(
				'title'			 => __( 'Email type', 'ti-woocommerce-wishlist-premium' ),
				'type'			 => 'select',
				'description'	 => __( 'Choose which format of email to send.', 'ti-woocommerce-wishlist-premium' ),
				'default'		 => 'html',
				'class'			 => 'email_type wc-enhanced-select',
				'options'		 => $this->get_email_type_options(),
				'desc_tip'		 => true,
			),
		);
	}
}
