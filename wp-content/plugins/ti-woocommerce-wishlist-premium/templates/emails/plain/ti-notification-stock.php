<?php
/**
 * The Template for displaying notification email for change stock status a product plain this plugin.
 *
 * @version             1.0.0
 * @package           TInvWishlist\Admin\Template
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

echo '= ' . $email_heading . " =\n\n"; // WPCS: xss ok.
echo "Hi $user_name\n"; // WPCS: xss ok.
echo "A product of your wishlist is in stock!\n\n";
echo "{$product->get_title()} {$this->product->get_price()} \n\n"; // WPCS: xss ok.
echo "Visit to:\n";
echo "$product_in_wishlists\n\n"; // WPCS: xss ok.
echo apply_filters( 'woocommerce_email_footer_text', get_option( 'woocommerce_email_footer_text' ) ); // WPCS: xss ok.
