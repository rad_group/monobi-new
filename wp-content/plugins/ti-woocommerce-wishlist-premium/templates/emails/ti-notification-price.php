<?php
/**
 * The Template for displaying notification email for change price a product this plugin.
 *
 * @version             1.0.0
 * @package           TInvWishlist\Admin\Template
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Output the email header
 *
 * @hooked WC_Emails::email_header()
 */
do_action( 'woocommerce_email_header', $email_heading, $email );
?>
<p>Hi <?php echo esc_html( $user_name ); ?></p>
<p>A product of your wishlist is on sale!</p>
<p><?php echo $product_in_wishlists; // WPCS: xss ok. ?></p>
<p>
	<table>
		<tr>
			<td><?php echo '<img src="' . ( $product->get_image_id() ? current( wp_get_attachment_image_src( $product->get_image_id(), 'thumbnail' ) ) : wc_placeholder_img_src() ) . '" alt="' . esc_attr__( 'Product image', 'woocommerce' ) . '" width="180" />'; // WPCS: xss ok. ?></td>
			<td><?php echo $product->get_title(); // WPCS: xss ok. ?></td>
			<td><?php echo $product->get_price_html(); // WPCS: xss ok. ?></td>
		</tr>
	</table>
</p>
<?php
/**
 * Output the email footer
 *
 * @hooked WC_Emails::email_footer()
 */
do_action( 'woocommerce_email_footer', $email );
