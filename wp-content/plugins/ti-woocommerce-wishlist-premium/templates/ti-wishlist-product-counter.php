<?php
/**
 * The Template for displaying dropdown wishlist products in topline.
 *
 * @version             1.4.0
 * @package           TInvWishlist\Template
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( $icon_class && 'custom' === $icon && ! empty( $icon_upload ) ) {
	$text = sprintf( '<img src="%s" /> %s', esc_url( $icon_upload ), $text );
}
?>

<div class="wishlist_products_counter<?php echo ' ' . $icon_class . ' ' . $icon_style . ( empty( $text ) ? ' no-txt' : '' ); // WPCS: xss ok.     ?>">
	<?php if ( $use_link ) : ?>
		<a href="<?php echo esc_url( $link ); ?>">
		<?php endif; ?>
			<i class="wishlist-icon"></i>
		<?php if ( $text ) : ?>
			<span class="wishlist_products_counter_text"><?php echo $text; // WPCS: xss ok. ?></span>
		<?php endif; ?>
		<?php if ( $show_counter ) : ?>
			<span class="wishlist_products_counter_number"></span>
		<?php endif; ?>
		<?php if ( $use_link ) : ?>
		</a>
	<?php endif; ?>
	<?php if ( $drop_down ) : ?>
		<div class="wishlist_products_counter_wishlist widget_wishlist" style="display:none; opacity: 0;">
			<div class="widget_wishlist_content">
				<div class="tinv_mini_wishlist_list"></div>
			</div>
		</div>
	<?php endif; ?>
</div>
