<?php
/**
 * The Template for displaying dialog for estimate wishlist this plugin.
 *
 * @version             1.0.0
 * @package           TInvWishlist\Admin\Template
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="tinvwl-estimate-wrap">
	<?php if ( $estimate_note ) : ?>
	<a href="javascript:void(0)" class="button tinv-modal-btn"><i class="fa fa-envelope-o"></i><?php esc_html_e( 'Ask For An Estimate', 'ti-woocommerce-wishlist-premium' ); ?></a>
		<div class="estimate-dialogbox tinv-modal">
			<div class="tinv-overlay"></div>
			<div class="tinv-table">
				<div class="tinv-cell">
					<div class="tinv-modal-inner">
						<a class="tinv-close-modal" href="javascript:void(0)"><i class="fa fa-times"></i></a>
						<h2><?php echo esc_html( $estimate_note_text ); ?></h2>
						<form method="POST">
							<textarea placeholder="<?php esc_html_e( 'Additional notes...', 'ti-woocommerce-wishlist-premium' ) ?>" name="estimate_note"></textarea>
							<button type="submit" class="" name="tinvwl_estimate" value="<?php echo esc_attr( $wishlist_id ); ?>"><?php esc_html_e( 'Send a Request', 'ti-woocommerce-wishlist-premium' ) ?></button>
							<?php wp_nonce_field( 'tinvwl_check_estimate_' . $wishlist_id, 'tinvwl_estimate_nonce' ); ?>
						</form>
					</div>
				</div>
			</div>
		</div>
	<?php else : ?>
		<form method="POST">
			<button type="submit" class="button tinv-modal-btn" name="tinvwl_estimate" value="<?php echo esc_attr( $wishlist_id ); ?>"><i class="fa fa-envelope-o"></i><?php esc_html_e( 'Ask for an Estimate', 'ti-woocommerce-wishlist-premium' ); ?></button>
			<?php wp_nonce_field( 'tinvwl_check_estimate_' . $wishlist_id, 'tinvwl_estimate_nonce' ); ?>
		</form>
	<?php endif; ?>
</div>
