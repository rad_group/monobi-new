<?php
/**
 * The Template for displaying dialog for rename wishlist this plugin.
 *
 * @version             1.0.0
 * @package           TInvWishlist\Admin\Template
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<span class="tinvwl-name-to-rename"><?php echo $wishlist_name; // WPCS: xss ok. ?></span>
<span class="tinvwl-rename-input" style="display:none;">
	<?php echo TInvWL_Form::_text( sprintf( 'wishlist_name[%d]', $wishlist_id ), $wishlist_value, array( 'class' => '' ) ); // WPCS: xss ok. ?>
</span>
<a href="javascript:void(0);" onclick="jQuery( this ).hide().prevAll( 'span' ).each( function () {
		jQuery( this ).toggle( jQuery( this ).is( ':hidden' ) ).find('input').focus();
	} );" class="tinvwl-rename-button"><i class="fa fa-pencil"></i><span><?php esc_html_e( 'Rename', 'ti-woocommerce-wishlist-premium' ) ?></span></a>
