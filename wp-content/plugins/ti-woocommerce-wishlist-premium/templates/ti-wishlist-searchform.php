<?php
/**
 * The Template for displaying empty search wishlists.
 *
 * @version             1.0.0
 * @package           TInvWishlist\Template
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="tinv-wishlist">
	<div class="tinv-search-list">
		<form role="search" method="get" class="tinv-search-form tinv-wrapped-block" action="<?php echo esc_url( get_permalink( tinv_get_option( 'page', 'search' ) ) ); ?>">
			<div class="input-group">
				<input type="search" class="tinv-search-field form-control" placeholder="<?php echo esc_attr( $placeholder_input_text, 'ti-woocommerce-wishlist-premium' ) ?>" value="<?php echo esc_attr( $search ) ?>" name="tiws" />
				<span class="input-group-btn">
					<button type="submit" class="tinv-search-submit"><?php echo esc_html( $button_text ) ?></button>
				</span>
			</div>
		</form>
	</div>
</div>
