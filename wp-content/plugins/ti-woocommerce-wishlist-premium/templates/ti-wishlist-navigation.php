<?php
/**
 * The Template for displaying wishlist navigation this plugin.
 *
 * @version             1.0.0
 * @package           TInvWishlist\Admin\Template
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="tinv-wishlist-clear"></div>
<ul class="navigation-button tinv-wishlist-clear">
	<?php if ( is_user_logged_in() && tinv_get_option( 'general', 'multi' ) && tinv_get_option( 'page', 'manage' ) ) { ?>
	<li><?php echo do_shortcode( '[ti_wishlists_create show="button"]' ); ?></li>
	<li><a href="<?php echo esc_url( get_permalink( apply_filters( 'wpml_object_id', tinv_get_option( 'page', 'manage' ), 'page', true ) ) ); ?>" class="<?php echo esc_attr( ( 'button' === $button_type )?'button':'' )?> <?php echo esc_attr( $button_icon ? '' : 'tinwl-no-icon' ); ?>"><?php if ( $button_icon ) { ?><i class="fa fa-wrench"></i><?php } ?><span class="tinvwl-txt"><?php esc_html_e( 'My Wishlists', 'ti-woocommerce-wishlist-premium' ) ?></span></a></li>
	<?php } ?>
	<?php if ( tinv_get_option( 'navigation', 'public' ) && tinv_get_option( 'page', 'public' ) ) { ?>
	<li><a href="<?php echo esc_url( get_permalink( apply_filters( 'wpml_object_id', tinv_get_option( 'page', 'public' ), 'page', true ) ) ); ?>" class="<?php echo esc_attr( ( 'button' === $button_type )?'button':'' )?> <?php echo esc_attr( $button_icon ? '' : 'tinwl-no-icon' ); ?>"><?php if ( $button_icon ) { ?><i class="fa fa-bars"></i><?php } ?><span class="tinvwl-txt"><?php esc_html_e( 'All Wishlists', 'ti-woocommerce-wishlist-premium' ) ?></span></a></li>
	<?php } ?>
	<?php if ( tinv_get_option( 'navigation', 'searchp' ) && tinv_get_option( 'page', 'searchp' ) ) { ?>
	<li><a href="<?php echo esc_url( get_permalink( apply_filters( 'wpml_object_id', tinv_get_option( 'page', 'searchp' ), 'page', true ) ) ); ?>" class="<?php echo esc_attr( ( 'button' === $button_type )?'button':'' )?> <?php echo esc_attr( $button_icon ? '' : 'tinwl-no-icon' ); ?>"><?php if ( $button_icon ) { ?><i class="fa fa-search"></i><?php } ?><span class="tinvwl-txt"><?php esc_html_e( 'Search', 'ti-woocommerce-wishlist-premium' ) ?></span></a></li>
	<?php } ?>
	<li><a href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>" class="<?php echo esc_attr( ( 'button' === $button_type ) ? 'button' : '' ); ?> <?php echo esc_attr( $button_icon ? '' : 'tinwl-no-icon' ); ?>"><?php if ( $button_icon ) { ?><i class="fa fa-chevron-left"></i><?php } ?><span class="tinvwl-txt"><?php esc_html_e( 'Continue Shopping', 'ti-woocommerce-wishlist-premium' ) ?></span></a></li>
</ul>
