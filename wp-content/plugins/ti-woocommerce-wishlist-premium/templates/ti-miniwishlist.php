<?php
/**
 * The Template for displaying mini wishlist products.
 *
 * @version             1.4.0
 * @package           TInvWishlist\Template
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="tinv_mini_wishlist_list">
	<?php do_action( 'tinvwl_before_mini_wishlist' ); ?>
	<ul class="product_list_widget <?php echo esc_attr( $args['list_class'] ); ?>">
		<?php
		if ( ! empty( $products ) ) :
			do_action( 'tinvwl_before_mini_wishlist_contents' );
			foreach ( $products as $wl_product ) {
				$product = apply_filters( 'tinvwl_mini_wishlist_item', $wl_product['data'] );
				unset( $wl_product['data'] );
				if ( $wl_product['quantity'] > 0 && apply_filters( 'tinvwl_mini_wishlist_item_visible', true, $wl_product, $product ) ) {
					$product_url	 = apply_filters( 'tinvwl_mini_wishlist_item_url', $product->get_permalink(), $wl_product, $product );
					$product_name	 = apply_filters( 'tinvwl_mini_wishlist_item_name', $product->get_title(), $wl_product, $product );
					$thumbnail		 = apply_filters( 'tinvwl_mini_wishlist_item_thumbnail', $product->get_image(), $wl_product, $product );
					$product_meta	 = apply_filters( 'tinvwl_mini_wishlist_item_meta_data', tinv_wishlist_get_item_data( $product ), $wl_product, $product );
					$product_price	 = apply_filters( 'tinvwl_mini_wishlist_item_price', $product->get_price_html(), $wl_product, $product );
					$wishlists		 = apply_filters( 'tinvwl_mini_wishlist_item_wishlist', $wl_product['wishlists'], $wl_product, $product );
					?><li class="<?php echo esc_attr( apply_filters( 'tinvwl_mini_wishlist_item_class', 'mini_wishlist_item', $wl_product, $product ) ); ?>">
						<?php if ( $args['show_wishlist'] && 'before' === $args['positios_wishlist'] ) { ?>
							<ul class="wishlist_list_titles">
								<?php
								foreach ( $wishlists as $key => $wishlist ) {
									$wishlists[ $key ] = apply_filters( 'tinvwl_mini_wishlist_item_wishlist', sprintf( '<li><a href="%s" class="wishlist">%s</a></li>', esc_url( $wishlist['url'] ), esc_html( $wishlist['title'] ) ), $wl_product, $product, $wishlist ); // WPCS: xss ok.
								}
								echo implode( ' ', $wishlists ); // WPCS: xss ok.
								if ( 3 < count( $wishlists ) ) {
									echo sprintf( ' <li class="wishlist_title_more"><a href="#" class="wishlist_title_more">%s</a></li>', esc_html__( 'more...', 'ti-woocommerce-wishlist-premium' ) ); // WPCS: xss ok.
								}
								?>
							</ul>
						<?php } ?>
						<?php
						if ( ! $product->is_visible() ) {
							echo $thumbnail; // WPCS: xss ok.
							echo $product_name; // WPCS: xss ok.
							echo '&nbsp;';
						} else {
							?>
							<a href="<?php echo esc_url( $product_url ); ?>">
								<?php
								echo $thumbnail; // WPCS: xss ok.
								echo $product_name; // WPCS: xss ok.
								echo '&nbsp;';
								?>
							</a>
							<?php
						}
						echo $product_meta; // WPCS: xss ok.
						echo apply_filters( 'tinvwl_mini_wishlist_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $wl_product['quantity'], $product_price ) . '</span>', $wl_product, $product ); // WPCS: xss ok.
						if ( $args['show_wishlist'] && 'after' === $args['positios_wishlist'] ) {
							?>
							<ul class="wishlist_list_titles">
								<?php
								foreach ( $wishlists as $key => $wishlist ) {
									$wishlists[ $key ] = apply_filters( 'tinvwl_mini_wishlist_item_wishlist', sprintf( '<li><a href="%s" class="wishlist">%s</a></li>', esc_url( $wishlist['url'] ), esc_html( $wishlist['title'] ) ), $wl_product, $product, $wishlist ); // WPCS: xss ok.
								}
								echo implode( ' ', $wishlists ); // WPCS: xss ok.
								if ( 3 < count( $wishlists ) ) {
									echo sprintf( ' <li class="wishlist_title_more"><a href="#" class="wishlist_title_more">%s</a></li>', esc_html__( 'more...', 'ti-woocommerce-wishlist-premium' ) ); // WPCS: xss ok.
								}
								?>
							</ul>
					<?php } ?>
					</li><?php
				} // End if().
			} // End foreach().
			do_action( 'tinvwl_after_mini_wishlist_contents' );
		else :
			?>
			<li class="empty"><?php esc_html_e( 'No products in the wishlist.', 'ti-woocommerce-wishlist-premium' ); ?></li>
	<?php endif; ?>
	</ul>

	<?php if ( ! empty( $products ) ) : ?>
		<p class="total"><strong><?php esc_html_e( 'Total products', 'ti-woocommerce-wishlist-premium' ); ?>:</strong> <?php echo $subtotal_count; // WPCS: xss ok. ?></p>
<?php endif; ?>

<?php do_action( 'tinvwl_after_mini_wishlist', $products ); ?>
</div>
