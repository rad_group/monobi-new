// Document Ready
$( document ).ready(function() {

  $('.secondary-navigation__list-item').hover(function(){
    $('.secondary-navigation__list-item').removeClass('show-box');
    $(this).addClass('show-box');
  })

  $('.secondary-navigation__list-item').mouseleave(function(){
    $(this).removeClass('show-box');
  })

}); //END Document Ready
