// Document Ready
$( document ).ready(function() {

  menu_toggle('.js--toggle-menu', '.js--mobile-menu');

}); //END Document Ready

function menu_toggle($click, $target) {
  $($click).click(function() {

    $(this).find('.hamburger__elements').toggleClass('is-active');
    $($target).toggleClass('opened');

    $('body').toggleClass('menu-opened');

  });
}
