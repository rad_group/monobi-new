// Document Ready
$( document ).ready(function() {

  $('.hero-slider').slick({
    dots: true,
    arrows: false,
    infinite: true,
    autoplay: false,
    autoplaySpeed: 3000,
    speed: 300,
    slidesToShow: 1,
    customPaging: function() {
      return $('');
    }
  });

}); //END Document Ready
