;(function ($) {

  // Document Ready
  $( document ).ready(function() {

    play_video();

  }); //END Document Ready


  function play_video() {
    if ( $('.js--video-ui').length ) {
      $('.js--video-ui').click(function () {
        var element = $(this).children(".video-ui__video").get(0);
        if( element.paused ){
          if (element.requestFullscreen) {
            element.requestFullscreen();
          } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen(); // Firefox
          } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen(); // Chrome and Safari
          }
          $('.video-ui__video').attr('controls','');

          $(this).children(".video-ui__video").removeClass('paused');

          element.play();
          //$(this).children(".video-ui__playpause").html('<i class="iconae regular ae--pause-circle"></i>');
          if( check_mobile() === false ) {
            $('body').addClass('video-fullscreen-playing');
          }
        } else {
          if( check_mobile() === false ) {
            $('body').removeClass('video-fullscreen-playing');
          }
          $('.video-ui__video').removeAttr('controls');

          $(this).children(".video-ui__video").addClass('paused');

          element.pause();
          //$(this).children(".video-ui__playpause").html('<i class="iconae regular ae--play-circle"></i>');
        }
      });
      $(document).bind('webkitfullscreenchange mozfullscreenchange fullscreenchange', function(e) {
        var state = document.fullScreen || document.mozFullScreenElement || document.webkitIsFullScreen;
        var event = state ? 'FullscreenOn' : 'FullscreenOff';

        if ( event == 'FullscreenOff' ) {
          $('.video-ui__video').each(function(){
            if( check_mobile() === false ) {
              $('body').removeClass('video-fullscreen-playing');
            }
            $(this).get(0).pause();
            //$(".video-ui__playpause").html('<i class="iconae regular ae--play-circle"></i>');
            $('.video-ui__video').removeAttr('controls');
          })
        }

      });
    }
  }

  function check_mobile() {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $(window).width() < 640 ) {
      return true;
    }
    else {
      return false;
    }
  }

}(jQuery));
