// Document Ready
$( document ).ready(function() {

  $('.first-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    prevArrow: '<i class="iconae light ae--arrow-left left"></i>',
    nextArrow: '<i class="iconae light ae--arrow-right right"></i>',
    infinite: true,
    swipe: false,
    accessibility: true,
    responsive: [{
      breakpoint: 640,
      settings: {
        swipe: true,
        dots: true,
        arrows: false,
      },
      breakpoint: 1024,
      settings: {
        swipe: true,
        dots: true,
        arrows: false,
      }
    }],
    customPaging: function() {
      return $('');
    }
    //asNavFor: '.second-slider'
  });

  $('.second-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.first-slider',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true,
    infinite: true,
    vertical: true,
  });

  $('.third-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.first-slider',
    dots: true,
    arrow: true,
    prevArrow: '<i class="iconae light ae--arrow-left left"></i>',
    nextArrow: '<i class="iconae light ae--arrow-right right"></i>',
    centerMode: false,
    focusOnSelect: true,
    infinite: true,
    swipe: false,
    responsive: [{
      breakpoint: 640,
      settings: {
        swipe: true,
        arrows: false,
      }
    }],
    customPaging: function() {
      return $('');
    }
  });

  $slides = $('.second-slider img').length;

  if( $('.second-slider img').length >= 1 && $('.second-slider img').length <= 6 ){
    $('.second-slider').slick('slickSetOption', 'slidesToShow', $slides, true);
  };

  $('.first-slider').on('afterChange', function(event, slick, currentSlide){
    $current = $(this).find('.slick-current').data('slick-index');
    $('.second-slider').slick('slickGoTo', $current, true);
    $('.third-slider').slick('slickGoTo', $current, true);
  });

  $('.third-slider .slick-list').click(function(){
    $('.overlay-slider').removeClass('active');
    $('body').removeClass('popup-opened');
  });


  $('.ivpa_term').click(function(){
    $color = $(this).data('term');
    // console.log($color);
    $('.slick-slide').removeClass('slick-current');
    $('.first-slider').find('.at-color_' + $color).addClass('slick-current');

    $get_index = $('.first-slider').find('.at-color_' + $color).data('slick-index');

    $('.first-slider').slick('slickGoTo', $get_index);

  });


}); //END Document Ready
