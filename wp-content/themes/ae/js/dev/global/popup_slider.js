// Document Ready
$( document ).ready(function() {

  open_popup_slider();

}); //END Document Ready

function open_popup_slider() {
  var $btn = $('.first-slider .js--popup-action');

  $btn.click(function(){
    //Get the container
    var target = $(this).data('slide');

    $('#' + target).addClass('active');

    $('body').addClass('popup-opened');
  })
}
