// Document Ready
$( document ).ready(function() {

  get_parent_h('.js--parent-focus-h', '.js--child-focus-h');
  get_parent_h('.js--parent-intagram-h', '.js--child-intagram-h');
  open_set_country('#countrySelector');
  instagram_size();

}); //END Document Ready

$( window ).resize(function() {

  get_parent_h('.js--parent-focus-h', '.js--child-focus-h');
  get_parent_h('.js--parent-intagram-h', '.js--child-intagram-h');
  instagram_size();

});  //END Window Resize

function get_parent_h($parent, $child) {
  if ( $('.js--parent-h').length ) {
    $($parent).each(function() {
      var parent_h = $($parent).outerHeight();
      console.log(parent_h);
      $($child).css('min-height', parent_h);
    });
  }
}

function open_set_country(target){
  if($.cookie('setCountry') != 1){
    $(target).addClass('active');
    $('body').addClass('popup-opened');
    $(target).find('.js--close-popup').addClass('popup-hide-close');
  }
}

function instagram_size() {
  $('.instagram__overlay').each(function(){
    var insta_width = $(this).width();
    $(this).css('height', insta_width);
  });
}
