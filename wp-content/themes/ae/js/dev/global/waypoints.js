// Document Ready
$( document ).ready(function() {

  var waypoints = $('#overlap').waypoint(function(direction) {
    $('.overlap__wrap').toggleClass('in-view');
  }, {
    offset: '90%'
  })

}); //END Document Ready
