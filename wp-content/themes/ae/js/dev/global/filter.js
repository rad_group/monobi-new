// Document Ready
$( document ).ready(function() {

  $('.js--filter').click(function(){

    $('.js--filter').removeClass('current');
    $(this).addClass('current');
    $data = $(this).data('filter');

    if($data == '*'){
      $('.js--filter_article').removeClass('hidden');
    }else{
      $('.js--filter_article').addClass('hidden');
      $('.js--filter_'+$data).removeClass('hidden');
    }
  });


}); //END Document Ready

// js--filter
// js--article
