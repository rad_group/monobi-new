$(document).ready(function(){
  open_tooltip();
});

function open_tooltip() {
  var $tooltip_trigger = $('.js--tooltip-trigger');

  $tooltip_trigger.click(function() {
    var target = $(this).data( "tooltip" );

    $('#' + target).toggleClass('active');

    event.stopPropagation();
  });

  $('.site-content').click(function() {

    $('.js--tooltip-target').removeClass('active');

  });
};
