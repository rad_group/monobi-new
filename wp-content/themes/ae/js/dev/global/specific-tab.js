// Document Ready
$( document ).ready(function() {
  if ( $('.specific').length ) {
    $('.specific__list-item').first().addClass('current-tab');
    $('.specific__feature').addClass('shown-tab');

    init_parent_height = $('.specific').outerHeight();
    init_child_height = $('.specific__panel.shown-tab').outerHeight();

    $('.specific').css('height', init_parent_height + init_child_height).fadeIn();
    $('.specific__panel').fadeIn();

    $('.specific').find('.specific__list-item').click(function(e){

      e.preventDefault();
      var tab_trigger_data = $(this).data('specific');

      $('.specific__list-item').removeClass('current-tab');
      $(this).addClass('current-tab');

      $('.specific__panel').removeClass('shown-tab');
      $('.specific__' + tab_trigger_data).addClass('shown-tab');

      $('.specific').css('height', '0');
      list_height = $('.specific__list').outerHeight();

      panel_height = $('.specific__' + tab_trigger_data).outerHeight();
      parent_height = list_height + panel_height + 30;


      $('.specific').css('height', parent_height);
    });

  };
}); //END Document Ready
