// Document Ready
$( document ).ready(function() {

  $('.js--search').click(function(){
    $('.search__bar').toggleClass('in-use');

  });

  $('.site-content').click(function(){
    $('.search__bar').removeClass('in-use');
  });

}); //END Document Ready
