$(document).ready(function(){
  size_guide_append();
});

function size_guide_append() {

  $('.ivpa_attribute').each(function(){
    var size_data = $(this).data('attribute');
    if ( size_data == 'pa_size') {
      $(this).find('.ivpa_title').append('<span data-popup="sizing" class="js--popup-action size-guide-label">(Charts)</span>');
      open_popup();
    }
  });

}
