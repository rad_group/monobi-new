<?php
/**
 * Country selector form
 *
 * @author 		oscargare
 * @version     1.6.13
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/*
 * Map a two-letter continent code onto the name of the continent.
 */

 function initCountriesSelector(){
 	$CONTINENTS = array(
 		"AS" => array("title" => "Asia", "countries" => array()),
 		"AN" => array("title" => "Antarctica", "countries" => array()),
 		"AF" => array("title" => "Africa", "countries" => array()),
 		"SA" => array("title" => "South America", "countries" => array()),
 		"EU" => array("title" => "Europe", "countries" => array()),
 		"OC" => array("title" => "Oceania", "countries" => array()),
 		"NA" => array("title" => "North America", "countries" => array())
 	);

 	return $CONTINENTS;
 }

 /*
  * Map a two-letter country code onto the country's two-letter continent code.
  */
 function getContinentSelector($iso){
 	$COUNTRY_CONTINENTS = array(
 		"AF" => "AS",
 		"AX" => "EU",
 		"AL" => "EU",
 		"DZ" => "AF",
 		"AS" => "OC",
 		"AD" => "EU",
 		"AO" => "AF",
 		"AI" => "NA",
 		"AQ" => "AN",
 		"AG" => "NA",
 		"AR" => "SA",
 		"AM" => "AS",
 		"AW" => "NA",
 		"AU" => "OC",
 		"AT" => "EU",
 		"AZ" => "AS",
 		"BS" => "NA",
 		"BH" => "AS",
 		"BD" => "AS",
 		"BB" => "NA",
 		"BY" => "EU",
 		"BE" => "EU",
 		"BZ" => "NA",
 		"BJ" => "AF",
 		"BM" => "NA",
 		"BT" => "AS",
 		"BO" => "SA",
 		"BA" => "EU",
 		"BW" => "AF",
 		"BV" => "AN",
 		"BR" => "SA",
 		"IO" => "AS",
 		"BN" => "AS",
 		"BG" => "EU",
 		"BF" => "AF",
 		"BI" => "AF",
 		"KH" => "AS",
 		"CM" => "AF",
 		"CA" => "NA",
 		"CV" => "AF",
 		"KY" => "NA",
 		"CF" => "AF",
 		"TD" => "AF",
 		"CL" => "SA",
 		"CN" => "AS",
 		"CX" => "AS",
 		"CC" => "AS",
 		"CO" => "SA",
 		"KM" => "AF",
 		"CD" => "AF",
 		"CG" => "AF",
 		"CK" => "OC",
 		"CR" => "NA",
 		"CI" => "AF",
 		"HR" => "EU",
 		"CU" => "NA",
 		"CY" => "AS",
 		"CZ" => "EU",
 		"DK" => "EU",
 		"DJ" => "AF",
 		"DM" => "NA",
 		"DO" => "NA",
 		"EC" => "SA",
 		"EG" => "AF",
 		"SV" => "NA",
 		"GQ" => "AF",
 		"ER" => "AF",
 		"EE" => "EU",
 		"ET" => "AF",
 		"FO" => "EU",
 		"FK" => "SA",
 		"FJ" => "OC",
 		"FI" => "EU",
 		"FR" => "EU",
 		"GF" => "SA",
 		"PF" => "OC",
 		"TF" => "AN",
 		"GA" => "AF",
 		"GM" => "AF",
 		"GE" => "AS",
 		"DE" => "EU",
 		"GH" => "AF",
 		"GI" => "EU",
 		"GR" => "EU",
 		"GL" => "NA",
 		"GD" => "NA",
 		"GP" => "NA",
 		"GU" => "OC",
 		"GT" => "NA",
 		"GG" => "EU",
 		"GN" => "AF",
 		"GW" => "AF",
 		"GY" => "SA",
 		"HT" => "NA",
 		"HM" => "AN",
 		"VA" => "EU",
 		"HN" => "NA",
 		"HK" => "AS",
 		"HU" => "EU",
 		"IS" => "EU",
 		"IN" => "AS",
 		"ID" => "AS",
 		"IR" => "AS",
 		"IQ" => "AS",
 		"IE" => "EU",
 		"IM" => "EU",
 		"IL" => "AS",
 		"IT" => "EU",
 		"JM" => "NA",
 		"JP" => "AS",
 		"JE" => "EU",
 		"JO" => "AS",
 		"KZ" => "AS",
 		"KE" => "AF",
 		"KI" => "OC",
 		"KP" => "AS",
 		"KR" => "AS",
 		"KW" => "AS",
 		"KG" => "AS",
 		"LA" => "AS",
 		"LV" => "EU",
 		"LB" => "AS",
 		"LS" => "AF",
 		"LR" => "AF",
 		"LY" => "AF",
 		"LI" => "EU",
 		"LT" => "EU",
 		"LU" => "EU",
 		"MO" => "AS",
 		"MK" => "EU",
 		"MG" => "AF",
 		"MW" => "AF",
 		"MY" => "AS",
 		"MV" => "AS",
 		"ML" => "AF",
 		"MT" => "EU",
 		"MH" => "OC",
 		"MQ" => "NA",
 		"MR" => "AF",
 		"MU" => "AF",
 		"YT" => "AF",
 		"MX" => "NA",
 		"FM" => "OC",
 		"MD" => "EU",
 		"MC" => "EU",
 		"MN" => "AS",
 		"ME" => "EU",
 		"MS" => "NA",
 		"MA" => "AF",
 		"MZ" => "AF",
 		"MM" => "AS",
 		"NA" => "AF",
 		"NR" => "OC",
 		"NP" => "AS",
 		"AN" => "NA",
 		"NL" => "EU",
 		"NC" => "OC",
 		"NZ" => "OC",
 		"NI" => "NA",
 		"NE" => "AF",
 		"NG" => "AF",
 		"NU" => "OC",
 		"NF" => "OC",
 		"MP" => "OC",
 		"NO" => "EU",
 		"OM" => "AS",
 		"PK" => "AS",
 		"PW" => "OC",
 		"PS" => "AS",
 		"PA" => "NA",
 		"PG" => "OC",
 		"PY" => "SA",
 		"PE" => "SA",
 		"PH" => "AS",
 		"PN" => "OC",
 		"PL" => "EU",
 		"PT" => "EU",
 		"PR" => "NA",
 		"QA" => "AS",
 		"RE" => "AF",
 		"RO" => "EU",
 		"RU" => "EU",
 		"RW" => "AF",
 		"SH" => "AF",
 		"KN" => "NA",
 		"LC" => "NA",
 		"PM" => "NA",
 		"VC" => "NA",
 		"WS" => "OC",
 		"SM" => "EU",
 		"ST" => "AF",
 		"SA" => "AS",
 		"SN" => "AF",
 		"RS" => "EU",
 		"SC" => "AF",
 		"SL" => "AF",
 		"SG" => "AS",
 		"SK" => "EU",
 		"SI" => "EU",
 		"SB" => "OC",
 		"SO" => "AF",
 		"ZA" => "AF",
 		"GS" => "AN",
 		"ES" => "EU",
 		"LK" => "AS",
 		"SD" => "AF",
 		"SR" => "SA",
 		"SJ" => "EU",
 		"SZ" => "AF",
 		"SE" => "EU",
 		"CH" => "EU",
 		"SY" => "AS",
 		"TW" => "AS",
 		"TJ" => "AS",
 		"TZ" => "AF",
 		"TH" => "AS",
 		"TL" => "AS",
 		"TG" => "AF",
 		"TK" => "OC",
 		"TO" => "OC",
 		"TT" => "NA",
 		"TN" => "AF",
 		"TR" => "AS",
 		"TM" => "AS",
 		"TC" => "NA",
 		"TV" => "OC",
 		"UG" => "AF",
 		"UA" => "EU",
 		"AE" => "AS",
 		"GB" => "EU",
 		"UM" => "OC",
 		"US" => "NA",
 		"UY" => "SA",
 		"UZ" => "AS",
 		"VU" => "OC",
 		"VE" => "SA",
 		"VN" => "AS",
 		"VG" => "NA",
 		"VI" => "NA",
 		"WF" => "OC",
 		"EH" => "AF",
 		"YE" => "AS",
 		"ZM" => "AF",
 		"ZW" => "AF"
 	);

 	return $COUNTRY_CONTINENTS[$iso];
 }

 $country_selector = initCountriesSelector();
 $country_selectable = array('IT','CN','CA','US','JP');


 foreach ($countries as $key => $value) :
 	$continent = getContinentSelector($key);

 	$country_selector[$continent]['countries'][$key] = $value;
 endforeach;


 function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
     $output = NULL;
     if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
         $ip = $_SERVER["REMOTE_ADDR"];
         if ($deep_detect) {
             if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                 $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
             if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                 $ip = $_SERVER['HTTP_CLIENT_IP'];
         }
     }
     $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
     $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
     $continents = array(
         "AF" => "Africa",
         "AN" => "Antarctica",
         "AS" => "Asia",
         "EU" => "Europe",
         "OC" => "Australia (Oceania)",
         "NA" => "North America",
         "SA" => "South America"
     );
     if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
         $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
         if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
             switch ($purpose) {
                 case "location":
                     $output = array(
                         "city"           => @$ipdat->geoplugin_city,
                         "state"          => @$ipdat->geoplugin_regionName,
                         "country"        => @$ipdat->geoplugin_countryName,
                         "country_code"   => @$ipdat->geoplugin_countryCode,
                         "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                         "continent_code" => @$ipdat->geoplugin_continentCode
                     );
                     break;
                 case "address":
                     $address = array($ipdat->geoplugin_countryName);
                     if (@strlen($ipdat->geoplugin_regionName) >= 1)
                         $address[] = $ipdat->geoplugin_regionName;
                     if (@strlen($ipdat->geoplugin_city) >= 1)
                         $address[] = $ipdat->geoplugin_city;
                     $output = implode(", ", array_reverse($address));
                     break;
                 case "city":
                     $output = @$ipdat->geoplugin_city;
                     break;
                 case "state":
                     $output = @$ipdat->geoplugin_regionName;
                     break;
                 case "region":
                     $output = @$ipdat->geoplugin_regionName;
                     break;
                 case "country":
                     $output = @$ipdat->geoplugin_countryName;
                     break;
                 case "countrycode":
                     $output = @$ipdat->geoplugin_countryCode;
                     break;
             }
         }
     }
     return $output;
 }


 function getEuropeISO($array, $exclude){
	 $check_iso = ip_info('', 'countrycode');

	 if(!in_array($check_iso, $exclude) && getContinentSelector($check_iso) == 'EU'){
		 $result = $check_iso;
	 }else{
		 $list_iso = array_keys($array);
		 $result = $list_iso[0];
	 }

	 return $result;
 }

 function getWorldISO($array, $exclude){
	 $check_iso = ip_info('', 'countrycode');

	 if(!in_array($check_iso, $exclude) && getContinentSelector($check_iso) != 'EU'){
		 $result = $check_iso;
	 }else{
		 $list_iso = array_keys($array);
		 $result = $list_iso[0];
	 }

	 return $result;
 }


 function checkEuropeISO($array, $exclude, $current){

	 $result = false;

	 if(array_key_exists($current, $array) && !in_array($current, $exclude) ){
		 $result = true;
	 }

	 return $result;
 }

 function checkWorldISO($array, $exclude, $current){

	 $result = false;

	 if(array_key_exists($current, $array) && !in_array($current, $exclude) ){
		 $result = true;
	 }

	 return $result;
 }

if ( ! function_exists( 'wcpbc_manual_country_script' ) ) {

	function wcpbc_manual_country_script() {
		?>
		<script type="text/javascript">
			// jQuery( document ).ready( function( $ ){
			// 	$('body').on('change', '.wcpbc-widget-country-selecting select.country', function(){
			// 		$(this).closest('form').submit();
			// 	} );
			// } );
		</script>
		<?php
	}
}

add_action( 'wp_print_footer_scripts', 'wcpbc_manual_country_script' ); ?>

<?php
$url = get_the_permalink();

$wpml_permalink_jp = apply_filters( 'wpml_permalink', $url , 'ja' );
$wpml_permalink_it = apply_filters( 'wpml_permalink', $url , 'it' );
$wpml_permalink_en = apply_filters( 'wpml_permalink', $url , 'en' );
?>

<script type="text/javascript">
	jQuery( document ).ready( function( $ ){
		$('.select-country__link').click(function(){
			$iso_selected = $(this).data('iso');
			$.cookie('setCountry', 1, { expires: 90, path: '/' });
			$('select.country').val($iso_selected).change();
			if($iso_selected == 'JP'){
				$('form.wcpbc-widget-country-selecting').attr('action', '<?php echo $wpml_permalink_jp; ?>');
			}else if($iso_selected == 'IT'){
				$('form.wcpbc-widget-country-selecting').attr('action', '<?php echo $wpml_permalink_it; ?>');
			}else{
				$('form.wcpbc-widget-country-selecting').attr('action', '<?php echo $wpml_permalink_en; ?>');
			}

			$('.wcpbc-widget-country-selecting').submit();
		})
	} );
</script>

<?php if ( $countries ) : ?>

	<form method="post" class="wcpbc-widget-country-selecting">
		<select class="country" name="wcpbc-manual-country">
			<?php foreach ($countries as $key => $value) : ?>
				<option value="<?php echo $key?>" <?php selected($key, $selected_country ); ?> ><?php echo $value; ?></option>
			<?php endforeach; ?>
		</select>
	</form>

<?php endif; ?>

<div class="select-country txt-ceter">
	<h2 class="select-country__main-title"><?php _e('Choose your country:', 'ae'); ?></h2>
	<div class="row columns-around">

		<div class="row__column tab-4">
			<a class="select-country__link animsition-link <?php if($selected_country == 'CA'){ echo 'current-country'; } ?>" data-iso="CA" href="#">
				<img class="select-country__flag" src="<?php echo bloginfo('template_directory'); ?>/img/canada.svg" alt="">
				<h3 class="select-country__title"><?php _e('Canada', 'ae'); ?></h3>
			</a>
		</div>

		<div class="row__column tab-4">
			<a class="select-country__link animsition-link <?php if($selected_country == 'CN'){ echo 'current-country'; } ?>" data-iso="CN" href="#">
				<img class="select-country__flag" src="<?php echo bloginfo('template_directory'); ?>/img/china.svg" alt="">
				<h3 class="select-country__title"><?php _e('China', 'ae'); ?></h3>
			</a>
		</div>

		<div class="row__column tab-4">
			<a class="select-country__link animsition-link <?php if($selected_country == 'JP'){ echo 'current-country'; } ?>" data-iso="JP" href="#">
				<img class="select-country__flag" src="<?php echo bloginfo('template_directory'); ?>/img/japan.svg" alt="">
				<h3 class="select-country__title"><?php _e('Japan', 'ae'); ?></h3>
			</a>
		</div>

		<div class="row__column tab-4">
			<a class="select-country__link animsition-link <?php if($selected_country == 'IT'){ echo 'current-country'; } ?>" data-iso="IT" href="#">
				<img class="select-country__flag" src="<?php echo bloginfo('template_directory'); ?>/img/italy.svg" alt="">
				<h3 class="select-country__title"><?php _e('Italy', 'ae'); ?></h3>
			</a>
		</div>

		<div class="row__column tab-4">
			<a class="select-country__link animsition-link <?php if($selected_country == 'US'){ echo 'current-country'; } ?>" data-iso="US" href="#">
				<img class="select-country__flag" src="<?php echo bloginfo('template_directory'); ?>/img/united-states-of-america.svg" alt="">
				<h3 class="select-country__title"><?php _e('USA', 'ae'); ?></h3>
			</a>
		</div>

		<div class="row__column tab-4">
			<a class="select-country__link animsition-link <?php if(checkEuropeISO($country_selector['EU']['countries'], $country_selectable, $selected_country) == true){ echo 'current-country'; } ?>" data-iso="<?php echo getEuropeISO($country_selector['EU']['countries'], $country_selectable); ?>" href="#">
				<img class="select-country__flag" src="<?php echo bloginfo('template_directory'); ?>/img/european-union.svg" alt="">
				<h3 class="select-country__title"><?php _e('Europe', 'ae'); ?></h3>
			</a>
		</div>
		<?php
		$country_merged = array_merge($country_selector['AS']['countries'], $country_selector['AN']['countries'], $country_selector['AF']['countries'], $country_selector['SA']['countries'], $country_selector['OC']['countries'], $country_selector['NA']['countries']);
		?>
		<div class="row__column tab-5">
			<a class="select-country__link animsition-link <?php if(checkWorldISO($country_merged, $country_selectable, $selected_country) == true){ echo 'current-country'; } ?>" data-iso="<?php echo getWorldISO($country_selector['AS']['countries'], $country_selectable); ?>" href="#">
				<img class="select-country__flag" src="<?php echo bloginfo('template_directory'); ?>/img/rest-of-the-world.svg" alt="">
				<h3 class="select-country__title"><?php _e('Other Countries', 'ae'); ?></h3>
			</a>
		</div>

	</div>
</div>
