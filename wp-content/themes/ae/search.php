<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/SearchResultsPage">

  <?php if ( have_posts() ) : ?>

    <header class="page-header">
      <h1 class="page-header__page-title txt-center">
        <?php printf( esc_html__( 'Search Results for: %s', 'ae' ), '<span>' . get_search_query() . '</span>' ); ?>
      </h1>
    </header><!-- .page-header -->

    <div class="grid-container--huge">
      <div class="row">
        <?php
        /* Start the Loop */
        while ( have_posts() ) : the_post(); ?>
          <div class="row__column tab-4">

            <?php get_template_part( 'template-parts/content', 'search' ); ?>

          </div>
        <?php endwhile; ?>
      </div>

    <?php else :
      get_template_part( 'template-parts/content', 'none' );
    endif; ?>
  </div><!-- .grid-container -->
</main><!-- #main -->


<?php get_footer(); ?>
