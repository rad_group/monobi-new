<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package framaework
 */

get_header(); ?>

<section class="single-performances">

  <?php

  get_template_part( 'template-parts/content', 'breadcrumbs' );

  get_template_part( 'template-parts/content', 'hero-cpt' );

  ?>

  <main id="main" class="site-main" role="main">
    <?php
    while ( have_posts() ) : the_post();

      get_template_part( 'template-parts/composer', '' );

    endwhile; // End of the loop.
    ?>
  </main><!-- #main -->

</section>
<?php get_footer(); ?>
