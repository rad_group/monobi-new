<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

get_header(); ?>

<div class="grid-container--large">
  <div class="row">
    <div class="row__column tab-8">
      <main id="main" class="site-main" role="main" itemprop="mainContentOfPage" itemscope="itemscope" itemtype="http://schema.org/Blog">

        <?php
        if ( have_posts() ) :

          /* Start the Loop */
          while ( have_posts() ) : the_post();

            get_template_part( 'template-parts/content', get_post_format() );

          endwhile;

          the_posts_navigation();

        else :

          get_template_part( 'template-parts/content', 'none' );

        endif; ?>

      </main><!-- #main -->
    </div>

    <div class="row__column tab-4">
      <?php get_sidebar(); ?>
    </div>

  </div> <!-- .row -->
</div>
<?php get_footer(); ?>
