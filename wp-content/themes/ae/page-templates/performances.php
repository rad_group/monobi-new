<?php
/**
 * The template to display Performances Page.
 *
 * Template Name: Performances
 *
 * This is the template that displays a page with custom performances.
 *
 * @package framaework
 */

get_header();

?>

<section class="archive-performance">
  <?php get_template_part( 'template-parts/content', 'breadcrumbs' ); ?>

  <?php
  $bg_image = '';
  $resize_hero = '';
  if( get_field('hero_bg_check') ){
    $bg_image = 'style="background-image: url('.get_field('hero_background_image').')"';
    $resize_hero = 'checked';
  }; ?>

  <div class="hero-cpt <?php echo $resize_hero; ?>" <?php echo $bg_image; ?>>
    <div class="grid-container--large">
      <div class="hero-cpt__content txt-center">

        <?php if( get_field('hero_icon') && get_field('hero_icon') != '' ): ?>
          <i class="iconae regular ae--<?php echo get_field('hero_icon'); ?>"></i>
        <?php endif; ?>

        <?php if( get_field('hero_title') ): ?>
          <h2 class="hero-cpt__title"><?php echo get_field('hero_title'); ?></h2>
        <?php endif; ?>

        <?php if( get_field('hero_subtitle') ): ?>
          <h3 class="hero-cpt__subtitle"><?php echo get_field('hero_subtitle'); ?></h3>
        <?php endif; ?>

        <?php if( get_field('hero_text') ): ?>
          <div class="hero-cpt__text"><?php echo get_field('hero_text'); ?></div>
        <?php endif; ?>

      </div>
    </div>
  </div>
  <main id="main" class="site-main" role="main">
    <section class="performance__body">
      <div class="grid-container--large">
        <?php
        $args = array( 'post_type' => 'performances', 'posts_per_page' => 99);
        $loop = new WP_Query( $args ); ?>

        <div class="row--half columns-between">

          <?php
          while ( $loop->have_posts() ) : $loop->the_post();
            $ID = get_the_ID();
            $text = get_field('hero_text', $ID);
            $bg_image = get_the_post_thumbnail_url($ID, 'large');
          ?>

            <div class="row__column tab-6">
              <article class="performance__article txt-center" style="background-image:url('<?php echo $bg_image; ?>')">
              <?php if ( !get_field('disable_link_bolean') ) : ?>
                <a href="<?php echo get_permalink(); ?>" class="performance__link animsition-link">
              <?php endif; ?>
                  <div class="overlay--gradient--static"></div>
                  <div class="performance__wrap">
                    <i class="iconae regular ae--<?php echo get_field('hero_icon', $ID); ?>"></i>
                    <div class="performance__content">
                      <h2 class="performance__title"><?php the_title(); ?></h2>
                      <span class="performance__text"><?php echo get_field('hero_subtitle', $ID); ?></span>
                    </div>
                  </div>
              <?php if ( !get_field('disable_link_bolean') ) : ?>
                </a>
              <?php endif; ?>
              </article>
            </div>
            <?php wp_reset_postdata(); ?>
          <?php endwhile; ?>
        </div>
      </div>
    </section>
    <?php get_template_part( 'template-parts/content', 'banner' ); ?>

  </main><!-- #main -->

</section>
<?php get_footer(); ?>
