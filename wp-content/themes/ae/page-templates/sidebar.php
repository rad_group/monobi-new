<?php
/**
 * The template to display Sidebar Page.
 *
 * Template Name: With sidebar
 *
 * This is the template that displays a page wit standard sidebar.
 *
 * @package framaework
 */

get_header(); ?>

<div class="grid-container--large">
  <div class="row">
    <div class="row__column tab-8">
      <main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">

        <?php
        while ( have_posts() ) : the_post();

          get_template_part( 'template-parts/content', 'page' );

          // If comments are open or we have at least one comment, load up the comment template.
          if ( comments_open() || get_comments_number() ) :
            comments_template();
          endif;

        endwhile; // End of the loop.
        ?>

      </main><!-- #main -->
    </div>

    <div class="row__column tab-4">
      <?php get_sidebar(); ?>
    </div>

  </div> <!-- .row -->
</div>
<?php get_footer(); ?>
