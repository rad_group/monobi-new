<?php
/**
 * The template to display Thank you Page.
 *
 * Template Name: Thank you
 *
 * This is the template that displays a page with custom thank you.
 *
 * @package framaework
 */

get_header(); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">

  <div class="grid-container--large txt-center">
    <div class="thank-you__box">
      <h1 class="page__title thank-you__title"><?php the_title(); ?>!</h1>
      <!-- If confirmation is needed. -->

      <p><?php _e('Thank you, your sign-up request was successful! Please check your email inbox to confirm.', 'ae'); ?></p>

      <!-- If confirmation is NOT needed. -->
      <?php /* <p><?php _e('Thank you, your sign-up request was successful!', 'ae'); ?></p> */ ?>
    </div>
  </div>


</main>

<?php get_footer(); ?>
