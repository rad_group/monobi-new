<?php
/**
 * The template to display Contacts Page.
 *
 * Template Name: Contacts
 *
 * This is the template that displays a Contacts page.
 *
 * @package framaework
 */

get_header(); ?>

<div class="grid-container--small">
  <main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">

    <section class="contacts">
      <h2 class="contacts__title txt-center"><?php the_title(); ?></h2>
      <?php if( get_field('contact_text') ): ?>
        <div class="contacts__content">
          <?php echo get_field('contact_text'); ?>
        </div>
      <?php endif; ?>
      <?php
      if ( defined( 'ICL_LANGUAGE_CODE' ) ) {
        if(ICL_LANGUAGE_CODE=='it'){
          echo do_shortcode('[contact-form-7 id="1783" title="Form contatti_it"]');
        }else{
          echo do_shortcode('[contact-form-7 id="468" title="Form contatti"]');
        }
      }
      ?>
      <?php// echo do_shortcode('[contact-form-7 id="468" title="Form contatti"]'); ?>
    </section>



  </main><!-- #main -->
</div>
<?php get_footer(); ?>
