<?php
/**
 * The template to display stories Page.
 *
 * Template Name: Stories
 *
 * This is the template that displays a page with custom stories.
 *
 * @package framaework
 */

get_header();

?>
<section class="archive-stories">
  <?php get_template_part( 'template-parts/content', 'breadcrumbs' ); ?>

  <?php
  $bg_image = '';
  $resize_hero = '';
  if( get_field('hero_bg_check') ){
    $bg_image = 'style="background-image: url('.get_field('hero_background_image').')"';
    $resize_hero = 'checked';
  }; ?>

  <div class="hero-cpt <?php echo $resize_hero; ?>" <?php echo $bg_image; ?>>
    <div class="grid-container--large">
      <div class="hero-cpt__content txt-center">

        <?php if( get_field('hero_icon') && get_field('hero_icon') != '' ): ?>
          <i class="iconae regular ae--<?php echo get_field('hero_icon'); ?>"></i>
        <?php endif; ?>

        <?php if( get_field('hero_title') ): ?>
          <h2 class="hero-cpt__title"><?php echo get_field('hero_title'); ?></h2>
        <?php endif; ?>

        <?php if( get_field('hero_subtitle') ): ?>
          <h3 class="hero-cpt__subtitle"><?php echo get_field('hero_subtitle'); ?></h3>
        <?php endif; ?>

        <?php if( get_field('hero_text') ): ?>
          <div class="hero-cpt__text"><?php echo get_field('hero_text'); ?></div>
        <?php endif; ?>

      </div>
    </div>
  </div>
  <main id="main" class="site-main" role="main">

    <section class="stories__body">

      <?php
      $terms = get_terms('categoria_stories');

      $args = array( 'post_type' => 'stories', 'posts_per_page' => 99);
      $loop = new WP_Query( $args ); ?>
      <ul class="stories__list">
        <li class="stories__list-item current js--filter" data-filter="*"><?php _e('All', 'ae'); ?></li>
        <?php foreach($terms as $term): ?>
          <li class="stories__list-item js--filter" data-filter="<?php echo $term->slug; ?>"><?php _e( $term->name, 'ae' ); ?></li>
        <?php endforeach; ?>
      </ul>
      <div class="grid-container--huge">
        <div class="row--half columns-between">

          <?php
          while ( $loop->have_posts() ) : $loop->the_post();
            $ID = get_the_ID();
            $text = get_field('hero_text', $ID);
            $bg_image = get_the_post_thumbnail_url($ID, 'square');

            $category = get_the_terms($ID, 'categoria_stories');

            $text_color = 'light';
            if (get_field('highlight_color', $ID)=='dark') {
              $text_color = 'dark';
            }
          ?>

            <div class="row__column tab-6 js--filter_article js--filter_<?php echo $category[0]->slug; ?>">
              <article class="stories" data-src="<?php echo $bg_image; ?>" >
                <a class="stories__link animsition-link" href="<?php echo get_permalink(); ?>" title="<?php echo $post->post_title; ?>">
                  <div class="overlay--gradient--static"></div>
                  <div class="stories__content">
                    <hgroup>
                      <h4 class="stories__category <?php echo $text_color; ?>"><?php echo $category[0]->name; ?></h4>
                      <h2 class="stories__title <?php echo $text_color; ?>"><?php echo $post->post_title; ?></h2>
                      <h3 class="stories__text <?php echo $text_color; ?>">
                        <?php if( get_field('hero_text', $ID) ): echo substr(get_field('hero_text', $ID), 0, 85).'...'; endif; ?>
                      </h3>
                    </hgroup>
                  </div>
                </a>

                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
              </article>
            </div>
          <?php endwhile; ?>
        </div>

      </div>
    </section>
    <?php get_template_part( 'template-parts/content', 'banner' ); ?>

  </main><!-- #main -->

</section>
<?php get_footer(); ?>
