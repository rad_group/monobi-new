<?php
/**
 * The template to display Home Page.
 *
 * Template Name: Home
 *
 * This is the template that displays a page with custom home.
 *
 * @package framaework
 */

get_header(); ?>

<?php get_template_part( 'template-parts/content', 'hero' ); ?>

<main id="main" class="site-main" role="main" role="main" itemprop="mainContentOfPage">

  <section id="overlap" class="overlap">
    <div class="grid-container--large">

      <div class="overlap__wrap">
        <div id="overlap__content" class="overlap__content">
          <?php if( get_field('overlap_title') ): ?>
            <h2 class="overlap__title"><?php the_field('overlap_title'); ?></h2>
            <hr class="overlap-score">
          <?php endif; ?>

          <?php if( get_field('overlap_text') ): ?>
            <p class="overlap__text"><?php the_field('overlap_text'); ?></p>
          <?php endif; ?>

            <a class="button gradient-button animsition-link" href="<?php echo get_field('overlap_link'); ?>" title="Shop page">
              <?php if( get_field('overlap_label') && get_field('overlap_label') != ''):
                echo '<i class="iconae filled ae--cart"></i> ' . get_field('overlap_label');
              else:
                echo '<i class="iconae filled ae--cart"></i>';
              endif; ?>
            </a>

        </div>
        <figure class="overlap__img-box">
          <div class="overlay--gradient--static"></div>
          <img src="<?php the_field('overlap_image'); ?>" alt="Monobi - <?php the_field('overlap_title'); ?>">
        </figure>
      </div>

    </div><!-- .grid-container -->
  </section><!-- .overlap -->

  <?php get_template_part( 'template-parts/content', 'latest' ); ?>

  <?php get_template_part( 'template-parts/content', 'banner' ); ?>

  <?php get_template_part( 'template-parts/content', 'newsletter' ); ?>

  <?php get_template_part( 'template-parts/content', 'instagram' ); ?>


</main><!-- #main -->

<?php get_footer(); ?>
