<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package framaework
 */

?>
  </div><!-- #content -->

  <?php get_template_part( 'template-parts/content', 'reassuring' ); ?>

  <section class="navigation-footer">
    <footer class="navigation-footer__body">
      <div class="grid-container--large">
        <div class="row">

          <div class="row__column mobile-6 tab-4">
            <div class="navigation-footer__primary">
              <?php bem_menu('primary_footer', 'primary-footer'); ?>
            </div>
          </div>

          <div class="row__column mobile-6 tab-3">
            <div class="navigation-footer__secondary">
              <?php bem_menu('secondary_footer', 'secondary-footer'); ?>
            </div>
          </div>

          <div class="row__column tab-5">
            <div class="navigation-footer__newsletter">
              <?php echo do_shortcode('[mc4wp_form id="1231"]'); ?>
            </div>
            <ul class="navigation-footer__social">
              <li class="navigation-footer__social-single"><a href="https://www.facebook.com/monobistudio" target="_blank"><i class="iconae filled ae--facebook-square"></i></a></li>
              <li class="navigation-footer__social-single"><a href="https://www.instagram.com/monobistudio/" target="_blank"><i class="iconae filled ae--instagram"></i></a></li>
            </ul>
            <figure class="navigation-footer__payments">
              <img class="navigation-footer__payments-image" src="<?php bloginfo('template_directory'); ?>/img/logo_paypal_carte_gray.png" alt="Paypal Credit Cards">
            </figure>
            <div data-popup="language" class="button-language hidden--on-tab js--popup-action"><?php _e('Languages', 'ae'); ?></div>
            <div data-popup="countrySelector" class="button-country js--popup-action"><?php _e('Choose country', 'ae'); ?></div>
          </div>

        </div> <!-- #.row -->
      </div>
    </footer>
  </section> <!-- #.navigation-footer -->

  <footer class="site-footer" role="contentinfo" itemscope="itemscope" itemtype="http://schema.org/WPFooter">

    <div class="grid-container--large">

      <div class="row--stuck">

        <div class="row__column mobile-8">
          <div class="site-footer__info">
            <span>© 2018 Monobi - P.IVA: 01629440973</span>
          </div><!-- .site-footer__info -->
        </div>

        <div class="row__column mobile-4">
          <div class="txt-right">
            <span>Made by </span><a class="animsition-link" href="https://we-rad.com" title="We Rad - Agenzia di Comunicazione Firenze">Rad.</a>
          </div>
        </div>

      </div>

    </div>
  </footer><!-- #colophon -->


  <a href="#" class="js--back-to-top-btn back-to-top-btn">
    <i class="iconae regular ae--arrow-up"></i>
  </a>

</div><!-- #page -->

<?php
if ( is_product() ) {
  get_template_part('template-parts/sizing-chart');
}
?>

<!-- Codice del Pop-up a fondo pagina (Prima della chiusura del body) -->
 <div class="js--popup popup--tiny" id="language">

   <div class="popup__wrap popup-languages vertical-align">
     <h2 class="language-menu__title txt-center"><?php _e('Choose your language') ?></h2>
     <?php bem_menu('language_menu_footer', 'language-menu'); ?>
     <div class="js--close-popup popup__button x3">×</div>
   </div>

 </div>

<div class="js--popup popup popup-lock" id="countrySelector">
  <div class="popup__wrap vertical-align">
    <?php do_action('wcpbc_manual_country_selector', 'Other countries'); ?>
    <button class="js--close-popup popup__button x3">×</button>
  </div>
</div>

<?php wp_footer(); ?>

</body>
</html>
