<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package framaework
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url'); ?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php bloginfo('template_url'); ?>/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php bloginfo('template_url'); ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <?php wp_head(); ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120218870-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-120218870-1');
    </script>

  </head>

  <body <?php body_class('animsition'); ?>>
    <div id="page" class="site">

      <header class="site-header" role="banner" itemscope="itemscope" itemtype="http://schema.org/WebPage">

          <div class="row--stuck columns-middle">
            <div class="row__column mobile-4 tab-5 eq-mobile-no-relative">
              <nav class="main-navigation" role="navigation">
                <div class="hamburger js--toggle-menu">
                  <span class="hamburger__elements">
                    <span></span>
                  </span>
                </div>
                <div class="main-navigation__wrap js--mobile-menu">
                  <?php bem_menu('primary', 'primary-menu', ''); ?>
                </div>
              </nav><!-- #site-navigation -->
            </div>


            <div class="row__column mobile-4 tab-2">
              <div class="site-branding">
                <h2 class="site-branding__site-title">
                  <a class="site-branding__link animsition-link" href="<?php bloginfo('url'); ?>" rel="home">
                    <img class="site-branding__logo" src="<?php bloginfo('template_directory'); ?>/img/monobi-logo.png" alt="">
                  </a>
                </h2>
              </div><!-- .site-branding -->
            </div>

            <div class="row__column mobile-4 tab-5 eq-mobile-no-relative">
              <nav class="secondary-navigation" role="navigation">
                <ul class="secondary-navigation__list inline-li txt-right">
                  <li class="secondary-navigation__list-item languages">
                    <?php bem_menu('language_menu', 'language-menu'); ?>
                    <i class="iconae light ae--arrow-small-down"></i>
                  </li>

                  <li class="secondary-navigation__list-item js--search">
                    <div class="secondary-navigation__list-item--search">
                      <i class="iconae regular ae--search"></i>
                    </div>
                  </li>

                  <?php if (is_user_logged_in()) : ?>
                    <li class="secondary-navigation__list-item">
                      <a class="secondary-navigation__list-item__link animsition-link" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Favorites' ) ) ) ?>"><i class="iconae filled ae--heart"></i></a>
                      <div class="secondary-navigation__list-item__box">
                        <h3 class="secondary-navigation__list-item__box-title"><?php _e('Favourites', 'ae'); ?></h3>
                        <span><?php _e('You have', 'ae'); ?></span><?php echo do_shortcode( '[ti_wishlist_products_counter]' ); ?><span><?php _e('items.', 'ae') ?></span>
                        <a class="button--round secondary-navigation__list-item__box-button animsition-link" href="<?php echo esc_url( get_permalink( get_page_by_title( 'Favorites' ) ) ) ?>"><?php _e('View Favourites') ?></a>
                      </div>
                    </li>
                  <?php endif; ?>

                  <?php if (is_user_logged_in()) : ?>
                    <li class="secondary-navigation__list-item">
                      <a class="secondary-navigation__list-item__link animsition-link" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="My account"><i class="iconae filled ae--user"></i></a>
                      <div class="secondary-navigation__list-item__box">
                        <h3 class="secondary-navigation__list-item__box-title"><?php _e('Your account', 'ae'); ?></h3>
                        <a class="animsition-link" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="My account"><?php _e('Dashboard', 'ae'); ?></a>
                        <a class="animsition-link" href="<?php echo bloginfo('wpurl'); ?>/my-account/customer-logout/" title="Logout"><?php _e('Logout', 'ae'); ?></a>
                      </div>
                    </li>
                  <?php else : ?>
                    <li class="secondary-navigation__list-item">
                      <a class="secondary-navigation__list-item__link animsition-link" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="My account"><i class="iconae filled ae--user"></i></a>
                      <div class="secondary-navigation__list-item__box">
                        <h3 class="secondary-navigation__list-item__box-title"><?php _e('Your account', 'ae'); ?></h3>
                        <a class="animsition-link" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="Login"><?php _e('Login', 'ae'); ?></a>
                        <a class="animsition-link" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="Register"><?php _e('Register', 'ae'); ?></a>
                      </div>
                    </li>
                  <?php endif; ?>

                  <li class="secondary-navigation__list-item">
                    <a class="secondary-navigation__list-item__link cart-btn animsition-link" href="<?php echo wc_get_cart_url(); ?>" title="Shopping Cart"><i class="iconae filled ae--cart"></i></a>
                    <div class="secondary-navigation__list-item__box">
                      <h3 class="secondary-navigation__list-item__box-title"><?php _e('Cart', 'ae'); ?></h3>
                      <?php $cart_items = WC()->cart->get_cart_contents_count();  ?>
                      <?php if( $cart_items < 0): ?>
                        <div class="empty">
                          <span><?php _e('Your cart is empty', 'ae'); ?></span>
                        </div>
                      <?php else: ?>
                        <div class="cart-items">
                          <span><?php _e('You have ', 'ae'); ?><b><?php echo $cart_items; ?></b><?php _e(' items', 'ae'); ?></span><br/>
                          <span><?php _e('Total ammount ', 'ae'); ?><?php echo WC()->cart->get_cart_total() ?></span>
                        </div>
                      <?php endif; ?>
                    </div>
                  </li>

                </ul>
              </nav><!-- #site-navigation -->
            </div>

          </div>

        <div class="search__bar">
          <?php echo get_search_form(); ?>
        </div>

      </header><!-- #masthead -->

      <div id="content" class="site-content">
