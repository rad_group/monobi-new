<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <input type="text" class="search-form__input" placeholder="<?php echo esc_attr_x( 'Type here to search inside the site', 'placeholder' ) ?>" autocomplete="off" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( '', 'label' ) ?>" >
</form>
