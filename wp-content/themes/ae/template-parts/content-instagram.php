<section class="instagram">
  <a class="instagram__link animsition-link" target="_blank" href="https://www.instagram.com/monobistudio/">
    <div class="instagram__cta vertical-align txt-center">
      <span class="instagram__tag">@monobistudio</span>
      <h2 class="instagram__title"><?php the_field('instagram_text'); ?></h2>
    </div>
    <div class="row--stuck columns-middle">

      <?php
      $insta_options = array();
      $insta_options['num_post'] = 20;
      $insta_options['num_view'] = 6;
      $insta_options['accept_format'] = array(
                                              //'video',
                                              //'carousel',
                                              'image'
                                            );
      $insta_options['carousel_multiple'] = 1;
      $insta_image = array();

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, 20);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_URL, 'https://api.instagram.com/v1/users/self/media/recent/?access_token=1133176349.77960f1.8c31e01281b44fc98e679d8dc08c374b&count='.$insta_options['num_post']);

      $output = curl_exec($ch);
      curl_close($ch);

      $output = json_decode($output, true);

      foreach ($output['data'] as $insta_post) {
        if (in_array($insta_post['type'], $insta_options['accept_format'])) : ?>

        <?php
        if($insta_post['type'] == 'carousel' && $insta_options['carousel_multiple'] == 1){
          foreach ($insta_post['carousel_media'] as $insta_post_carousel) {
            $thumbnail = $insta_post_carousel['images']['standard_resolution']['url'];
            //$insta_image[] = 'https://www.monobi-it.com/insta_image.php?s='.$thumbnail;
            $insta_image[] = $thumbnail;
          }
        }else{
          $thumbnail = $insta_post['images']['standard_resolution']['url'];
          //$insta_image[] = 'https://www.monobi-it.com/insta_image.php?s='.$thumbnail;
          $insta_image[] = $thumbnail;
        }
        endif;
      }
      ?>


      <?php
      $num_feed=1;

      foreach ($insta_image as $image) {
        if ($num_feed <= $insta_options['num_view']) {
      ?>
      <div class="row__column mobile-6 tab-2">
        <figure class="instagram__overlay insta-<?php echo $num_feed; ?>" style="background-image: url(<?php echo $image; ?>)" itemprop="associatedMedia" itemscope itemtype="https://schema.org/ImageObject">
          <?php /*
          <img class="instagram__img" <?php if ( $num_feed == 0 ) {echo 'class="js--parent-h js--parent-intagram-h"';} ?> src="<?php echo $image; ?>" itemprop="thumbnail" alt="Monobistudio Instagram image" > */?>
          <!-- <figcaption class="home-instagram__caption hidden--on-eq-mobile" itemprop="caption description">
            <div class="home-instagram__txt">
              <?php// echo $insta_post['caption']['text']; ?>
            </div>
          </figcaption> -->
        </figure>
      </div>
      <?php
          $num_feed++;
        }
      }
      ?>





    </div>
  </a>
</section>
