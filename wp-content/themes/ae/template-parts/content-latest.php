<section class="latest">
  <div class="grid-container--large">
    <div class="row--half columns-around">
      <?php
        $boxes = array();

        $boxes[] = 'performances';
        $boxes[] = 'stories';

      ?>

      <?php foreach ($boxes as $value) :
        $post_object = get_field('highlight_'.$value);
        $post = $post_object;
        setup_postdata( $post );
        $ID = get_the_ID();

        $post_type = $post->post_type;

        $tax = 'categoria_'.$post_type;
        $term = get_the_terms($ID, 'categoria_'.$post_type);

        $text_color = 'light';
        if (get_field('highlight_color', $ID)=='dark') {
          $text_color = 'dark';
        }

      ?>
      <div class="row__column tab-6">
        <article class="box" style="background-image:url('<?php the_post_thumbnail_url('square'); ?>')">
          <a class="box__link animsition-link" href="<?php echo get_permalink($ID); ?>" title="<?php echo $post->post_title; ?>">
            <div class="overlay--gradient--static"></div>
            <div class="box__content">
              <hgroup>
                <h4 class="box__category <?php echo $text_color; ?>"><?php echo $term[0]->name; ?></h4>
                <h2 class="box__title <?php echo $text_color; ?>"><?php echo $post->post_title; ?></h2>
                <h3 class="box__subtitle <?php echo $text_color; ?>">
                  <?php if( get_field('hero_subtitle', $ID) ): the_field('hero_subtitle', $ID); endif; ?>
                </h3>
              </hgroup>
              <hr class="middle-score <?php echo $text_color; ?>">
              <span class="box__discover <?php echo $text_color; ?>">
                <?php _e('Discover', 'ae'); ?> <i class="iconae filled ae--arrow-right"></i>
              </span>
            </div>
          </a>
          <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
        </article>
      </div>
      <?php endforeach; ?>

    </div> <!-- .row -->
  </div><!-- .grid-container -->
</section><!-- .latest -->
