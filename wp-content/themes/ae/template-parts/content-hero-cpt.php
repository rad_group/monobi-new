<?php
$bg_image = '';
$resize_hero = '';
if( get_field('hero_bg_check') ){
  $bg_image = 'style="background-image: url('.get_field('hero_background_image').')"';
  $resize_hero = 'checked';
};

if ( get_field('hero_title') ) : ?>
  <div class="hero-cpt <?php echo $resize_hero; ?>" <?php echo $bg_image; ?>>
    <div class="grid-container--large">
      <div class="hero-cpt__content txt-center">

        <?php if( get_field('hero_icon') && get_field('hero_icon') != '' ): ?>
          <i class="iconae regular ae--<?php echo get_field('hero_icon'); ?>"></i>
        <?php endif; ?>

        <?php if( get_field('hero_title') ): ?>
          <h2 class="hero-cpt__title"><?php echo get_field('hero_title'); ?></h2>
        <?php endif; ?>

        <?php if( get_field('hero_subtitle') ): ?>
          <h3 class="hero-cpt__subtitle"><?php echo get_field('hero_subtitle'); ?></h3>
        <?php endif; ?>

        <?php if( get_field('hero_text') ): ?>
          <h3 class="hero-cpt__text"><?php echo get_field('hero_text'); ?></h3>
        <?php endif; ?>

      </div>
    </div>
  </div>
<?php endif; ?>
