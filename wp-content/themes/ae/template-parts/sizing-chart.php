<!-- Codice del Pop-up a fondo pagina (Prima della chiusura del body) -->
<div class="js--popup popup--full" id="sizing">
   <div class="popup__wrap sizing-table">
     <button class="js--close-popup popup__button x3">×</button>
     <div class="sizing-content grid-container--large">

       <h2 class="sizing-table__title">Sizing Information</h2>
       <table>
         <tbody>
           <tr>
             <td style="width: 82px;">SIZE</td>
             <td style="width: 82px;">US</td>
             <td style="width: 82px;">UK</td>
             <td style="width: 83px;">EU</td>
             <td style="width: 83px;">JP</td>
             <td style="width: 84px;">CHEST</td>
             <td style="width: 92px;">ARM LENGTH</td>
           </tr>
           <tr>
             <td style="width: 82px;">XS</td>
             <td style="width: 82px;">32</td>
             <td style="width: 82px;">32</td>
             <td style="width: 83px;">44</td>
             <td style="width: 83px;"></td>
             <td style="width: 84px;">93-97</td>
             <td style="width: 92px;">62-63</td>
           </tr>
           <tr>
             <td style="width: 82px;">S</td>
             <td style="width: 82px;">36</td>
             <td style="width: 82px;">36</td>
             <td style="width: 83px;">46/48</td>
             <td style="width: 83px;">XS</td>
             <td style="width: 84px;">98-102</td>
             <td style="width: 92px;">64-65</td>
           </tr>
           <tr>
             <td style="width: 82px;">M</td>
             <td style="width: 82px;">42</td>
             <td style="width: 82px;">42</td>
             <td style="width: 83px;">50</td>
             <td style="width: 83px;">S</td>
             <td style="width: 84px;">108-112</td>
             <td style="width: 92px;">68-69</td>
           </tr>
           <tr>
             <td style="width: 82px;">L</td>
             <td style="width: 82px;">40</td>
             <td style="width: 82px;">40</td>
             <td style="width: 83px;">52</td>
             <td style="width: 83px;">M</td>
             <td style="width: 84px;">103-107</td>
             <td style="width: 92px;">66-67</td>
           </tr>
           <tr>
             <td style="width: 82px;">XL</td>
             <td style="width: 82px;">44</td>
             <td style="width: 82px;">44</td>
             <td style="width: 83px;">54</td>
             <td style="width: 83px;">L</td>
             <td style="width: 84px;">113-117</td>
             <td style="width: 92px;">70-71</td>
           </tr>
           <tr>
             <td style="width: 82px;">XXL</td>
             <td style="width: 82px;">46</td>
             <td style="width: 82px;">46</td>
             <td style="width: 83px;">56</td>
             <td style="width: 83px;">LL</td>
             <td style="width: 84px;">118-122</td>
             <td style="width: 92px;">28 - 28,7</td>
           </tr>
         </tbody>
       </table>

    </div>
  </div>
</div>
