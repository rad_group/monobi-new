<?php
/**
 * Template part for displaying single posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?> itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost">

  <header class="single-post__header">

  <?php the_title( '<h1 class="single-post__title">', '</h1>' ); ?>

  <?php if ( 'post' === get_post_type() ) : ?>
    <div class="single-post__meta">
      <?php ae_posted_on(); ?>
    </div><!-- .single-post__meta -->
  <?php endif; ?>

  </header><!-- .single-post__header -->

  <div class="single-post__content">
    <?php
    the_content( sprintf(
      /* translators: %s: Name of current post. */
      wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'ae' ), array( 'span' => array( 'class' => array() ) ) ),
      the_title( '<span class="screen-reader-text">"', '"</span>', false )
    ) );

    wp_link_pages( array(
      'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'ae' ),
      'after'  => '</div>',
    ) );
    ?>
  </div><!-- .single-post__content -->

  <footer class="single-post__footer">
    <?php ae_entry_footer(); ?>
  </footer><!-- .single-post__footer -->

</article><!-- #post-## -->
