<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>

<section class="no-results not-found">
  <div class="grid-container--large txt-center">
    <div class="not-found__box">
      <h1 class="not-found__title"><?php esc_html_e( 'Nothing Found', 'ae' ); ?></h1>
      <?php
      if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

        <p><?php printf( wp_kses( __( 'Ready to publish your first post? <a class="animsition-link" href="%1$s">Get started here</a>.', 'ae' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

      <?php elseif ( is_search() ) : ?>

        <p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'ae' ); ?></p>

        <?php get_search_form(); ?>

      <?php else : ?>

        <p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'ae' ); ?></p>

        <?php get_search_form(); ?>

      <?php endif; ?>

    </div>
  </div>
</section><!-- .no-results -->
