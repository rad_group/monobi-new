<?php
  $text_color = 'light';
  if (get_field('banner_color', $ID)=='dark') {
    $text_color = 'dark';
  };

  $add_cart = '';
  if(get_field('banner_cart', $ID)){
    $add_cart = '<i class="iconae filled ae--cart"></i>';
  };

  $ID = get_the_ID();
  $title = get_field('banner_title', $ID);
  $subtitle = get_field('banner_subtitle', $ID);
  $text = get_field('banner_content', $ID);
?>

<section class="banner <?php echo $text_color; ?> txt-center">
  <div class="grid-container">
    <div class="banner__box">
      <h2 class="banner__title <?php echo $text_color; ?>"> <?php if( $title ){ echo $title; } ?></h2>
      <h3 class="banner__subtitle <?php echo $text_color; ?>"> <?php if( $subtitle ){ echo $subtitle; } ?></h3>
      <p class="banner__text <?php echo $text_color; ?>"> <?php if( $text ){ echo $text; } ?></p>
      <?php if( get_field('banner_label') && get_field('banner_label') != ''): ?>
        <a class="button gradient-button animsition-link" href="<?php bloginfo('url'); ?>/about-us" title="About us">
          <?php echo $add_cart . ' ' . get_field('banner_label'); ?>
        </a>
      <?php endif; ?>
    </div>
  </div>
</section>
