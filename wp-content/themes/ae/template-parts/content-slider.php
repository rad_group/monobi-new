<?php $ID = 5;

if( have_rows('header_slider', $ID) ): ?>
  <section class="hero-slider">
    <?php while( have_rows('header_slider', $ID) ): the_row();
      $color = 'light';
      if( get_sub_field('header_slider_color')=='dark' ):
        $color = 'dark';
      endif;

      $slider_link = '';
      if(get_sub_field('header_slider_link')):
        $slider_link = get_sub_field('header_slider_link');
      endif;
    ?>

      <div class="hero-slider__wrap" <?php if( get_sub_field('slider_background_image', $ID) ){ ?> style="background-image:url('<?php echo get_sub_field('slider_background_image', $ID); ?>')"<?php }; ?> >
        <div class="overlay--gradient--static"></div>
        <div class="hero-slider__content">
          <?php if( get_sub_field('slider_title', $ID) ): ?>
            <h2 class="hero-slider__title <?php echo $color; ?>"><?php echo get_sub_field('slider_title', $ID); ?></h2>
          <?php endif; ?>

          <?php if( get_sub_field('slider_subtitle', $ID) ): ?>
            <hr class="middle-score <?php echo $color; ?>">
            <a class="hero-slider__link animsition-link" href="<?php echo $slider_link ?>">
              <h3 class="hero-slider__subtitle <?php echo $color; ?>"><?php echo get_sub_field('slider_subtitle', $ID); ?> <i class="iconae filled ae--arrow-right"></i></h3>
            </a>
          <?php endif; ?>
        </div>
      </div>

    <?php endwhile; ?>
  </section>
<?php endif; ?>
