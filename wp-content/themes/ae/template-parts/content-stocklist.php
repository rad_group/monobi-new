<section class="stocklist">
  <div class="grid-container">
    <?php
    if( have_rows('stocklist_table') ) : ?>

      <?php while ( have_rows('stocklist_table') ) : the_row(); ?>

        <?php if ( get_sub_field('stocklist_table_country_bolean') ) : ?>
          <h2 class="stocklist__country-title">
            <?php echo get_sub_field('stocklist_table_title'); ?>
          </h2>
        <?php else : ?>
          <article class="stocklist__single-retail">
            <?php if ( get_sub_field('stocklist_table_link') ) : ?>
              <a class="animsition-link" href="<?php echo get_sub_field('stocklist_table_link'); ?>" title="<?php echo get_sub_field('stocklist_table_title'); ?>" target="_blank">
            <?php endif; ?>
              <?php echo get_sub_field('stocklist_table_title'); ?>
            <?php if ( get_sub_field('stocklist_table_link') ) : ?>
              <i class="iconae regular ae--circle-plus"></i>
              </a>
            <?php endif; ?>
          </article>
        <?php endif; ?>

      <?php endwhile; ?>

    <?php endif; ?>
  </div>
</section>
