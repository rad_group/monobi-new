<section class="hero-header">
    <div class="hero-header__wrap" <?php if( get_field('header_background_image') ): ?> style="background-image:url('<?php echo get_field('header_background_image'); ?>')"<?php endif; ?> >
      <div class="hero-header__content">
        <?php if( get_field('header_title') ): ?>
          <h2 class="hero-header__title"><?php echo get_field('header_title'); ?></h2>
        <?php endif; ?>

        <?php if( get_field('header_subtitle') ): ?>
          <hr class="middle-score">
          <a class="hero-header__link animsition-link" href="<?php echo get_field('header_link'); ?>">
            <h3 class="hero-header__subtitle"><?php echo get_field('header_subtitle'); ?> <i class="iconae filled ae--arrow-right"></i></h3>
          </a>
        <?php endif; ?>
      </div>
    </div>
</section>
