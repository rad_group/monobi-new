<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

global $product;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?>>

  <header class="single-post__header">
    <div class="product-box backface">
      <?php echo get_the_post_thumbnail('', 'square'); ?>
    </div>

    <h2 class="single-post__title">
      <?php the_title( sprintf( '<a class="animsition-link" href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a>' ); ?>
    </h2><!-- .single-post__title -->

    <div class="summary__short-description">
			<?php $ID = get_the_ID();
        echo get_field('single_short_description', $ID);
      ?>
		</div>

  </header><!-- .single-post__header -->

</article><!-- #post-## -->
