<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>

<?php
if ( is_cart() || is_account_page() || is_page( 'wishlist' ) ) {

} else {
  get_template_part( 'template-parts/content', 'breadcrumbs' );
} ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('page'); ?> itemscope="itemscope" itemtype="http://schema.org/CreativeWork">
  <?php if ( !get_field('hero_title') ) : ?>
    <header class="page__header">
      <?php the_title( '<h1 class="page__title">', '</h1>' ); ?>
    </header><!-- .page__header -->
  <?php endif; ?>

  <div class="page__content">
    <div class="hentry__content">
      <?php the_content(); ?>
    </div>
  </div>


    <?php wp_link_pages( array(
      'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'ae' ),
      'after'  => '</div>',
    ) );
    ?>
  </div><!-- .page__content -->
</article><!-- #post-## -->
