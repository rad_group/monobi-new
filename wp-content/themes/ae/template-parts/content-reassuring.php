<?php


  $tab = array();

  $tab[] = 'returns';
  $tab[] = 'italy';
  $tab[] = 'payment';
  $tab[] = 'shipping';

  $tab_count = 12 / count($tab);
?>

<section class="reassuring">
  <div class="grid-container--large">
    <div class="row--stuck columns-between">

      <?php foreach ($tab as $key => $field) :
        $icon = get_field('reassuring_icon_'.$field, 'option');
        $title = get_field('reassuring_title_'.$field, 'option');
        $sub = get_field('reassuring_subtitle_'.$field, 'option');
      ?>
        <div class="row__column mobile-6 tab-<?php echo $tab_count; ?>">
          <div class="reassuring__box txt-center">
            <div class="reassuring__icon">
              <i class="iconae light ae--<?php echo $icon; ?>"></i>
            </div>
            <h3 class="reassuring__title"><?php echo $title; ?></h3>
            <h4 class="reassuring__subtitle"><?php echo $sub; ?></h4>
          </div>
        </div>
      <?php endforeach; ?>

    </div>
  </div>

</section>
