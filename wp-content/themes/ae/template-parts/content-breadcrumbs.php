<div class="bread__bar">
  <div class="bread__container">
    <div class="bread__crumbs">
      <?php
        /**
         * Hook: woocommerce_before_main_content.
         *
         * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
         * @hooked woocommerce_breadcrumb - 20
         * @hooked WC_Structured_Data::generate_website_data() - 30
         */

        //do_action( 'woocommerce_before_main_content' );
        $args = array(
          'delimiter' => ' <i class="iconae bread__slicer light ae--arrow-double-right"></i> ',
        );
        woocommerce_breadcrumb( $args );

      ?>
    </div>
    <?php
      /**
       * Hook: woocommerce_before_shop_loop.
       *
       * @hooked wc_print_notices - 10
       * @hooked woocommerce_result_count - 20
       * @hooked woocommerce_catalog_ordering - 30
       */

      // Removed notices hook
      remove_action( 'woocommerce_before_shop_loop', 'wc_print_notices', 10 );
      remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
      do_action( 'woocommerce_before_shop_loop' );

    ?>
  </div>
</div>
