<?php
/**
 * Template part for displaying posts on blog page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package framaework
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post_preview'); ?> itemscope="itemscope" itemtype="http://schema.org/BlogPosting" itemprop="blogPost">

  <header class="post-preview__header">
    <h2 class="post-preview__title">
      <?php the_title( '<a class="post-preview__link animsition-link" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a>' ); ?>
    </h2> <!-- .post-preview__title -->

  <?php if ( 'post' === get_post_type() ) : ?>
    <div class="post-preview__meta">
      <?php ae_posted_on(); ?>
    </div><!-- .post-preview__meta -->
  <?php endif; ?>

  </header><!-- .post-preview__header -->

  <div class="post-preview__content">
    <?php
    the_content( sprintf(
      /* translators: %s: Name of current post. */
      wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'ae' ), array( 'span' => array( 'class' => array() ) ) ),
      the_title( '<span class="screen-reader-text">"', '"</span>', false )
    ) );

    wp_link_pages( array(
      'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'ae' ),
      'after'  => '</div>',
    ) );
    ?>
  </div><!-- .post-preview__content -->

  <footer class="post-preview__footer">
    <?php ae_entry_footer(); ?>
  </footer><!-- .post-preview__footer -->

</article><!-- #post-## -->
