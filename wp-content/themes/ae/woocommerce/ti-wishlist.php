<?php
/**
 * The Template for displaying wishlist.
 *
 * @version             1.7.0
 * @package           TInvWishlist\Template
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="tinv-wishlist woocommerce tinv-wishlist-clear">
	<?php do_action( 'tinvwl_before_wishlist', $wishlist ); ?>
	<?php if ( function_exists( 'wc_print_notices' ) ) {
		wc_print_notices();
	} ?>
	<form action="<?php echo esc_url( tinv_url_wishlist() ); ?>" method="post" autocomplete="off">
		<?php do_action( 'tinvwl_before_wishlist_table', $wishlist ); ?>
		<table class="tinvwl-table-manage-list">
			<thead>
			<tr>
				<th class="product-name">
					<span class="tinvwl-full"><?php esc_html_e( 'Product Name', 'ti-woocommerce-wishlist-premium' ); ?></span>
					<span class="tinvwl-mobile"><?php esc_html_e( 'Product', 'ti-woocommerce-wishlist-premium' ); ?></span>
				</th>
				<th class="product-thumbnail">&nbsp;</th>
				<?php if ( isset( $wishlist_table_row['colm_price'] ) && $wishlist_table_row['colm_price'] ) { ?>
					<th class="product-price"><?php esc_html_e( 'Unit Price', 'ti-woocommerce-wishlist-premium' ); ?></th>
				<?php } ?>
				<?php if ( isset( $wishlist_table_row['colm_stock'] ) && $wishlist_table_row['colm_stock'] ) { ?>
					<th class="product-stock"><?php esc_html_e( 'Stock Status', 'ti-woocommerce-wishlist-premium' ); ?></th>
				<?php } ?>

				<?php if ( ( isset( $wishlist_table_row['move'] ) && $wishlist_table_row['move'] ) || ( isset( $wishlist_table_row['add_to_cart'] ) && $wishlist_table_row['add_to_cart'] ) ) { ?>
					<th class="product-action">&nbsp;</th>
					<th class="product-remove"></th>
				<?php } ?>
			</tr>
			</thead>
			<tbody>
			<?php do_action( 'tinvwl_wishlist_contents_before' ); ?>

			<?php
			foreach ( $products as $wl_product ) {
				$product = apply_filters( 'tinvwl_wishlist_item', $wl_product['data'] );
				unset( $wl_product['data'] );
				if ( $wl_product['quantity'] > 0 && apply_filters( 'tinvwl_wishlist_item_visible', true, $wl_product, $product ) ) {
					$product_url = apply_filters( 'tinvwl_wishlist_item_url', $product->get_permalink(), $wl_product, $product );
					do_action( 'tinvwl_wishlist_row_before', $wl_product, $product );
					?>
					<tr class="<?php echo esc_attr( apply_filters( 'tinvwl_wishlist_item_class', 'wishlist_item', $wl_product, $product ) ); ?>">
						<td class="product-thumbnail">
							<?php
							$thumbnail = apply_filters( 'tinvwl_wishlist_item_thumbnail', $product->get_image(), $wl_product, $product );

							if ( ! $product->is_visible() ) {
								echo $thumbnail; // WPCS: xss ok.
							} else {
								printf( '<a href="%s">%s</a>', esc_url( $product_url ), $thumbnail ); // WPCS: xss ok.
							}
							?>
						</td>
						<td class="product-name">
							<?php
							if ( ! $product->is_visible() ) {
								echo apply_filters( 'tinvwl_wishlist_item_name', $product->get_title(), $wl_product, $product ) . '&nbsp;'; // WPCS: xss ok.
							} else {
								echo apply_filters( 'tinvwl_wishlist_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_url ), $product->get_title() ), $wl_product, $product ); // WPCS: xss ok.
							}

							echo apply_filters( 'tinvwl_wishlist_item_meta_data', tinv_wishlist_get_item_data( $product, $wl_product ), $wl_product, $product ); // WPCS: xss ok.
							?>
						</td>
						<?php if ( isset( $wishlist_table_row['colm_price'] ) && $wishlist_table_row['colm_price'] ) { ?>
							<td class="product-price">
								<?php
								echo apply_filters( 'tinvwl_wishlist_item_price', $product->get_price_html(), $wl_product, $product ); // WPCS: xss ok.
								?>
							</td>
						<?php } ?>
						<?php if ( isset( $wishlist_table_row['colm_stock'] ) && $wishlist_table_row['colm_stock'] ) { ?>
							<td class="product-stock">
								<?php
								$availability = (array) $product->get_availability();
								if ( ! array_key_exists( 'availability', $availability ) ) {
									$availability['availability'] = '';
								}
								if ( ! array_key_exists( 'class', $availability ) ) {
									$availability['class'] = '';
								}
								$availability_html = empty( $availability['availability'] ) ? '<p class="stock ' . esc_attr( $availability['class'] ) . '"><span><i class="fa fa-check"></i></span><span class="tinvwl-txt">' . esc_html__( 'In stock', 'ti-woocommerce-wishlist-premium' ) . '</span></p>' : '<p class="stock ' . esc_attr( $availability['class'] ) . '"><span><i class="fa fa-check"></i></span><span>' . esc_html( $availability['availability'] ) . '</span></p>';

								echo apply_filters( 'tinvwl_wishlist_item_status', $availability_html, $availability['availability'], $wl_product, $product ); // WPCS: xss ok.
								?>
							</td>
						<?php } ?>
						<?php if ( ( isset( $wishlist_table_row['move'] ) && $wishlist_table_row['move'] ) || ( isset( $wishlist_table_row['add_to_cart'] ) && $wishlist_table_row['add_to_cart'] ) ) { ?>
							<td class="product-action">
								<?php
								if ( apply_filters( 'tinvwl_wishlist_item_action_add_to_cart', $wishlist_table_row['add_to_cart'], $wl_product, $product ) ) {
									?>
									<button class="button--round alt" name="tinvwl-add-to-cart"
									        value="<?php echo esc_attr( $wl_product['ID'] ); ?>"><span
											class="tinvwl-txt"><?php echo esc_html( apply_filters( 'tinvwl_wishlist_item_add_to_cart', $wishlist_table_row['text_add_to_cart'], $wl_product, $product ) ); ?></span>
									</button>
								<?php } ?>
								<?php
								if ( apply_filters( 'tinvwl_wishlist_item_action_move', $wishlist_table_row['move'], $wl_product, $product ) ) {
									echo apply_filters( 'tinvwl_wishlist_item_move', sprintf( '<button class="button--round tinvwl_move_product_button" type="button" name="move"><span class="tinvwl-txt">%s</span></button>', esc_html( __( 'Move', 'ti-woocommerce-wishlist-premium' ) ) ), $wl_product, $product, $wishlist ); // WPCS: xss ok.
								} ?>
							</td>
							<td class="product-remove">
								<button type="submit" name="tinvwl-remove" value="<?php echo esc_attr( $wl_product['ID'] ); ?>"><i class="iconae regular ae--cross"></i></button>
							</td>
						<?php } ?>
					</tr>
					<?php
					do_action( 'tinvwl_wishlist_row_after', $wl_product, $product );
				} // End if().
			} // End foreach().
			?>
			<?php do_action( 'tinvwl_wishlist_contents_after' ); ?>
			</tbody>
			<tfoot>
			<tr>
				<td colspan="100">
					<?php do_action( 'tinvwl_after_wishlist_table', $wishlist ); ?>
					<?php wp_nonce_field( 'tinvwl_wishlist_owner', 'wishlist_nonce' ); ?>
				</td>
			</tr>
			</tfoot>
		</table>
	</form>
	<?php do_action( 'tinvwl_after_wishlist', $wishlist ); ?>
	<div class="tinv-lists-nav tinv-wishlist-clear">
		<?php do_action( 'tinvwl_pagenation_wishlist', $wishlist ); ?>
	</div>
</div>
