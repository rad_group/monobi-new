<?php
/**
 * The Template for displaying dialog for add to wishlist product.
 *
 * @version             1.7.0
 * @package           TInvWishlist\Template
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="tinvwl_add_to_select_wishlist tinv-modal">
	<div class="tinv-overlay"></div>
	<div class="tinv-table">
		<div class="tinv-cell">
			<div class="tinv-modal-inner">
				<i class="ti-icon icon_big_heart_plus"></i>
				<label>
					<?php esc_html_e( 'Choose Wishlist:', 'ti-woocommerce-wishlist-premium' ); ?>
					<select class="tinvwl_wishlist"></select>
				</label>
				<input class="tinvwl_new_input" style="display: none" type="text" value=""/>
				<button class="tinvwl_button_add button--round" type="button">
					<i class="fa fa-heart-o"></i><?php _e('Add to Favourites', 'ae'); ?>
				</button>
				<button class="button tinvwl_button_close button--round" type="button"><i class="fa fa-times"></i><?php esc_html_e( 'Close', 'ti-woocommerce-wishlist-premium' ); ?></button>
				<div class="tinv-wishlist-clear"></div>
			</div>
		</div>
	</div>
</div>
