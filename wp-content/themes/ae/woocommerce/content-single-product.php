<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>

<section class="product-single">

	<?php get_template_part( 'template-parts/content', 'breadcrumbs' ); ?>

	<div id="product-<?php the_ID(); ?>" <?php post_class('single__article'); ?>>
	  <div class="row--stuck">
	    <div class="row__column tab-6 desk-7">
	      <?php
	    		/**
	    		 * Hook: woocommerce_before_single_product_summary.
	    		 *
	    		 * @hooked woocommerce_show_product_sale_flash - 10
	    		 * @hooked woocommerce_show_product_images - 20
	    		 */
	    		do_action( 'woocommerce_before_single_product_summary' );
	    	?>
	    </div>

	    <div class="row__column tab-6 desk-5">
	    	<div class="summary entry-summary txt-left">
					<div class="summary__content">
		    		<?php
		    			/**
		    			 * Hook: Woocommerce_single_product_summary.
		    			 *
		    			 * @hooked woocommerce_template_single_title - 5
		    			 * @hooked woocommerce_template_single_rating - 10
		    			 * @hooked woocommerce_template_single_price - 10
		    			 * @hooked woocommerce_template_single_excerpt - 20
		    			 * @hooked woocommerce_template_single_add_to_cart - 30
		    			 * @hooked woocommerce_template_single_meta - 40
		    			 * @hooked woocommerce_template_single_sharing - 50
		    			 * @hooked WC_Structured_Data::generate_product_data() - 60
		    			 */
							?>


							<?php
              add_action('woocommerce_single_product_summary', 'rad_single_short_description', 5);
							remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
              remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
							add_action( 'woocommerce_before_add_to_cart_button', 'rad_charts' );
							add_action( 'woocommerce_after_add_to_cart_button', 'rad_share' );

							do_action( 'woocommerce_single_product_summary' );
		    		?>
					</div>
	    	</div>
	    </div>
	  </div>
	</div>
</section>

<section class="specific">
	<div class="grid-container">
		<ul class="specific__list">
			<li class="specific__list-item" data-specific="feature">
				<a href="#specific" title="Monobi - Specifics Features">
					<h2 class="specific__category"><?php _e('Features', 'ae'); ?></h2>
				</a>
			</li>
			<li class="specific__list-item" data-specific="fabric">
				<a href="#specific" title="Monobi - Specifics Fabric & Materials">
					<h2 class="specific__category"><?php _e('Fabrics & Materials', 'ae'); ?></h2>
				</a>
			</li>
			<li class="specific__list-item" data-specific="care">
				<a href="#specific" title="Monobi - Specifics Care">
					<h2 class="specific__category"><?php _e('Care', 'ae'); ?></h2>
				</a>
			</li>
		</ul>
	</div>

	<?php
	$specifics = array();

	$specifics[] = 'feature';
	$specifics[] = 'fabric';
	$specifics[] = 'care';

	foreach ($specifics as $specific): ?>
		<?php if( get_field($specific.'_specific_check') ):
			$text_align = 'txt-left';
		?>
			<div class="specific__panel specific__<?php echo $specific . ' ' . $text_align; ?>" data-specific="<?php echo $specific; ?>">
				<?php
					if( have_rows($specific.'_icon_box') ) :

				?>
					<div class="grid-container--huge">
						<div class="row columns-between">
							<?php while( have_rows($specific.'_icon_box') ): the_row(); ?>
								<div class="row__column mobile-6 tab-4">
									<div class="specific__<?php echo $specific; ?>-container">
										<i class="specific__<?php echo $specific; ?>-icon iconae regular x4 ae--<?php echo get_sub_field($specific.'_icon')['value']; ?>"></i>
										<h2 class="specific__<?php echo $specific; ?>-title"><?php echo __(get_sub_field($specific.'_icon')['label'], 'ae'); ?></h2>
									</div>
								</div>
							<?php endwhile; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?php else:
			$text_align = 'txt-left';
			$columns = '12';
			$is_split = 'style= "display: none"';
			if( get_field($specific.'_text_columns') == '2' ){
				$columns = '6';
				$is_split = '';
			};
		?>
			<div class="specific__panel specific__<?php echo $specific . ' ' . $text_align; ?>" data-specific="<?php echo $specific; ?>">
				<div class="grid-container--huge">
					<div class="row columns-between">
						<div class="row__column mobile-12 tab-<?php echo $columns; ?>">
							<?php if( get_field($specific.'_column_title') ): ?>
								<h2 class="specific__<?php echo $specific; ?>-title"><?php echo get_field($specific.'_column_title'); ?></h2>
								<hr class="specific__<?php echo $specific; ?>-score">
							<?php endif; ?>
							<?php if( get_field($specific.'_column_text') ){
								echo get_field($specific.'_column_text');
							}; ?>
						</div>

						<div class="row__column mobile-12 tab-<?php echo $columns; ?>" <?php echo $is_split; ?>>
							<?php if( get_field($specific.'_second_column_title') ): ?>
								<h2 class="specific__<?php echo $specific; ?>-second-title"><?php echo get_field($specific.'_second_column_title'); ?></h2>
								<hr class="specific__<?php echo $specific; ?>-score">
							<?php endif; ?>
							<?php if( get_field($specific.'_second_column_text') ){
								echo get_field($specific.'_second_column_text');
							}; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>

	<?php endforeach; ?>

</section>

<?php get_template_part( 'template-parts/composer', '' ); ?>

<div class="grid-container--huge">
	<?php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
		remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
		do_action( 'woocommerce_after_single_product_summary' );

	?>
</div>

<div class="overlay-slider js--popup popup" id="slide">
	<button class="js--close-popup popup__button"><i class="iconae light ae--cross"></i></button>
	<div class="third-slider joint">
		<?php do_action( 'woocommerce_product_thumbnails' ); ?>
	</div>
</div>

<script>
 jQuery(document).ready(function($) {
		 $('select').blur( function(){
				 if( '' != $('input.variation_id').val() ){
						 $('p.price').html($('div.woocommerce-variation-price > span.price').html()).append('<p class="availability">'+$('div.woocommerce-variation-availability').html()+'</p>');
				 } else {
						 $('p.price').html($('div.hidden-variable-price').html());
						 if($('p.availability'))
								 $('p.availability').remove();
				 }
		 });
 });
 </script>
