<?php
/**
 * framaework functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package framaework
 */

// Custom template tags for this theme.
require get_template_directory() . '/inc/template-tags.php';

// Custom functions that act independently of the theme templates.
require get_template_directory() . '/inc/extras.php';

// Include Elements
require get_template_directory() . '/inc/file_calling.php';

// Merge CSS and JS files into one
if ( !is_admin() && $pagenow != 'wp-login.php' ) {
  require get_template_directory() . '/inc/minification.php';
  require get_template_directory() . '/inc/merging.php';
}

// Inculde Widgets
require get_template_directory() . '/inc/widgets.php';

// Change the menu layout with Bem Semantic & CPT
require get_template_directory() . '/inc/bem_menu.php';
require get_template_directory() . '/inc/monobi_configurator.php';
require get_template_directory() . '/inc/cpt/performances.php';
require get_template_directory() . '/inc/cpt/stories.php';

// Woocommerce calling - If Woocommerce isn't installed, this file will be ignored
if ( class_exists( 'WooCommerce' ) ) {
  require get_template_directory() . '/inc/woocommerce.php';
}


function shorten_string($string, $wordsreturned){
  $retval = $string;
  $string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $string);
  $string = str_replace("\n", " ", $string);
  $array = explode(" ", $string);
  if (count($array)<=$wordsreturned)
  {
    $retval = $string;
  }
  else
  {
    array_splice($array, $wordsreturned);
    $retval = implode(" ", $array)." ...";
  }
  return $retval;
}


if( function_exists('acf_add_options_page') ) {

  $option_page = acf_add_options_page(array(
		'page_title' 	=> 'Icone rassicuranti',
		'menu_title' 	=> 'Icone rassicuranti',
		'menu_slug' 	=> 'icone-rassicuranti',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));

}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
if ( ! function_exists( 'ae_setup' ) ) :

  function ae_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on framaework, use a find and replace
     * to change 'ae' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'ae', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    //Add image size
    add_image_size( 'favicon-16', 16, 16, false );
    add_image_size( 'favicon-32', 32, 32, false );
    add_image_size( 'rectangular', 650, 325, true );
    add_image_size( 'square', 800, 800, true );
    add_theme_support( 'woocommerce', array(
      'thumbnail_image_width' => 600,
      'single_image_width' => 1200,
      )
    );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
      'primary' => __( 'Primary Menu', 'ae' ),
      'secondary' => __( 'Secondary Menu', 'ae' ),
      'primary_footer' => __( 'Primary Footer', 'ae' ),
      'secondary_footer' => __( 'Secondary Footer', 'ae' ),
      'language_menu' => __( 'Language Menu', 'ae' ),
      'language_menu_footer' => __( 'Language Menu Footer', 'ae' )
    ) );


    // Override woocommerce single related products ( woocommerce_related_products exist as well )!!
    if ( ! function_exists( 'woocommerce_output_related_products' ) ) {

    	/**
    	 * Output the related products.
    	 */
    	function woocommerce_output_related_products() {

    		$args = array(
    			'posts_per_page' => 3,
    			'columns'        => 4,
    			'orderby'        => 'rand', // @codingStandardsIgnoreLine.
          'short_description'    => ' ',
    		);

    		woocommerce_related_products( apply_filters( 'woocommerce_output_related_products_args', $args ) );
    	}
    };

    // Override woocommerce archive related products
    if ( ! function_exists( 'woocommerce_related_products' ) ) {

    	/**
    	 * Output the related products.
    	 *
    	 * @param array $args Provided arguments.
    	 */
    	function woocommerce_related_products( $args = array() ) {
    		global $product;
    		if ( ! $product ) {
    			return;
    		}

    		$defaults = array(
    			'posts_per_page' => 2,
    			'columns'        => 2,
    			'orderby'        => 'rand', // @codingStandardsIgnoreLine.
    			'order'          => 'desc',
    		);

    		$args = wp_parse_args( $args, $defaults );

    		// Get visible related products then sort them at random.
    		$args['related_products'] = array_filter( array_map( 'wc_get_product', wc_get_related_products( $product->get_id(), $args['posts_per_page'], $product->get_upsell_ids() ) ), 'wc_products_array_filter_visible' );

    		// Handle orderby.
    		$args['related_products'] = wc_products_array_orderby( $args['related_products'], $args['orderby'], $args['order'] );

    		// Set global loop values.
    		wc_set_loop_prop( 'name', 'related' );
    		wc_set_loop_prop( 'columns', apply_filters( 'woocommerce_related_products_columns', $args['columns'] ) );

    		wc_get_template( 'single-product/related.php', $args );
    	}
    }

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ) );

    // Set Excerpt length
    function custom_excerpt_length( $length ) {
      return 36;
    }
    add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

    // Set Read More at the excerpt end
    function new_excerpt_more( $more ) {
      return ' <a class="read-more animsition-link" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'ae') . '</a>';
    }
    add_filter( 'excerpt_more', 'new_excerpt_more' );

  }

endif;
add_action( 'after_setup_theme', 'ae_setup' );
