<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package framaework
 */

 get_header(); ?>

<div class="grid-container--large">
  <main id="main" class="site-main" role="main">

    <section class="error-404 not-found">
      <div class="grid-container--large txt-center">
        <div class="not-found__box">
          <h1 class="not-found__title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'ae' ); ?></h1>

          <p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'ae' ); ?></p>

          <?php get_search_form(); ?>

        </div>
      </div>
    </section><!-- .error-404 -->

  </main><!-- #main -->
</div>

<?php get_footer(); ?>
