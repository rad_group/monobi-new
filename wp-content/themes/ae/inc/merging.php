<?php
// CSS Merging function
function show_all_styles() {
  // use global to call variable outside function
  global $wp_styles;

  // arrange the queue based on its dependency
  $wp_styles->all_deps($wp_styles->queue);

  // The result
  $handles = $wp_styles->to_do;

  // ALL ARRAY
  // echo '<pre>'; print_r($wp_styles); echo '</pre>';
  // CSS ARRAY
  // echo '<pre>'; print_r($handles); echo '</pre>';

  $css_code = '';

  $merged_file_location = get_stylesheet_directory() . '/css' . DIRECTORY_SEPARATOR . 'merged-style.css';
  $merged_file_location_min = get_stylesheet_directory() . '/css' . DIRECTORY_SEPARATOR . 'merged-style.min.css';

  // loop all styles
  foreach ($handles as $handle) {

    $src = strtok($wp_styles->registered[$handle]->src, '?');

    // #1. Combine CSS File.
    if (strpos($src, 'http') !== false) {

      $site_url = site_url();

      if (strpos($src, $site_url) !== false) {
        $css_file_path = str_replace($site_url, '', $src);
      } else {
        $css_file_path = $src;
      }

      $css_file_path = ltrim($css_file_path, '/');
    } else {
      $css_file_path = ltrim($src, '/');
    }

    // Check wether file exists then merge
    if  (file_exists($css_file_path)) {
      $css_code .=  file_get_contents($css_file_path);
    }
  }

  // write the merged styles into current theme directory
  file_put_contents ( $merged_file_location , $css_code);
  // write the minified styles into current theme directory
  file_put_contents ( $merged_file_location_min , minify_css($css_code));

  // #2. Load the URL of merged file
  wp_enqueue_style('merged-style',  get_stylesheet_directory_uri() . '/css/merged-style.min.css');
  //wp_enqueue_style('merged-style',  get_stylesheet_directory_uri() . '/css/merged-style.css');

  // #3. Deregister all handles
  foreach ($handles as $handle) {
    wp_deregister_style($handle);
  }
}
add_action('wp_print_styles', 'show_all_styles');

// JS merging function
// function show_all_scripts() {
//   // use global to call variable outside function
//   global $wp_scripts;
//
//   $excluded_sctipt = array('jquery-migrate', 'jquery', 'jquery-core');
//
//   // arrange the queue based on its dependency
//   $wp_scripts->all_deps($wp_scripts->queue);
//
//   // The result
//   $handles = $wp_scripts->to_do;
//
//   // ALL ARRAY
//   // echo '<pre>'; print_r($wp_scripts); echo '</pre>';
//   // Script ARRAY
//   // echo '<pre>'; print_r($handles); echo '</pre>';
//
//   $script_code = '';
//
//   $merged_file_location = get_template_directory() . '/js' . DIRECTORY_SEPARATOR . 'merged-script.js';
//   $merged_file_location_min = get_template_directory() . '/js' . DIRECTORY_SEPARATOR . 'merged-script.min.js';
//
//   // loop all styles
//   foreach ($handles as $handle) {
//
//     if ( array_search ( $handle , $excluded_sctipt) === false ) {
//
//       $src = strtok($wp_scripts->registered[$handle]->src, '?');
//
//       // #1. Combine Script File.
//       // If the src is url
//       if (strpos($src, 'http') !== false) {
//
//         $site_url = site_url();
//
//         if (strpos($src, $site_url) !== false) {
//           $script_file_path = str_replace($site_url, '', $src);
//         } else {
//           $script_file_path = $src;
//         }
//
//         $script_file_path = ltrim($script_file_path, '/');
//       } else {
//         $script_file_path = ltrim($src, '/');
//       }
//
//       // Check wether file exists then merge
//       if  (file_exists($script_file_path)) {
//         $script_code .=  file_get_contents($script_file_path);
//       }
//     }
//   }
//
//   // write the merged styles into current theme directory
//   file_put_contents ( $merged_file_location , $script_code);
//
//   // write the minified styles into current theme directory
//   file_put_contents ( $merged_file_location_min , minify_js($script_code));
//
//   // #2. Load the URL of merged file
//   wp_enqueue_script( 'merged-script', get_template_directory_uri() . '/js/merged-script.min.js', array(), null, true );
//
//   // #3. Deregister all handles
//   foreach ($handles as $handle) {
//     if ( array_search ( $handle , $excluded_sctipt) === false ) {
//       wp_deregister_script($handle);
//     }
//   }
//
// }
// add_action('wp_print_scripts', 'show_all_scripts');
