<?php
/**
 * Enqueue scripts and styles.
 *
 * @package framaework
 */

function ae_scripts() {
  // PRIMARY CSS
  wp_enqueue_style( 'wp-style', get_stylesheet_uri() );
  wp_enqueue_style( 'ae-css', get_template_directory_uri() . '/css/framaework.min.css' );

  // PRIMARY JS
  //wp_enqueue_script( 'ae-jquery', get_template_directory_uri() . '/js/gen/ae-jquery.min.js', array(), null, true );
  wp_enqueue_script( 'ae-plugin', get_template_directory_uri() . '/js/gen/plugins.min.js', array(), null, true );
  wp_enqueue_script( 'ae-global', get_template_directory_uri() . '/js/gen/global.min.js', array(), null, true );

  // if ( is_front_page() ) {
  //   wp_enqueue_script( 'ae-home', get_template_directory_uri() . '/js/gen/pages/home.min.js', array(), null, true );
  // }
  //
  // if ( is_page_template( 'page-templates/about.php' ) ) {
  //   wp_enqueue_script( 'ae-about', get_template_directory_uri() . '/js/gen/pages/about.min.js', array(), null, true );
  // }

  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
  wp_enqueue_script( 'comment-reply' );
  }

  // if ( !is_admin() ) {
  //   wp_deregister_script('jquery');
  //   wp_deregister_script('jquery-core');
  //   wp_deregister_script('jquery-migrate');
  // }

}
add_action( 'wp_enqueue_scripts', 'ae_scripts' );
