<?php function monobi_composer(){ ?>

  <?php if( have_rows('configurator') ):

    while ( have_rows('configurator') ) : the_row();

    //Listato di tutte le variabili
    //// CHECK
    $check_text = get_sub_field('check_text');
    $check_media = get_sub_field('check_media');
    ////COLUMNS & ALIGN
    $content_column = get_sub_field('content_column');
    $text_column = get_sub_field('text_column');
    $content_align = get_sub_field('content_align');
    ////CONTENT
    $title = get_sub_field('conf_title');
    $subtitle = get_sub_field('conf_subtitle');
    $content = get_sub_field('conf_content');
    $background_color = get_sub_field('content_background_color');
    ////VIDEO
    $video_mp4 = get_sub_field('conf_video_mp4');
    $video_webm = get_sub_field('conf_video_webm');
    $video_poster = get_sub_field('conf_video_poster');

    $section_size = get_sub_field('section_size');
    $img_full = get_sub_field('section_img_full');
    $repeater_gallery = get_sub_field('section_img_repeater');
    $img_single = get_sub_field('section_img_single');



      if( get_row_layout() == 'standard_block' ):
        //Da aggiungere il pre check delle altre selezioni

        //Check if block is divided in 1 or 2 columns
        if ($content_column == 1) {
          $content__column = '12';
          $start_container = '<div class="grid-container ">';
          $end_container = '</div>';
        }else{
          $content__column = '6';
          $start_container = '';
          $end_container = '';
        };

        if ($text_column == 1){
          $columns = '';
        }else{
          $columns = 'split-column';
        };

        print_r(get_sub_field('check_text'));
        if ($check_text == 'text' || $check_media == 'video' || $check_media == 'image') : ?>
        <?php
        print_r(get_sub_field('check_text'));
        /*
        Basta avevre una delle condizioni per entrare,
        I blocchi o sezioni in seguito verranno gesiti singolarmente e in modo indipendente
        */
        ?>
          <section class="standard-block">
            <?php echo $start_container; ?>
              <div class="row--stuck columns-around">
                <?php if ($check_text == true): ?>
                  <!-- Se il check di testo e fleggato mostra il contenuto -->
                  <div class="row__column tab-<?php echo $content__column; ?>">

                    <div class="standard-block__single-wrap <?php echo $background_color; ?>">
                      <?php if ($title == true): ?>
                        <h2 class="standard-block__title"><?php echo $title; ?></h2>
                      <?php endif; ?>

                      <?php if ($subtitle == true): ?>
                        <h3 class="standard-block__subtitle"><?php echo $subtitle; ?></h3>
                      <?php endif; ?>

                      <div class="standard-block__content <?php echo $columns; ?>">
                        <?php echo $content; ?>
                      </div>
                    </div>

                  </div>
                <?php endif; ?> <!-- #.text -->

                <?php if ($check_media == true): ?>
                  <!-- Se il check media e fleggato mostra il contenuto -->
                  <div class="row__column tab-<?php echo $content__column; ?>">
                    <div class="standard-block__single-wrap">

                      <div class="standard-block__video video-ui js--video-ui wow fadeInDown">
                        <video class="video-ui__video paused" preload="metadata" poster="<?php echo get_sub_field('conf_video_poster'); ?>" data-setup="{}">
                          <source src="<?php echo get_sub_field('conf_video_mp4'); ?>" type="video/mp4">
                          <source src="<?php echo get_sub_field('conf_video_webm'); ?>.webm" type="video/webm">
                            <?php _e('Your browser does not support the video tag.', 'ae'); ?>
                        </video>
                        <div class="video-ui__playpause-img vertical-align">
                          <img src="<?php bloginfo('template_directory'); ?>/img/video-play.png" alt="Video play icon">
                        </div>
                      </div>

                    </div>
                  </div>
                <?php endif; ?> <!-- #.media -->
              </div> <!-- #.row -->
            <?php echo $end_container; ?>
          </section> <!-- #.standard-block -->

        <?php endif; ?>

      <?php //elseif( get_row_layout() == 'image_section' ):

      	//$file = get_sub_field('file');

      endif;

    endwhile;

  endif;

} ?>
