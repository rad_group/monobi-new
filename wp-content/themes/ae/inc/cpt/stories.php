<?php
if( ! function_exists( 'Stories_post_type' ) ) :
  function Stories_post_type() {
    $labels = array(
      'name' => __('Stories'),
      'singular_name' => __('Story'),
      'add_new' => 'Aggiungi nuovo',
      'all_items' => 'Tutte le stories',
      'add_new_item' => 'Aggiungi nuova story',
      'edit_item' => 'Modifica story',
      'new_item' => 'Nuova story',
      'view_item' => 'Visualizza story',
      'search_items' => 'Cerca story',
      'not_found' => 'Nessuna story trovata',
      'not_found_in_trash' => 'Nessuna story trovata nel cestino',
      'parent_item_colon' => 'Genitore story'
      //'menu_name' => default to 'name'
    );
    $args = array(
      'labels' => $labels,
      'description' => 'I lavori della tua pagina stories',
      'public' => true,
      'has_archive' => false,
      'publicly_queryable' => true,
      'query_var' => true,
      'rewrite' => array(
        'slug'                  => 'stories',
    		'with_front'            => true,
    		'pages'                 => true,
    		'feeds'                 => true,
      ),
      'capability_type' => 'post',
      'hierarchical' => false,
      'menu_icon'   => 'dashicons-book-alt',
      'supports' => array(
        'title',
        'editor',
        'excerpt',
        'thumbnail',
        //'author',
        //'trackbacks',
        //'custom-fields',
        //'comments',
        'revisions',
        //'page-attributes', // (menu order, hierarchical must be true to show Parent option)
        //'post-formats',
      ),
      //'taxonomies' => array( 'category', 'post_tag' ), // add default post categories and tags
      'menu_position' => 5,
      //'register_meta_box_cb' => 'Stories_add_post_type_metabox'
    );
    register_post_type( 'stories', $args );
    //flush_rewrite_rules();

    /* Register custom taxonomy - Stories category */
    register_taxonomy (
      'categoria_stories',
      'stories',
      array( 'hierarchical' => true,
        'label' => 'Categorie stories'
      )
    );

  }
  add_action( 'init', 'Stories_post_type' );

endif; // end of function_exists()
