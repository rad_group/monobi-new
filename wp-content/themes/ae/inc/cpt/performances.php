<?php
if( ! function_exists( 'Performances_post_type' ) ) :
  function Performances_post_type() {
    $labels = array(
      'name' => __('Performances'),
      'singular_name' => __('Performance'),
      'add_new' => 'Aggiungi nuovo',
      'all_items' => 'Tutte le performances',
      'add_new_item' => 'Aggiungi nuova performance',
      'edit_item' => 'Modifica performance',
      'new_item' => 'Nuova performance',
      'view_item' => 'Visualizza performance',
      'search_items' => 'Cerca performance',
      'not_found' => 'Nessuna performance trovata',
      'not_found_in_trash' => 'Nessuna performance trovata nel cestino',
      'parent_item_colon' => 'Genitore performance'
      //'menu_name' => default to 'name'
    );
    $args = array(
      'labels' => $labels,
      'description' => 'I lavori della tua pagina performances',
      'public' => true,
      'has_archive' => false,
      'publicly_queryable' => true,
      'query_var' => true,
      'rewrite' => array(
        'slug'                  => 'performances',
    		'with_front'            => true,
    		'pages'                 => true,
    		'feeds'                 => true,
      ),
      'capability_type' => 'post',
      'hierarchical' => false,
      'menu_icon'   => 'dashicons-awards',
      'supports' => array(
        'title',
        'editor',
        'excerpt',
        'thumbnail',
        //'author',
        //'trackbacks',
        //'custom-fields',
        //'comments',
        'revisions',
        //'page-attributes', // (menu order, hierarchical must be true to show Parent option)
        //'post-formats',
      ),
      //'taxonomies' => array( 'category', 'post_tag' ), // add default post categories and tags
      'menu_position' => 5,
      //'register_meta_box_cb' => 'Performances_add_post_type_metabox'
    );
    register_post_type( 'performances', $args );
    //flush_rewrite_rules();

    /* Register custom taxonomy - Performances category */
    register_taxonomy (
      'categoria_performances',
      'performances',
      array( 'hierarchical' => true,
        'label' => 'Categorie performances'
      )
    );

  }
  add_action( 'init', 'Performances_post_type' );

endif; // end of function_exists()
